/**
 * \example: klist.c
 * This example demonstrates the use of the KList, which is a linux kernel
 * style linked list object.  Any struct can be made into a linked list by
 * adding a KList member (e.g. struct employee below).  In this example, we
 * build a very simple employee ID demo that allows for querying, deletion,
 * and display of employee profiles using KList manipulation facilities.
 * Employee photos are stored as KImages, but only contain pixels whose
 * value are simply equal to the employee's ID number.
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "kpixel.h"
#include "kimage.h"
#include "klist.h"
#include "kzipper.h"


struct employee {
    unsigned int id;
    char name[20];
    KImage* img;
    KList list;
};

static KLIST_HEAD(employee_list);


static int employee_create (char* name, unsigned int id)
{
    int i;
    KPixel *px;
    int b[3];

    struct employee *new = malloc(sizeof(*new));

    if (!new)
        return -ENOMEM;

    new->id = id;
    sprintf (new->name, "%s", name);

    new->img = kimage_new (uint_px, 1, 2, kzip(b, 10, 10));
    px = kimage_raw_pixels(new->img);
    for (i=0; i<kimage_npix(new->img); i++) {
        px[i] = (KPixel)id;
    }
    
    klist_add (&new->list, &employee_list);

    return 0;
}

static struct employee *employee_find (unsigned int id)
{
    struct employee *tmp = NULL;

    klist_for_each_entry (tmp, &employee_list, list) {
        if (tmp->id == id)
            return tmp;
    }

    return NULL;
}

static void employee_destroy (unsigned int id)
{
    struct employee *tmp = NULL;

    tmp = employee_find (id);

    if (tmp) {
        klist_del (&tmp->list);
        kimage_release (tmp->img);
        free (tmp);
    }
}

static void employee_display (struct employee *e)
{
    int i, j;
    int b[2];

    KImage* img = e->img;

    printf ("Name: %s\n", e->name);
    printf ("ID# : %u\n", e->id);
    printf ("Photo:\n");

    for (j=0; j<kimage_height(img); j++) {
        for (i=0; i<kimage_width(img); i++) {
            printf ("%u ", kimage_pixel(img, 0, kzip(b, i, j)).uint_px);
        }
        printf ("\n");
    }

    printf ("\n");
}

int main (int argc, char** argv)
{
    struct employee *tmp;

    employee_create ("Alice", 1);
    employee_create ("Bob",   2);
    employee_create ("Sam",   3);
    employee_create ("Zep",   4);

    tmp = employee_find (3);
    employee_display (tmp);

    tmp = employee_find (1);
    employee_display (tmp);

    tmp = employee_find (4);
    employee_display (tmp);

    employee_destroy (3);
    employee_destroy (1);
    employee_destroy (2);
    employee_destroy (4);

    return 0;
}
