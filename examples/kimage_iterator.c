/**
 * \example kimage_iterator.c
 * KImageIterator objects are capable of iterating through pixels for
 * images of arbitrary dimensionality.  This example demonstrates their
 * use for a 2D image.  Usage for images of other dimensionality is
 * performed similarly.  The dimensionality of iter.coords[] simply
 * increases accordingly to match the dimensionality of the image being
 * iterated.
 */


#include <stdio.h>
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


int main (int argc, char** argv)
{
    KImage* lena;
    KImage* clone;
    KImageIterator iter;
    KPixel val;
    int b[2];

    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");

    /* convert the pixels to floats (this occurs "in place") */
    kimage_convert (lena, float_px);

    /* make a copy with an empty pixel buffer
     * (still has same pixel buffer dimensions, pixel type, etc) */
    clone = kimage_empty_clone (lena);

    /* iterate through out lena image & generate something trippy */
    kimage_iterator_init (&iter, lena);
    for (; kimage_iterator_check(&iter); kimage_iterator_incr(&iter)) {
        val.float_px  = kimage_pixel (lena, 0, kzip(b, iter.coords[0]-10, iter.coords[1])).float_px;
        val.float_px += kimage_pixel (lena, 0, kzip(b, iter.coords[0]+10, iter.coords[1])).float_px;
        val.float_px += kimage_pixel (lena, 0, kzip(b, iter.coords[0], iter.coords[1]-10)).float_px;
        val.float_px += kimage_pixel (lena, 0, kzip(b, iter.coords[0], iter.coords[1]+10)).float_px;
        val.float_px /= 4.0;

        kimage_set_pixel (clone, val, 0, kzip(b, iter.coords[0], iter.coords[1]));
    }

    /* Note: if the above loop cycles to completion (i.e. doesn't terminate
     * earier via break), the following call to kimage_iterator_destroy()
     * is unnecessary (but doesn't hurt). */
    kimage_iterator_destroy (&iter);

    /* convert pixels back to chars before writing to PNG */
    kimage_convert (clone, char_px);

    /* write our trippy new image to disk */
    kimage_write ("kimage_iterator.png", clone);
    printf ("Wrote kimage_iterator.png\n");

    /* cleanup */
    kimage_release (clone);
    kimage_release (lena);

    kmodule_unload_bank ();

    return 0;
}
