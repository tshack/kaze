/**
 * \example kzipper.c
 * A kzipper is a utility function provided by Kaze that can be used to
 * quickly populate an array variadicly.  Because kzippers also return a
 * pointer to the populated array, they are commonly used in conjunction
 * with Kaze API functions which accept an array of coordinates.
 *
 * To demonstrate, this example provides a simple function
 * `print_coords(int* A)`, which prints the 4 `int`s pointed to by `A`
 * to the screen.
 *
 * A kzipper can be employed to effectively convert `print_coords()`
 * into a variadic function:
 *
 * `print_coords(kzip(a, i, j, k, l));`
 *
 * where `a` is an `int[4]` array.
 *
 * Also note, that kzippers provide limited type polymorphism.  The
 * `kzip()` function can populate `int`, `unsigned int`, and `double`
 * arrays.
 */ 

#include <stdlib.h>
#include <stdio.h>

#include "kimage.h"
#include "kzipper.h"

void print_coords (int* A)
{
    printf ("[%i, %i, %i, %i]\n", A[0], A[1], A[2], A[3]);
}


int main (int argc, char** argv)
{
    int i, j, k, l;
    int a[4];

    for (l=0; l<3; l++)
        for (k=0; k<3; k++)
            for (j=0; j<3; j++)
                for (i=0; i<3; i++)
                    print_coords (kzip(a, i, j, k, l));

    return 0;
}
