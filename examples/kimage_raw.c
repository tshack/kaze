/**
 * \example kimage_raw.c
 * This example demonstrates the basics of manipulating a KImage object by
 * requesting raw encapsulated pixel buffer.  The method of pixel access
 * and manipulation is faster than using the "safe" user API functions, but
 * comes at the risk of corrupting the pixel buffer with an ill-formed
 * memory write pattern.
 */

#include <stdio.h>
#include "kimage.h"
#include "kzipper.h"


static void print_image_info (KImage* img)
{
    unsigned int i;

    printf ("kimg @ %p\n", img);
    printf ("  --   type: %u\n", kimage_type(img));
    printf ("  --   npix: %lu\n", kimage_npix(img));
    printf ("  --  width: %u\n", kimage_width(img));
    printf ("  -- height: %u\n", kimage_height(img));
    printf ("  --  depth: %u\n", kimage_depth(img));
    printf ("  --   ndim: %u\n", kimage_ndim(img));

    for (i=0; i<kimage_ndim(img); i++) {
        printf ("      -- dim[%u]: %u\n", i, kimage_dim(img, i));
    }
}


int main (int argc, char** argv)
{
    unsigned int i, j;
    volatile float val;
    KImage* img;
    unsigned char* char_pix;
    float* float_pix;
    int b[2];

    img = kimage_new(char_px, 1, 2, kzip(b, 3, 5));
    char_pix = kimage_raw_pixels(img);

    print_image_info (img);

    /* populate the image */
    for (j=0; j<kimage_height(img); j++) {
        for (i=0; i<kimage_width(img); i++) {
            char_pix[j*kimage_width(img)+i] = 5;
        }
    }

    /* convert the pixel type to float */
    kimage_convert(img, float_px);
    float_pix = kimage_raw_pixels(img);

    /* display the image */
    for (j=0; j<kimage_height(img); j++) {
        for (i=0; i<kimage_width(img); i++) {
            val = float_pix[j*kimage_width(img)+i];
            printf ("%f ", val);
        }
        printf ("\n");
    }

    kimage_release (img);

    return 0;
}
