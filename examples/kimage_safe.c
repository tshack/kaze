/**
 * \example kimage_safe.c
 * This example demonstrates the basics of manipulating a KImage object
 * using the "safe" pixel setting and getting routines"
 */

#include <stdio.h>
#include "kimage.h"
#include "kzipper.h"


static void print_image_info (KImage* img)
{
    unsigned int i;

    printf ("kimg @ %p\n", img);
    printf ("  --   type: %u\n", kimage_type(img));
    printf ("  --   npix: %lu\n", kimage_npix(img));
    printf ("  --  width: %u\n", kimage_width(img));
    printf ("  -- height: %u\n", kimage_height(img));
    printf ("  --  depth: %u\n", kimage_depth(img));
    printf ("  --   ndim: %u\n", kimage_ndim(img));

    for (i=0; i<kimage_ndim(img); i++) {
        printf ("      -- dim[%u]: %u\n", i, kimage_dim(img, i));
    }
}


int main (int argc, char** argv)
{
    unsigned int i, j;
    volatile float val;
    int b[2];

    KImage* img;
    KImage* clone;

    img = kimage_new(char_px, 1, 2, kzip(b, 3, 5));

    print_image_info (img);

    /* populate the image */
    for (j=0; j<kimage_height(img); j++) {
        for (i=0; i<kimage_width(img); i++) {
            kimage_set_pixel(img, (KPixel)9, 0, kzip(b, i, j));
        }
    }

    /* convert the pixel type to float */
    kimage_convert(img, float_px);

    /* display the image */
    for (j=0; j<kimage_height(img); j++) {
        for (i=0; i<kimage_width(img); i++) {
            val = kimage_pixel(img, 0, kzip(b, i, j)).float_px;
            printf ("%f ", val);
        }
        printf ("\n");
    }

    /* make a perfect clone */
    clone = kimage_clone(img);
    kimage_convert(clone, float_px);

    print_image_info(clone);

    /* display the image */
    printf ("clone:\n");
    for (j=0; j<kimage_height(clone); j++) {
        for (i=0; i<kimage_width(clone); i++) {
            val = kimage_pixel(clone, 0, kzip(b, i, j)).float_px;
            printf ("%f ", val);
        }
        printf ("\n");
    }

    kimage_release (clone);
    kimage_release (img);

    return 0;
}
