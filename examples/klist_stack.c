/**
 * \example klist_stack.c
 * This example shows how to build a simple stack using a KList.  Items are
 * first in last out (FILO).
 */

#include <stdlib.h>
#include <stdio.h>

#include "klist.h"

char* names[] = {
    "Alice",
    "Bob",
    "Carl",
    "Danny",
    "Earl",
    "Frank",
    "Gilbert",
    "Hobbs",
    "Iggy",
    "Jeeves",
    "Krusty"
};

struct item {
    char name[20];
    KList list;
};

static KLIST_HEAD(stack);


static void push (struct item *obj)
{
    /* add item to top of list (i.e. PUSH) */
    klist_add (&obj->list, &stack);
}


static struct item *pop (void)
{
    /* get the item at the top of the list */
    struct item *ret = klist_first_entry (&stack, struct item, list);

    /* remove the item from the list (i.e. POP) */
    klist_del (&ret->list);

    return ret;
}


static struct item *item_create (char* name)
{
    struct item *new = malloc(sizeof(*new));

    sprintf (new->name, "%s", name);

    return new;
}


static void item_display (struct item *obj)
{
    printf ("Name: %s\n", obj->name);
}


int main (int argc, char** argv)
{
    int i;
    struct item *tmp;

    int num_names = sizeof(names) / sizeof(char*);

    /* load up the stack with names */
    printf ("====== PUSH ======\n");
    for (i=0; i<num_names; i++) {
        tmp = item_create (names[i]);
        item_display (tmp);

        push (tmp);
    }

    /* pop'em off... LIFO style */
    printf ("\n====== POP =======\n");
    for (i=0; i<num_names; i++) {
        tmp = pop ();
        item_display (tmp);

        free (tmp);
    }

    return 0;
}
