/**
 * \example block_downsample.c
 * This example demonstrates how to use the "downsample" block to reduce
 * the size of an image.
 *
 * The image may be reduced one of two ways: with or without averaging.
 * The option "averaging" can be set to either "enable" or "disable". If
 * averaging is not enabled, then the image is reduced by dropping undesired
 * pixels. If averaging is enabled, then each pixel in the image is the result
 * of linear interpolation and averaging of a 3x3 neighbourhood of pixels.
 * The "factor" for image reduction must be between 0.0001 and 1.0.
 *
 * This example also demonstrates the difference between disabling averaging
 * mode with and without Gaussian blurring on the input image.
 * */

#include <stdio.h>
#include <stdlib.h>
#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kblock.h"

static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}

int main (int argc, char** argv)
{
    int i, j;
    KImage* lena;
    KImage* lena_blur;
    KImage* lena_dwn1;
    KImage* lena_dwn2;
    KImage* lena_dwn3;
    KImage* gkernel;
    KBlockContext* gkern, *convolve, *dwnsmp;

    /* initialize Kaze generators and modules */
    kmodule_load_bank ();

    /* load example image */
    lena = kimage_read ("images/lena.png");

    /* set up a Gaussian kernel generator ******************/
    gkern = kblock_new_context ("gausskernel");
    if (!gkern) {
        fprintf (stderr, "error -- unable to load filter\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (gkern, error_handler, NULL);
    /*******************************************************/

    /* use gaussian kernel generator to build a sigma = 2.0 kernel ****/
    kblock_set_option (gkern, "sigma", 2.0);
    kblock_set_option (gkern, "dimension", 2);
    kblock_execute (gkern);
    gkernel = kblock_get_output (gkern, "output");
    kblock_free_context (gkern);
    /******************************************************************/

    /* set up convolution filter to blur image **************/
    convolve = kblock_new_context ("convolve");
    if (!convolve) {
        fprintf (stderr, "error -- unable to load filter\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (convolve, error_handler, NULL);
    /********************************************************/

    /* convolve lena image with gauss kernel and free convolve module ******/
    kblock_set_input (convolve, "input", lena);
    kblock_set_input (convolve, "kernel", gkernel);
    kblock_execute (convolve);
    lena_blur = kblock_get_output (convolve, "output");
    kblock_free_context (convolve);
    kimage_release (gkernel);
    /***********************************************************************/

    /* set up downsample filter ******************************/
    dwnsmp = kblock_new_context ("downsample");
    if (!dwnsmp) {
        fprintf (stderr, "error -- unable to load filter\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (dwnsmp, error_handler, NULL);
    /*********************************************************/

    /* downsample blurred Lena to 50% of image size */
    kblock_set_input (dwnsmp, "input", lena_blur);
    kblock_set_option (dwnsmp, "factor", 0.5);
    kblock_execute (dwnsmp);
    lena_dwn1 = kblock_get_output (dwnsmp, "output");
    kimage_write ("module_downsample_blur_50p.png", lena_dwn1);
    printf("Wrote module_downsample_blur_50p.png\n");
    kimage_release (lena_dwn1);
    kimage_release (lena_blur);
    /************************************************/

    /* downsample unblurred Lena to 50% of image size */
    kblock_set_input (dwnsmp, "input", lena);
    kblock_execute (dwnsmp);
    lena_dwn2 = kblock_get_output (dwnsmp, "output");
    kimage_write ("module_downsample_no_blur_50p.png", lena_dwn2);
    printf("Wrote module_downsample_noblur_50p.png\n");
    kimage_release (lena_dwn2);
    /**************************************************/

    /* downsample unblurred Lena to 70% of image size with average mode */
    kblock_set_option (dwnsmp, "averaging", "enable");
    kblock_set_option (dwnsmp, "factor", 0.7);
    kblock_execute (dwnsmp);
    lena_dwn3 = kblock_get_output (dwnsmp, "output");
    kimage_write ("module_downsample_average_70p.png", lena_dwn3);
    printf("Wrote module_downsample_average_70p.png\n");
    kimage_release (lena_dwn3);
    kimage_release (lena);
    /********************************************************************/

    /* cleanup */
    kblock_free_context (dwnsmp);
    kmodule_unload_bank ();

    return 0;
}
