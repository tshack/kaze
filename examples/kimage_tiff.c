#include <stdio.h>
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


static KImage* generate_image (unsigned int width, unsigned int height, unsigned int channels)
{
    int i, j, c;
    int b[2];

    KImage* img = kimage_new (char_px, channels, 2, kzip(b, width, height));
    unsigned char* pixels = kimage_raw_pixels(img);

    for (c=0; c<channels; c++)
        for (j=0; j<height; j++)
            for (i=0; i<width; i++)
                pixels[width*height*c + width*j + i] = (i*j) % 255;

    return img;
}

static KErrorCode compare (KImage* a, KImage* b)
{
    unsigned int i, j;

    unsigned char* pix_a = kimage_raw_pixels(a);
    unsigned char* pix_b = kimage_raw_pixels(b);
    unsigned int width  = kimage_width(a);
    unsigned int height = kimage_height(b);

    for (j=0; j<height; j++)
        for (i=0; i<width; i++)
            if (pix_a[j*width+i] != pix_b[j*width+i])
                return FAILURE;

    return SUCCESS;
}


int main (int argc, char** argv)
{
    unsigned int i, j;
    KImage *img1, *img2, *img;

    unsigned int width  = 1024;
    unsigned int height = 768;
    unsigned int channels = 3;

    kmodule_load_bank ();

    printf ("Generating test image...\n");
    img1 = generate_image(width, height, channels);

    printf ("Writing image to test.tiff\n");
    if (kimage_write ("test.tiff", img1) != SUCCESS) {
        fprintf (stderr, "...FAIL!\n");
        return (int)FAILURE;
    }

    printf ("Reading test.tiff\n");
    img2 = kimage_read ("test.tiff");
    if (!img2) {
        fprintf (stderr, "...FAIL!\n");
        return (int)FAILURE;
    }

    printf ("Comparing test.tiff to generated image\n");
    if (compare (img1, img2)) {
        fprintf (stderr, "...FAIL!\n\n");
        return (int)FAILURE;
    }
    fprintf (stderr, "...SUCCESS!\n\n");

    kimage_release (img1);
    kimage_release (img2);

    kmodule_unload_bank ();

    return 0;
}
