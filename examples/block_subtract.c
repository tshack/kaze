/**
\example block_subtract.c
This example demonstrates how to use the "subtract" block to subtract
one image from another to produce a third image containing the
difference between them.

Both input images must have the same dimenstionality and pixel type.  If
the subtraction of two pixel values would result in a value smaller than
the minimal storable value of the current pixel type, then the produced
pixel value is clamped to the minimum value that the current pixel type
is able to represent.
*/


#include <stdlib.h>
#include <stdio.h>
#include "kblock.h"
#include "kimage.h"
#include "kmodule.h"


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    KImage* lena;
    KImage* shift;
    KImage* out;
    KBlockContext* subtract;

    unsigned long n;
    unsigned char* pixels;

    /* initialize all of Kaze's internal modules */
    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");
    shift = kimage_read ("images/lena.png");
    pixels = kimage_raw_pixels (shift);

    /* shift all pixels back by one */
    for (n=0; n<kimage_nchannels(shift)*kimage_npix(shift)-1; n++)
        pixels[n] = pixels[n+1];

    /* setup a subtraction module context *****************************/
    subtract = kblock_new_context ("subtract");
    if (!subtract) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (subtract, error_handler, NULL);
    /******************************************************************/

    /* subtract "shifted lena" from "normal lena" *********************/
    kblock_set_input (subtract, "minuend", lena);
    kblock_set_input (subtract, "subtrahend", shift);

    kblock_execute (subtract);

    out = kblock_get_output (subtract, "output");

    kblock_free_context (subtract);
    /******************************************************************/

    /* write result to disk */
    kimage_write ("module_subtract.png", out);
    printf ("Wrote module_subtract.png\n");

    /* cleanup */
    kimage_release (out);
    kimage_release (shift);
    kimage_release (lena);

    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
