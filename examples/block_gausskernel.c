/**
 * \example block_gausskernel.c
 * This example first demonstrates how to use the Gaussian kernel
 * generator.  Subsequently, the Gaussain kernel is used to demonstrate
 * the use of the convolution block in three different image edge
 * handling modes ("crop", "wrap", and "extend").
 */


#include <stdlib.h>
#include <stdio.h>
#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    int i, j;
    KImage* lena;
    KImage* out;
    KImage* gkernel;
    KBlockContext* gausskernel;
    KBlockContext* convolve;
    KErrorCode err;

    /* initialize all of Kaze's modules and generators */
    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");

    /* setup a Gaussian kernel generator context **********/
    gausskernel = kblock_new_context ("gausskernel");
    if (!gausskernel) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (gausskernel, error_handler, NULL);
    /******************************************************/

    /* build 2 sigma Gaussian kernel & free the generator context *****/
    kblock_set_option (gausskernel, "sigma", 2.0);
    kblock_set_option (gausskernel, "dimension", 2);

    kblock_execute (gausskernel);

    gkernel = kblock_get_output (gausskernel, "output");

    kblock_free_context (gausskernel);
    /******************************************************************/

    /* setup a convolution module context ************************/
    convolve = kblock_new_context ("convolve");
    if (!convolve) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (convolve, error_handler, NULL);
    /*************************************************************/


    /* convolve the lena image with our Gaussian kernel **********/
    kblock_set_input (convolve, "input", lena);
    kblock_set_input (convolve, "kernel", gkernel);

    kblock_execute (convolve);

    out = kblock_get_output (convolve, "output");
    /*************************************************************/

    /* Write result to disk & free output KImage */
    kimage_write ("module_gausskernel_crop.png", out);
    printf ("Wrote module_gausskernel_crop.png\n");
    kimage_release (out);


    /* convolve again, but with image wrapping **********/
    kblock_set_option (convolve, "edge_handle", "wrap");

    kblock_execute (convolve);

    out = kblock_get_output (convolve, "output");
    /****************************************************/

    /* Write result to disk & free output KImage */
    kimage_write ("module_gausskernel_wrap.png", out);
    printf ("Wrote module_gausskernel_wrap.png\n");
    kimage_release (out);


    /* convolve again, but with image extending **********/
    kblock_set_option (convolve, "edge_handle", "extend");
    kblock_execute (convolve);

    out = kblock_get_output (convolve, "output");
    /*****************************************************/

    /* Write result to disk & free output KImage */
    kimage_write ("module_gausskernel_extend.png", out);
    printf ("Wrote module_gausskernel_extend.png\n");
    kimage_release (out);


    /* cleanup */
    kblock_free_context (convolve);
    kimage_release (lena);
    kimage_release (gkernel);

    kmodule_unload_bank ();

    return 0;
}
