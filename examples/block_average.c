/**
\example block_average.c
This example demonstrates how to use the averaging block.  The averaging
block accepts a single input (named `"input"`) and produces a single
output (named `"output"`).  Each pixel in the output buffer will be the
average of the neighborhood centered at the corresponding pixel in the
input image.  The size of the neighborhood is settable via the
`"radius"` option.  Note that the `"radius"` option defiles the edge
length (in pixels) for square neighborhood and must be an odd number.
*/

#include <stdlib.h>
#include <stdio.h>
#include "kblock.h"
#include "kimage.h"
#include "kmodule.h"


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    KImage* lena;
    KImage* out;
    KBlockContext* average;

    /* initialize all of Kaze's modules and generators */
    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");

    /* setup an averaging module context ******************************/
    average = kblock_new_context ("average");
    if (!average) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (average, error_handler, NULL);
    /******************************************************************/

    /* average each pixel using a 7x7 neighborhood (must be odd) ******/
    kblock_set_input (average, "input", lena);
    kblock_set_option (average, "radius", 7);

    kblock_execute (average);

    out = kblock_get_output (average, "output");

    kblock_free_context (average);
    /******************************************************************/

    /* write result to disk */
    kimage_write ("module_average.png", out);
    printf ("Wrote module_average.png\n");

    /* cleanup */
    kimage_release (out);
    kimage_release (lena);

    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
