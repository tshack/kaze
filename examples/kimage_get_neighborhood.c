/**
 * \example kimage_get_neighborhood.c
 * One of the powerful convenience features provided by the Kaze core is
 * the ability to snatch an arbitrary neighborhood of pixels into a
 * temporary KImage pixel buffer by using kimage_get_neighborhood().
 * Generally, processing would be performed on neighborhoods obtained in
 * this fashion, but here we simply write the obtained neighborhood to disk
 * as a PNG for demonstration purposes.
 */

#include <stdlib.h>
#include <stdio.h>
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


int main (int argc, char** argv)
{
    KImage* lena;
    KImage* nhood;
    KErrorCode err;
    int b[2];

    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");

    /* allocate a 111x111 KImage buffer for the neighborhood (it's HUGE!)
     *   Note: Since neighborhoods must be centered about a pixel,
     *   neighborhood dimensions must be odd! */
    nhood = kimage_new (kimage_type(lena), kimage_nchannels(lena),
            kimage_ndim(lena), kzip(b, 111, 111));

    /* grab a 111x111 neighborhood of pixels centered @ (300, 270) */
    err = kimage_get_neighborhood (lena, nhood, kzip(b, 300, 270));
    if (err) {
        fprintf (stderr, "error -- %s [%i]\n", kerror(err), (unsigned int)err);
        exit (EXIT_FAILURE);
    }

    /* write result to disk */
    kimage_write ("kimage_get_neighborhood.png", nhood);
    printf ("Wrote kimage_get_neighborhood.png\n");

    /* cleanup */
    kimage_release (nhood);
    kimage_release (lena);

    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
