/**
 * \example block_threshold.c
 * This example demonstrates how to use the treshold block on an image.
 * The threshold option "threshold" sets the threshold value and the option
 * "direction," which can take on values "above" and "below" determines if
 * pixel above or below the threshold value are replaced.  The option
 * "fill_value" provides the pixel value used for replacement.
 */

#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"


void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    KImage* lena;
    KImage* out;
    KBlockContext* threshold;

    /* initialize all of Kaze's modules and generators */
    kmodule_load_bank ();

    /* load an example image to play with */
    lena = kimage_read ("images/lena.png");

    /* setup a threshold context *******************************/
    threshold = kblock_new_context ("threshold");
    if (!threshold) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }

    kblock_error_handler (threshold, error_handler, NULL);
    /***********************************************************/

    /* threshold: replace all pixel with value < 128 with 0 ****/
    kblock_set_input (threshold, "input", lena);
    kblock_set_option (threshold, "threshold", 128);
    kblock_set_option (threshold, "fill_value", 0);
    kblock_set_option (threshold, "direction", "below");

    kblock_execute (threshold);

    out = kblock_get_output (threshold, "output");

    kblock_free_context (threshold);
    /***********************************************************/

    /* write result to disk */
    kimage_write ("module_threshold.png", out);
    printf ("Wrote module_threshold.png\n");


    /* cleanup */
    kimage_release (lena);
    kimage_release (out);

    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
