/**
 * \example kimage_png.c
 * This example generates a KImage, writes it to a PNG file, reads that PNG
 * file back into a new KImage, and compares the read PNG pixels to the
 * original generated pixel data.
 */

#include <stdio.h>
#include "kerror.h"
#include "kmodule.h"
#include "kimage.h"
#include "kzipper.h"


static KImage* generate_image (unsigned int width, unsigned int height)
{
    int i, j;
    int b[2];

    KImage* img = kimage_new (char_px, 1, 2, kzip(b, width, height));
    unsigned char* pixels = kimage_raw_pixels(img);

    for (j=0; j<height; j++)
        for (i=0; i<width; i++)
            pixels[j*width+i] = (i*j) % 255;

    return img;
}


static KErrorCode compare (KImage* a, KImage* b)
{
    unsigned int i, j;

    unsigned char* pix_a = kimage_raw_pixels(a);
    unsigned char* pix_b = kimage_raw_pixels(b);
    unsigned int width  = kimage_width(a);
    unsigned int height = kimage_height(b);

    for (j=0; j<height; j++)
        for (i=0; i<width; i++)
            if (pix_a[j*width+i] != pix_b[j*width+i])
                return FAILURE;

    return SUCCESS;
}


int main (int argc, char** argv)
{
    KImage *img1, *img2;

    unsigned int width  = 1024;
    unsigned int height = 768;

    kmodule_load_bank ();

    printf ("Generating test image...\n");
    img1 = generate_image(width, height);

    printf ("Writing image to test.png\n");
    if (kimage_write ("test.png", img1) != SUCCESS) {
        fprintf (stderr, "...FAIL!\n");
        return (int)FAILURE;
    }

    printf ("Reading test.png\n");
    img2 = kimage_read ("test.png");
    if (!img2) {
        fprintf (stderr, "...FAIL!\n");
        return (int)FAILURE;
    }

    printf ("Comparing test.png to generated image\n");
    if (compare (img1, img2)) {
        fprintf (stderr, "...FAIL!\n\n");
        return (int)FAILURE;
    }
    fprintf (stderr, "...SUCCESS!\n\n");

    kimage_release (img1);
    kimage_release (img2);

    kmodule_unload_bank ();

    return 0;
}
