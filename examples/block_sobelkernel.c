/**
 * \example block_sobelkernel.c
 * This example first demonstrates how to use the sobel kernel generator.
 * Subsequently, the sobel kernels are used to demonstrate the use of the
 * convolution block.
 */

#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit (EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    int i, j;
    KImage* lena;
    KImage* lena_sobel_x;
    KImage* lena_sobel_y;
    KImage* lena_sobel_xy;
    KImage* kernelx;
    KImage* kernely;
    KBlockContext* conv;
    KBlockContext* sobelkernel;
    KErrorCode err;

    /* initialize all the Kaze modules & generators */
    kmodule_load_bank ();

    /* load our example image of Lena */
    lena = kimage_read ("images/lena.png");

    /* setup sobel kernel generator context ***************/
    sobelkernel = kblock_new_context ("sobelkernel");
    if (!sobelkernel) {
        return NO_CONTEXT;
    }

    kblock_error_handler (sobelkernel, error_handler, NULL);
    /******************************************************/

    /* build sobel kernels ********************************/
    kblock_set_option (sobelkernel, "size", 3);

    kblock_execute (sobelkernel);

    kernelx = kblock_get_output (sobelkernel, "outputX");
    kernely = kblock_get_output (sobelkernel, "outputY");

    kblock_free_context (sobelkernel);
    /******************************************************/


    /* setup convolution context **************************/
    conv = kblock_new_context ("convolve");
    if (!conv) {
        return NO_CONTEXT;
    }

    kblock_error_handler (conv, error_handler, NULL);
    /******************************************************/

    /* convolve lena with x sobel kernel ******************/
    kblock_set_input (conv, "input", lena);
    kblock_set_input (conv, "kernel", kernelx);

    kblock_set_option (conv, "edge_handle", "extend");

    kblock_execute (conv);

    lena_sobel_x = kblock_get_output (conv, "output");
    kimage_write ("module_sobelkernel_x.png", lena_sobel_x);
    printf ("Wrote module_sobelkernel_x.png\n");
    /******************************************************/

    /* convolve lena with y sobel kernel ******************/
    kblock_set_input (conv, "kernel", kernely);

    kblock_execute (conv);

    lena_sobel_y = kblock_get_output (conv, "output");
    kimage_write ("module_sobelkernel_y.png", lena_sobel_y);
    printf ("Wrote module_sobelkernel_y.png\n");
    kimage_release (lena_sobel_y);
    /******************************************************/

    /* convolve lena_sobel_x with y sobel kernel **********/
    kblock_set_input (conv, "input", lena_sobel_x);

    kblock_execute (conv);

    lena_sobel_xy = kblock_get_output (conv, "output");
    kimage_write ("module_sobelkernel_xy.png", lena_sobel_xy);
    printf ("Wrote module_sobelkernel_xy.png\n");
    kimage_release (lena_sobel_xy);
    /******************************************************/


    /* cleanup time ***************************************/
    kblock_free_context (conv);
    kimage_release (kernelx);
    kimage_release (kernely);
    kimage_release (lena);
    kimage_release (lena_sobel_x);

    kmodule_unload_bank ();
    /******************************************************/

    return 0;
}
