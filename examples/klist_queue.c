/**
 * \example klist_queue.c
 * This example shows how to build a simple queue using a KList.  Items are
 * first in first out (FIFO).
 */

#include <stdlib.h>
#include <stdio.h>

#include "klist.h"

char* names[] = {
    "Alice",
    "Bob",
    "Carl",
    "Danny",
    "Earl",
    "Frank",
    "Gilbert",
    "Hobbs",
    "Iggy",
    "Jeeves",
    "Krusty"
};

struct item {
    char name[20];
    KList list;
};

static KLIST_HEAD(queue);


static void enqueue (struct item *obj)
{
    /* add item to top of list (i.e. PUSH) */
    klist_add (&obj->list, &queue);
}


static struct item *dequeue (void)
{
    /* get the item at the end of the list */
    struct item *ret = klist_last_entry (&queue, struct item, list);

    /* remove the item from the list */
    klist_del (&ret->list);

    return ret;
}


static struct item *item_create (char* name)
{
    struct item *new = malloc(sizeof(*new));

    sprintf (new->name, "%s", name);

    return new;
}


static void item_display (struct item *obj)
{
    printf ("Name: %s\n", obj->name);
}


int main (int argc, char** argv)
{
    int i;
    struct item *tmp;

    int num_names = sizeof(names) / sizeof(char*);

    /* load up the queue with names */
    printf ("====== ENQUEUE ======\n");
    for (i=0; i<num_names; i++) {
        tmp = item_create (names[i]);
        item_display (tmp);

        enqueue (tmp);
    }

    /* unload... FIFO style */
    printf ("\n====== DEQUEUE ======\n");
    for (i=0; i<num_names; i++) {
        tmp = dequeue ();
        item_display (tmp);

        free (tmp);
    }

    return 0;
}
