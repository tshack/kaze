#include <math.h>
#include <stdlib.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#define DELTA 0.0001


#define MAKE_TEST_IMAGE(type, input) \
do { \
    unsigned long i, j, k; \
    unsigned int width, height, depth; \
\
    type* input_px = kimage_raw_pixels(input); \
    width  = kimage_width(input); \
    height = kimage_height(input); \
    depth  = kimage_depth(input); \
\
    for (k=0; k < depth ; k++) \
    for (j=0; j < height; j++) \
    for (i=0; i < width ; i++) \
        input_px[k*height*width+j*width+i] = 0; \
\
    switch (kimage_ndim(input)) { \
        case 1: \
            input_px[7] = 255; \
            break; \
        case 2: \
            input_px[7*width+7] = 255; \
            break; \
        case 3: \
            input_px[7*height*width+7*width+7] = 255; \
            break; \
        default: \
            break; \
    } \
\
} while (0)


#define CHECK_OUTPUT_IMAGE(type, input, kernel, offset, err_int) \
do { \
    unsigned int i, j, k, i_stop, j_stop, k_stop; \
    unsigned int input_width, input_height, input_depth; \
    unsigned int kern_width, kern_height, kern_depth; \
    KImage* nhood; \
    int b[3]; \
\
    float* kernel_px = kimage_raw_pixels(kernel); \
    input_width  = kimage_width(input)/2; \
    input_height = kimage_height(input)/2; \
    input_depth  = kimage_depth(input)/2; \
    i_stop = kern_width  = kimage_width(kernel); \
    j_stop = kern_height = kimage_height(kernel); \
    k_stop = kern_depth  = kimage_depth(kernel); \
\
    if (!input_width%2) \
        input_width++; \
    if (!input_height%2) \
        input_height++; \
    if (!input_depth%2) \
        input_depth++; \
    if ((int)(kern_width-offset) > 0) \
        i_stop = kern_width-offset; \
    if ((int)(kern_height-offset) > 0) \
        j_stop = kern_height-offset; \
    if ((int)(kern_depth-offset) > 0) \
        k_stop = kern_depth-offset; \
\
    nhood = kimage_new (kimage_type(input), kimage_nchannels(input), \
                        kimage_ndim(input), kzip(b, kern_width, \
                        kern_height, kern_depth)); \
    kimage_get_neighborhood (input, nhood, kzip(b, input_width, \
                             input_height, input_depth)); \
    type* nhood_px = kimage_raw_pixels(nhood); \
\
    for (k=offset; k < k_stop; k++) \
    for (j=offset; j < j_stop; j++) \
    for (i=offset; i < i_stop; i++) \
        if ( fabs(nhood_px[k*kern_height*kern_width+j*kern_width+i] - \
                (type)(255.0*kernel_px[ k*kern_height*kern_width \
                                       +j*kern_width+i])) > DELTA ) { \
            kimage_release(input); \
            kimage_release(nhood); \
            return err_int+i; \
        } \
\
    kimage_release(input); \
    kimage_release(nhood); \
} while (0)


#define SETUP_EDGE_HANDLING(type, gblur, img, kernel, err_code) \
do { \
   /* Check extend edge_handle */ \
    kblock_set_option (gblur, "edge_handle", "extend"); \
    kblock_execute (gblur); \
\
    img = kblock_get_output (gblur, "output"); \
    CHECK_OUTPUT_IMAGE(type, img, kernel, 0, err_code); \
\
    /* Check wrap edge_handle */ \
    kblock_set_option (gblur, "edge_handle", "wrap"); \
    kblock_execute (gblur); \
\
    img = kblock_get_output (gblur, "output"); \
    CHECK_OUTPUT_IMAGE(type, img, kernel, 0, err_code); \
\
    /* Check crop edge_handle */ \
    kblock_set_option (gblur, "edge_handle", "crop"); \
    kblock_execute (gblur); \
\
    img = kblock_get_output (gblur, "output"); \
    CHECK_OUTPUT_IMAGE(type, img, kernel, 4, err_code); \
\
} while (0)

void make_test_images (KPixelType type, KImage* image)
{
    switch (type) {
        case char_px:
            MAKE_TEST_IMAGE(unsigned char, image);
            break;
        case uint_px:
            MAKE_TEST_IMAGE(unsigned int, image);
            break;
        case int_px:
            MAKE_TEST_IMAGE(int, image);
            break;
        case float_px:
            MAKE_TEST_IMAGE(float, image);
            break;
        default:
            break;
    }
}


KErrorCode check_output_images (KPixelType type, KBlockContext* gblur, KImage* kernel, int err_code)
{
    KImage* img;

    switch (type) {
        case char_px:
            SETUP_EDGE_HANDLING(unsigned char, gblur, img, kernel, err_code);
            break;
        case uint_px:
            SETUP_EDGE_HANDLING(unsigned int, gblur, img, kernel, err_code);
            break;
        case int_px:
            SETUP_EDGE_HANDLING(int, gblur, img, kernel, err_code);
            break;
        case float_px:
            SETUP_EDGE_HANDLING(float, gblur, img, kernel, err_code);
            break;
        default:
            return UNSUPPORTED_PIXEL_TYPE;
    }

    return SUCCESS;
}


KErrorCode test_XDGaussian (KBlockContext* gblur, KBlockContext* gkern)
{
    int t, i, j, k, ndim, depth, height, width;
    KImage* img;
    KImage* kernel;
    KErrorCode err;
    float* k_px;
    int b[3];

    KPixelType types [] = {
        char_px,
        uint_px,
        int_px,
        float_px,
    };

    b[0] = 1; b[1] = 1; b[2] = 1;
    for (ndim = 0; ndim < 3; ndim++) {
        b[ndim] = 15;

        /* create gaussian kernel */
        kblock_set_option (gkern, "sigma", 2.0);
        kblock_set_option (gkern, "dimension", ndim+1);
        kblock_execute (gkern);
        kernel = kblock_get_output (gkern, "output");
        kblock_set_input (gblur, "kernel", kernel);

        for (t = 0; t < 4; t++) {
            img = kimage_new (types[t], 1, ndim+1, b);
            make_test_images (types[t], img);
            kblock_set_input (gblur, "input", img);
            err = check_output_images (types[t], gblur, kernel, 10*ndim+20);

            if (err) {
                kimage_release (img);
                kimage_release (kernel);
                return err;
            }
            kimage_release (img);
        }
        kimage_release (kernel);
    }
    return SUCCESS;
}


KErrorCode test_error_codes (KBlockContext* gblur, KBlockContext* gkern)
{
    KImage *img, *kernel;
    KErrorCode err;
    int b[2];

    /* No inputs */
    err = kblock_execute(gblur);
    if (err != NULL_INPUT) {
        return NULL_INPUT;
    }

    /* Input image only */
    img = kimage_new (float_px, 1, 2, kzip(b, 15, 15));
    make_test_images (float_px, img);
    kblock_set_input (gblur, "input", img);

    err = kblock_execute(gblur);
    if (err != NULL_INPUT) {
        kimage_release (img);
        return NULL_INPUT;
    }

    /* Image and kernel have mismatched dimensions */
    kblock_set_option (gkern, "sigma", 2.0);
    kblock_set_option (gkern, "dimensions", 1);
    kblock_execute (gkern);
    kernel = kblock_get_output (gkern, "output");
    kblock_set_input (gblur, "kernel", kernel);

    err = kblock_execute(gblur);
    if (err != INVALID_INPUT) {
        kimage_release (img);
        kimage_release (kernel);
        return INVALID_INPUT;
    }

    /* Input kernel only */
    kimage_release (img);
    kblock_set_input (gblur, "input", NULL);

    err = kblock_execute(gblur);
    if (err != NULL_INPUT) {
        kimage_release (kernel);
        return NULL_INPUT;
    }

    /* Invalid edge handling string */
    img = kimage_new (float_px, 1, 1, kzip(b, 15, 1));
    make_test_images (float_px, img);
    kblock_set_input (gblur, "input", img);
    kblock_set_option (gblur, "edge_handle", "tacos");
    err = kblock_execute (gblur);
    if (err != BAD_OPTION_VALUE) {
        kimage_release (img);
        kimage_release (kernel);
        return BAD_OPTION_VALUE;
    }

    kimage_release (img);
    kimage_release (kernel);
    return SUCCESS;
}


int main (int argc, char** argv)
{
    int i;
    KBlockContext* gblur, *gkern;
    KErrorCode err;
    KImage* kernel;

    kmodule_load_bank ();

    /* generate gaussian kernel */
    gkern = kblock_new_context ("gausskernel");
    if (!gkern) {
        return NO_CONTEXT;
    }

    /* convolve the image */
    gblur = kblock_new_context ("convolve");
    if (!gblur) {
        kblock_free_context (gkern);
        return NO_CONTEXT;
    }

    err = test_error_codes(gblur, gkern);
    if (err != SUCCESS) {
        goto clean_up;
    }

    err = test_XDGaussian (gblur, gkern);
    if (err != SUCCESS) {
        goto clean_up;
    }

clean_up:
    kblock_free_context (gblur);
    kblock_free_context (gkern);
    kmodule_unload_bank ();
    return err;
}
