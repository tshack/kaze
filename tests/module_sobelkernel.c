#include <stdio.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"

int main (int argc, char** argv)
{
    int i, j, size;
    KBlockContext* skern;
    KErrorCode err;
    KImage* kernelx;
    KImage* kernely;
    float* kernelx_pix;
    float* kernely_pix;

    kmodule_load_bank ();

    /* generate gaussian kernel */
    skern = kblock_new_context ("sobelkernel");
    if (!skern) {
        return NO_CONTEXT;
    }

    int sobel_3x3[]  = {
             -1, -2, -1,
              0,  0,  0,
              1,  2,  1,
    };

    size = 3;
    kblock_set_option (skern, "size", size);
    kblock_execute (skern);
    kernelx = kblock_get_output (skern, "outputX");
    kernely = kblock_get_output (skern, "outputY");

    kernelx_pix = (float*) kimage_raw_pixels (kernelx);
    kernely_pix = (float*) kimage_raw_pixels (kernely);
    if ( kimage_width (kernelx) != size
         || kimage_height (kernelx) != size
         || kimage_width (kernely) != size
         || kimage_height (kernely) != size) {
        err = DIMENSION_MISMATCH;
        goto clean_up;
    }

    for (j = 0; j < size; j++) {
    for (i = 0; i < size; i++) {
        if ( kernelx_pix[j*size+i] != sobel_3x3[i*size+j]
             || kernely_pix[j*size+i] != sobel_3x3[j*size+i] ) {
            return 20+i;
        }
    }
    }

    kimage_release (kernelx);
    kimage_release (kernely);

    int sobel_5x5[]  = {
             -1,  -4,  -6,  -4,  -1,
             -2,  -8, -12,  -8,  -2,
              0,   0,   0,   0,   0,
              2,   8,  12,   8,   2,
              1,   4,   6,   4,   1,
    };

    size = 5;
    kblock_set_option (skern, "size", size);
    kblock_execute (skern);
    kernelx = kblock_get_output (skern, "outputX");
    kernely = kblock_get_output (skern, "outputY");

    kernelx_pix = (float*) kimage_raw_pixels (kernelx);
    kernely_pix = (float*) kimage_raw_pixels (kernely);
    if ( kimage_width (kernelx) != size
         || kimage_height (kernelx) != size
         || kimage_width (kernely) != size
         || kimage_height (kernely) != size) {
        err = DIMENSION_MISMATCH;
        goto clean_up;
    }

    for (j = 0; j < size; j++) {
    for (i = 0; i < size; i++) {
        if ( kernelx_pix[j*size+i] != sobel_5x5[i*size+j]
             || kernely_pix[j*size+i] != sobel_5x5[j*size+i] ) {
            return 30+i;
        }
    }
    }

    err = SUCCESS;

clean_up:
    kblock_free_context (skern);
    kimage_release (kernelx);
    kimage_release (kernely);
    kmodule_unload_bank ();
    return err;
}
