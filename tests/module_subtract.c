#include <float.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kpixel.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}

#define TEST(px_type, native_type, min, npix) \
do { \
    int n; \
    KBlockContext* subtract; \
    KImage* img_r; \
    native_type* r; \
    int buff[1]; \
\
    KImage* img_a = kimage_new (px_type, 1, 1, kzip(buff, npix)); \
    KImage* img_b = kimage_new (px_type, 1, 1, kzip(buff, npix)); \
    KImage* img_s = kimage_new (px_type, 1, 1, kzip(buff, npix)); \
\
    native_type* a = kimage_raw_pixels (img_a); \
    native_type* b = kimage_raw_pixels (img_b); \
    native_type* s = kimage_raw_pixels (img_s); \
\
    /* setup inputs and compute the correct solution */ \
    for (n=0; n<npix; n++) { \
        a[n] = n; \
        b[n] = npix - n; \
        s[n] = b[n] + min < a[n] ? a[n] - b[n] : min; \
    } \
\
    /* run the subtract module */ \
    subtract = kblock_new_context ("subtract"); \
    if (!subtract) \
        exit (EXIT_FAILURE); \
    kblock_error_handler (subtract, error_handler, NULL); \
    kblock_set_input (subtract, "minuend", img_a); \
    kblock_set_input (subtract, "subtrahend", img_b); \
    kblock_execute (subtract); \
    img_r = kblock_get_output (subtract, "output"); \
    r = kimage_raw_pixels (img_r); \
\
    /* compare solution with result */ \
    for (n=0; n<npix; n++) { \
        if (r[n] != s[n]) { \
            fprintf (stderr, "failed subtracting %s\n", #px_type); \
            exit (EXIT_FAILURE); \
        } \
    } \
\
    kimage_release (img_r); \
    kimage_release (img_s); \
    kimage_release (img_b); \
    kimage_release (img_a); \
    kblock_free_context (subtract);\
} while (0);


static void check_errors ()
{
    KErrorCode err;
    KBlockContext* subtract;
    int b[3];

    KImage* A = NULL;
    KImage* B = NULL;

    subtract = kblock_new_context ("subtract");

    /* check null inputs */

    kblock_set_input (subtract, "minuend", A);
    kblock_set_input (subtract, "subtrahend", B);

    err = kblock_execute (subtract);
    if (err != NULL_INPUT) {
        fprintf (stderr, "failure: check_errors(): NULL_INPUT\n");
        exit (EXIT_FAILURE);
    }

    /* check dimension mismatch 1 */

    A = kimage_new (char_px, 1, 2, kzip(b, 100, 100));
    B = kimage_new (char_px, 1, 3, kzip(b, 100, 100, 100));

    kblock_set_input (subtract, "minuend", A);
    kblock_set_input (subtract, "subtrahend", B);

    err = kblock_execute (subtract);
    if (err != DIMENSION_MISMATCH) {
        fprintf (stderr, "failure: check_errors(): DIMENSION_MISMATCH 1\n");
        exit (EXIT_FAILURE);
    }

    /* check dimension mismatch 2 */

    kimage_release (B);
    B = kimage_new (char_px, 1, 2, kzip(b, 100, 10));
    kblock_set_input (subtract, "subtrahend", B);

    err = kblock_execute (subtract);
    if (err != DIMENSION_MISMATCH) {
        fprintf (stderr, "failure: check_errors(): DIMENSION_MISMATCH 2\n");
        exit (EXIT_FAILURE);
    }

    /* check pixel type mismatch */

    kimage_release (B);
    B = kimage_new (float_px, 1, 2, kzip(b, 100, 100));
    kblock_set_input (subtract, "subtrahend", B);

    err = kblock_execute (subtract);
    if (err != PIXEL_TYPE_MISMATCH) {
        fprintf (stderr, "failure: check_errors(): PIXEL_TYPE_MISMATCH\n");
        exit (EXIT_FAILURE);
    }

    /* check # channels mismatch */

    kimage_release (B);
    B = kimage_new (char_px, 2, 2, kzip(b, 100, 100));
    kblock_set_input (subtract, "subtrahend", B);

    err = kblock_execute (subtract);
    if (err != CHANNEL_MISMATCH) {
        fprintf (stderr, "failure: check_errors(): CHANNEL_MISMATCH\n");
        exit (EXIT_FAILURE);
    }

    kimage_release (A);
    kimage_release (B);
    kblock_free_context (subtract);
}


int main (int argc, char** argv)
{
    kmodule_load_bank ();

    check_errors ();

    TEST (char_px,  unsigned char, 0, 255);
    TEST (uint_px,  unsigned int,  0, 65535);
    TEST (int_px,   int,           INT_MIN, 65535);
    TEST (float_px, float,         -FLT_MAX, 10000000);

    kmodule_unload_bank ();

    return 0;
}
