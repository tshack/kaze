/* Each check returns SUCCESS if the module system returns
 * the correct error code corresponding to the intentionally
 * induced fault.
 *
 * If the fault doesn't get caught, then the error code that
 * should be returned by the check is returned.
 *
 * The return code of this test program can be checked to
 * easily figure out which of the checks failed.
 */

#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


/* Checks if the returned error is what was expected.  If not,
 * exit with the KErrorCode we expected
 */
static void error_handler (KErrorCode err, void* userdata)
{
    KErrorCode expect = (KErrorCode)userdata;

    fprintf (stderr, "Expected Error:\n");
    fprintf (stderr, "  %s\n", kerror(expect));
    fprintf (stderr, "Received Error:\n");
    fprintf (stderr, "  %s\n", kerror(err));
    fprintf (stderr, "---\n");

    if (err != expect)
        exit((int)expect);
}


static void init_image (KImage* img)
{
    int n;

    unsigned char* pixels = kimage_raw_pixels(img);

    for (n=0; n<kimage_npix(img); n++) {
        pixels[n] = 128;
    }
}


/* CHECK: INVALID_INPUT
 *
 *   Supplying an invalid input name to a module context should
 *   result in kblock_set_input() returning INVALID_INPUT
 */
static void check_invalid_input (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)INVALID_INPUT);

    kblock_set_input (threshold, "iambatman", img);

    kblock_free_context (threshold);
}


/* CHECK: WRONG_IO_DIRECTION
 *
 *   Supplying an output name to kblock_set_input() should
 *   result in kblock_set_input() returning WRONG_IO_DIRECTION
 */
static void check_wrong_io_direction_input (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)WRONG_IO_DIRECTION);

    kblock_set_input (threshold, "output", img);

    kblock_free_context (threshold);
}


/* CHECK: WRONG_IO_DIRECTION
 *
 *   Supplying an input name to kblock_get_output() should
 *   result in kblock_get_output() returning NULL and throwing
 *   a WRONG_IO_DIRECTION to the registered error handler.
 */
static void check_wrong_io_direction_output (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)WRONG_IO_DIRECTION);

    kblock_get_output (threshold, "input");

    kblock_free_context (threshold);
}


/* CHECK: INVALID_OUTPUT
 *
 *   Supplying an invalid output name to a module context should
 *   result in kblock_get_output() returning INVALID_OUTPUT
 */
static void check_invalid_output (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)INVALID_OUTPUT);

    kblock_get_output (threshold, "iambatman");

    kblock_free_context (threshold);
}


/* CHECK: INVALID_OPTION
 *
 *   Supplying an invalid option name to a module context should
 *   result in kblock_set_option() returning INVALID_OPTION
 */
static void check_invalid_option (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)INVALID_OPTION);

    kblock_set_input (threshold, "input", img);

    kblock_set_option (threshold, "iambatman", 23);

    kblock_free_context (threshold);
}

/* CHECK: BAD_OPTION_VALUE
 *
 *   Supplying an invalid (i.e. out of range) value for an option
 *   should result in kblock_set_option() returning BAD_OPTION_VALUE
 */
static void check_bad_option_value (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)BAD_OPTION_VALUE);

    kblock_set_input (threshold, "input", img);

    /* N.b. "threshold" value is bound within [0, 255] */
    kblock_set_option (threshold, "threshold", 9000);

    kblock_free_context (threshold);
}

/* CHECK: module execution return codes
 *
 *   The KBlock system should propagate error codes returned
 *   by executing module algorithms back through kblock_execute()
 *   This test makes sure this facility is working correctly.
 */
static void check_execute_return (KImage* img)
{
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");

    kblock_error_handler (threshold, error_handler, (void*)NULL_INPUT);

    /* N.b. The "threshold" module cannot accept a NULL KImage input */
    kblock_set_input (threshold, "input", NULL);
    kblock_set_option (threshold, "threshold", 10);
    kblock_set_option (threshold, "fill_value", 0);
    kblock_set_option (threshold, "direction", "above");

    kblock_execute (threshold);

    kblock_free_context (threshold);
}


int main (int argc, char** argv)
{
    int b[2];

    KImage* img = kimage_new(char_px, 1, 2, kzip(b, 48, 48));

    kmodule_load_bank ();
    init_image (img);

    check_invalid_input (img);
    check_wrong_io_direction_input (img);
    check_wrong_io_direction_output (img);
    check_invalid_output (img);
    check_invalid_option (img);
    check_bad_option_value (img);
    check_execute_return (img);

    kimage_release (img);

    kmodule_unload_bank ();

    return 0;
}
