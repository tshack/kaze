#include <stdio.h>
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


static KImage* generate_image (unsigned int width, unsigned int height)
{
    int i, j, c;
    int b[2];

    KImage* img = kimage_new (char_px, 3, 2, kzip(b, width, height));
    unsigned char* pixels = kimage_raw_pixels(img);

    for (c=0; c<3; c++)
        for (j=0; j<height; j++)
            for (i=0; i<width; i++)
                pixels[width*height*c + width*j + i] = (i*j) % 255;

    return img;
}



static KErrorCode compare (KImage* a, KImage* b)
{
    unsigned int i, j;

    unsigned char* pix_a = kimage_raw_pixels(a);
    unsigned char* pix_b = kimage_raw_pixels(b);
    unsigned int width  = kimage_width(a);
    unsigned int height = kimage_height(b);

    for (j=0; j<height; j++)
        for (i=0; i<width; i++)
            if (pix_a[j*width+i] != pix_b[j*width+i])
                return FAILURE;

    return SUCCESS;
}


int main (int argc, char** argv)
{
    unsigned int i, j;
    KImage *img1, *img2;

    unsigned int width  = 1024;
    unsigned int height = 768;

    kmodule_load_bank ();

    /* generate a test image */
    img1 = generate_image (width, height);

    /* write the generated img1 to disk as test.tiff */
    if (kimage_write ("test.tiff", img1) != SUCCESS) {
        return (int)FILE_ERROR;
    }

    /* now read test.tiff back from disk into img2 */
    img2 = kimage_read ("test.tiff");
    if (!img2) {
        return (int)FILE_ERROR;
    }

    /* compare what we read back to what we generated */
    if (compare (img1, img2)) {
        return (int)FAILURE;
    }

    kimage_release(img1);
    kimage_release(img2);

    kmodule_unload_bank ();

    return (int)SUCCESS;
}
