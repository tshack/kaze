/* Each check returns SUCCESS if the module system returns
 * the correct error code corresponding to the intentionally
 * induced fault.
 *
 * If the fault doesn't get caught, then the error code that
 * should be returned by the check is returned.
 *
 * The return code of this test program can be checked to
 * easily figure out which of the checks failed.
 */

#include <stdlib.h>

#include "kblock.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


static void init_image (KImage* img)
{
    int n;

    unsigned char* pixels = kimage_raw_pixels(img);

    for (n=0; n<kimage_npix(img); n++) {
        pixels[n] = 128;
    }
}

/* Checks if the returned error is what was expected.  If so,
 * replace it with SUCCESS.  If not, replace it with the error
 * that we did expect
 */
static void check_and_set (KErrorCode* err, KErrorCode expect)
{
    if (*err == expect)
        *err = SUCCESS;
    else
        *err = expect;
}


/* CHECK: NO_CONTEXT
 *
 *   Supplying an invalid module name to kblock_new_context()
 *   should result in receiving a NULL pointer in return.
 */
static KErrorCode check_invalid_module (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("iambatman");
    if (!threshold)
        err = SUCCESS;
    else
        err = NO_CONTEXT;

    kblock_free_context (threshold);

    return err;
}


/* CHECK: INVALID_INPUT
 *
 *   Supplying an invalid input name to a module context should
 *   result in kblock_set_input() returning INVALID_INPUT
 */
static KErrorCode check_invalid_input (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    err = kblock_set_input (threshold, "iambatman", img);
    check_and_set (&err, INVALID_INPUT);

    kblock_free_context (threshold);

    return err;
}


/* CHECK: WRONG_IO_DIRECTION
 *
 *   Supplying an output name to kblock_set_input() should
 *   result in kblock_set_input() returning WRONG_IO_DIRECTION
 */
static KErrorCode check_wrong_io_direction_input (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    err = kblock_set_input (threshold, "output", img);
    check_and_set (&err, WRONG_IO_DIRECTION);

    kblock_free_context (threshold);

    return err;
}


/* CHECK: WRONG_IO_DIRECTION
 *
 *   Supplying an input name to kblock_get_output() should
 *   result in kblock_get_output() returning NULL and throwing
 *   a WRONG_IO_DIRECTION to the registered error handler.
 */
static KErrorCode check_wrong_io_direction_output (KImage* img)
{
    KBlockContext* threshold;
    void* out;

    KErrorCode err = FAILURE;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    out = kblock_get_output (threshold, "input");
    if (!out)
        err = SUCCESS;

    kblock_free_context (threshold);

    return err;
}


/* CHECK: INVALID_OUTPUT
 *
 *   Supplying an invalid output name to a module context should
 *   result in kblock_get_output() returning INVALID_OUTPUT
 */
static KErrorCode check_invalid_output (KImage* img)
{
    KBlockContext* threshold;
    void* out;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    out = kblock_get_output (threshold, "iambatman");

    kblock_free_context (threshold);

    if (!out)
        return SUCCESS;

    return FAILURE;
}


/* CHECK: INVALID_OPTION
 *
 *   Supplying an invalid option name to a module context should
 *   result in kblock_set_option() returning INVALID_OPTION
 */
static KErrorCode check_invalid_option (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    kblock_set_input (threshold, "input", img);

    err = kblock_set_option (threshold, "iambatman", 23);
    check_and_set (&err, INVALID_OPTION);

    kblock_free_context (threshold);

    return err;
}

/* CHECK: BAD_OPTION_VALUE
 *
 *   Supplying an invalid (i.e. out of range) value for an option
 *   should result in kblock_set_option() returning BAD_OPTION_VALUE
 */
static KErrorCode check_bad_option_value (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    kblock_set_input (threshold, "input", img);

    /* N.b. "threshold" value is bound within [0, 255] */
    err = kblock_set_option (threshold, "threshold", 9000);
    check_and_set (&err, BAD_OPTION_VALUE);

    kblock_free_context (threshold);

    return err;
}

/* CHECK: module execution return codes
 *
 *   The KBlock system should propagate error codes returned
 *   by executing module algorithms back through kblock_execute()
 *   This test makes sure this facility is working correctly.
 */
static KErrorCode check_execute_return (KImage* img)
{
    KErrorCode err;
    KBlockContext* threshold;

    threshold = kblock_new_context ("threshold");
    if (!threshold)
        return NO_CONTEXT;

    /* N.b. The "threshold" module cannot accept a NULL KImage input */
    kblock_set_input (threshold, "input", NULL);
    kblock_set_option (threshold, "threshold", 10);
    kblock_set_option (threshold, "fill_value", 0);
    kblock_set_option (threshold, "direction", "above");

    err = kblock_execute (threshold);
    check_and_set (&err, NULL_INPUT);

    kblock_free_context (threshold);

    return err;
}

int main (int argc, char** argv)
{
    KErrorCode err;
    int b[2];

    KImage* img = kimage_new(char_px, 1, 2, kzip(b, 48, 48));

    kmodule_load_bank ();
    init_image (img);

    err = check_invalid_module (img);
    if (err)
        return err;

    err = check_invalid_input (img);
    if (err)
        return err;

    err = check_wrong_io_direction_input (img);
    if (err)
        return err;

    err = check_wrong_io_direction_output (img);
    if (err)
        return err;

    err = check_invalid_output (img);
    if (err)
        return err;

    err = check_invalid_option (img);
    if (err)
        return err;

    err = check_bad_option_value (img);
    if (err)
        return err;

    err = check_execute_return (img);
    if (err)
        return err;

    kimage_release (img);

    kmodule_unload_bank ();

    return 0;
}
