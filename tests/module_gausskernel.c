#include <stdio.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

int main (int argc, char** argv)
{
    int i, j, k, ndim;
    KBlockContext* gkern;
    KErrorCode err;
    KImage* kernel, *gauss_vals2D;
    float* kernel_px, *gauss_vals2D_px;
    int b[2];

    kmodule_load_bank ();

    /* generate known kernel values for 1D and 2D */
    float gauss_vals1D[]  = {
             0.0022159242, 0.0087641502, 0.0269954832, 0.0647587978, 0.1209853622,
             0.1760326633, 0.1994711402, 0.1760326633, 0.1209853622, 0.0647587978,
             0.0269954832, 0.0087641502, 0.0022159242,
    };

    gauss_vals2D = kimage_new (float_px, 1, 2, kzip(b, 13, 13));
    gauss_vals2D_px = kimage_raw_pixels(gauss_vals2D);

    for (j = 0; j < kimage_height(gauss_vals2D); j++)
    for (i = 0; i < kimage_width(gauss_vals2D) ; i++)
        gauss_vals2D_px[j*kimage_width(gauss_vals2D)+i] =  gauss_vals1D[i]
                                                          *gauss_vals1D[j];
    
    /* generate gaussian kernel */
    gkern = kblock_new_context ("gausskernel");
    if (!gkern) {
        return NO_CONTEXT;
    }

    /* 1D gaussian kernel check */
    kblock_set_option (gkern, "sigma", 2.0);
    kblock_execute (gkern);
    kernel = kblock_get_output (gkern, "output");
    kernel_px = (float*) kimage_raw_pixels (kernel);

    if ( kimage_width (kernel) != 13 ) {
        err = DIMENSION_MISMATCH;
        goto clean_up;
    }

    for (i = 0; i < kimage_width (kernel); i++) {
        if (fabs (kernel_px[i] - gauss_vals1D[i]) > 0.000001) {
            err = 30+i;
            goto clean_up;
        }
    }
    kimage_release (kernel);

    /* 2D gaussian kernel check */
    kblock_set_option (gkern, "sigma", 2.0);
    kblock_set_option (gkern, "dimension", 2);
    kblock_execute (gkern);
    kernel = kblock_get_output (gkern, "output");
    kernel_px = (float*) kimage_raw_pixels (kernel);

    if ( kimage_width (kernel) != 13 ) {
        err = DIMENSION_MISMATCH;
        goto clean_up;
    }

    if ( kimage_height (kernel) != 13 ) {
        err = DIMENSION_MISMATCH;
        goto clean_up;
    }

    for (j = 0; j < kimage_height (kernel); j++)
    for (i = 0; i < kimage_width (kernel) ; i++)
        if (fabs ( kernel_px[j*kimage_width(kernel)+i]
                  -gauss_vals2D_px[j*kimage_width(gauss_vals2D)+i] )
            > 0.000001) {
            err = 40+i;
            goto clean_up;
        }

    err = SUCCESS;

clean_up:
    kblock_free_context (gkern);
    kimage_release (gauss_vals2D);
    kimage_release (kernel);
    kmodule_unload_bank ();
    return err;
}
