#include <stdlib.h>
#include <stdio.h>

#include "kimage.h"
#include "kzipper.h"


static void print_image (KImage* img, const char* title)
{
    int i, j;
    int b[2];

    fprintf (stderr, "%s:\n", title);
    for (j=0; j<kimage_dim(img, 1); j++) {
        for (i=0; i<kimage_dim(img, 0); i++) {
            fprintf (stderr, "%2i ", kimage_pixel(img, 0, kzip(b, i, j)).int_px);
        }
        fprintf (stderr, "\n");
    }
    fprintf (stderr, "\n");
}


static void check (KImage* img, unsigned char* sol)
{
    int i, j;
    int b[2];

    unsigned int xdim = kimage_dim (img, 0);
    unsigned int ydim = kimage_dim (img, 1);

    for (j=0; j<ydim; j++)
        for (i=0; i<xdim; i++)
            if (kimage_pixel (img, 0, kzip(b, i, j)).char_px != sol[j*xdim+i])
                exit (EXIT_FAILURE);
}


static void generate_image (KImage** img)
{
    int i, j;
    int b[2];

    *img   = kimage_new (char_px, 1, 2, kzip(b, 20, 10));

    for (j=0; j<kimage_dim(*img, 1); j++)
        for (i=0; i<kimage_dim(*img, 0); i++)
            kimage_set_pixel (*img, (KPixel)(i+j), 0, kzip(b, i, j));

    print_image (*img, "Full Pixel Data");
}


static void test_normal (KImage* img)
{
    KImage* nhood;
    KErrorCode err;
    int b[2];

    unsigned char correct[] = {
         8,  9, 10,
         9, 10, 11,
        10, 11, 12
    };

    nhood = kimage_new (kimage_type(img), kimage_nchannels(img),
                kimage_ndim(img), kzip(b, 3, 3));

    err = kimage_get_neighborhood (img, nhood, kzip(b, 6, 4));
    if (err) {
        fprintf (stderr, "error -- %s [%i]\n", kerror(err), (unsigned int)err);
        exit (EXIT_FAILURE);
    }

    print_image (nhood, "3x3 @ (6, 4)");
    check (nhood, correct);

    kimage_release(nhood);
}


static void test_topleft (KImage* img)
{
    KImage* nhood;
    KErrorCode err;
    int b[2];

    unsigned char correct[] = {
         0,  0,  0,  1,  2,
         0,  0,  0,  1,  2,
         0,  0,  0,  1,  2,
         1,  1,  1,  2,  3,
         2,  2,  2,  3,  4
    };

    nhood = kimage_new (kimage_type(img), kimage_nchannels(img),
                kimage_ndim(img), kzip(b, 5, 5));

    err = kimage_get_neighborhood (img, nhood, kzip(b, 0, 0));
    if (err) {
        fprintf (stderr, "error -- %s [%i]\n", kerror(err), (unsigned int)err);
        exit (EXIT_FAILURE);
    }

    print_image (nhood, "5x5 @ (0, 0)");
    check (nhood, correct);

    kimage_release(nhood);
}


static void test_bottomright (KImage* img)
{
    KImage* nhood;
    KErrorCode err;
    int b[2];

    unsigned char correct[] = {
        24, 25, 26, 26, 26,
        25, 26, 27, 27, 27,
        26, 27, 28, 28, 28,
        26, 27, 28, 28, 28,
        26, 27, 28, 28, 28
    };

    nhood = kimage_new (kimage_type(img), kimage_nchannels(img),
                kimage_ndim(img), kzip(b, 5, 5));

    err = kimage_get_neighborhood (img, nhood, kzip(b, 19, 9));
    if (err) {
        fprintf (stderr, "error -- %s [%i]\n", kerror(err), (unsigned int)err);
        exit (EXIT_FAILURE);
    }

    print_image (nhood, "5x5 @ (19, 9)");
    check (nhood, correct);

    kimage_release(nhood);
}


int main (int argc, char** argv)
{
    KImage* img;

    generate_image (&img);
    test_normal (img);
    test_topleft (img);
    test_bottomright (img);

    kimage_release(img);
    return EXIT_SUCCESS;
}
