#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kmodule.h"
#include "kimage.h"
#include "kzipper.h"


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


static void print_image (KImage* img, const char* title)
{
    int i, j, c;
    int b[2];

    fprintf (stderr, "%s:\n", title);
    for (c=0; c<kimage_nchannels(img); c++) {
        for (j=0; j<kimage_dim(img, 1); j++) {
            for (i=0; i<kimage_dim(img, 0); i++) {
                fprintf (stderr, "%2i, ", kimage_pixel(img, c, kzip(b, i, j)).int_px);
            }
            fprintf (stderr, "\n");
        }
        fprintf (stderr, "\n");
    }
    fprintf (stderr, "\n");
}


static void check (KImage* img, unsigned char* sol)
{
    int i, j, c;
    int b[2];

    unsigned int xdim = kimage_dim (img, 0);
    unsigned int ydim = kimage_dim (img, 1);

    for (c=0; c<kimage_nchannels(img); c++)
        for (j=0; j<ydim; j++)
            for (i=0; i<xdim; i++)
                if (kimage_pixel (img, c, kzip(b, i, j)).char_px != sol[xdim*ydim*c+xdim*j+i])
                    exit (EXIT_FAILURE);
}


static void generate_2d_image (KImage** img, unsigned int nchannels)
{
    int i, j, c;
    int b[2];

    *img   = kimage_new (char_px, nchannels, 2, kzip(b, 20, 10));

    for (c=0; c<nchannels; c++)
        for (j=0; j<kimage_dim(*img, 1); j++)
            for (i=0; i<kimage_dim(*img, 0); i++)
                kimage_set_pixel (*img, (KPixel)(i+j+c), c, kzip(b, i, j));

    print_image (*img, "Full Pixel Data");
}


static void test_2d_1c (KImage* img)
{
    KBlockContext* average;
    KImage* out;

    unsigned char correct[] = {
        1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 18, 19,
        2,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        3,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 20,
        4,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 21,
        5,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 22,
        6,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 23,
        7,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 24,
        8,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25,
        8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 26,
        9, 10, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
    };

    average = kblock_new_context ("average");
    if (!average) {
        fprintf (stderr, "error -- unable to load module\n");
        exit (EXIT_FAILURE);
    }

    kblock_error_handler (average, error_handler, NULL);

    kblock_set_input (average, "input", img);
    kblock_set_option (average, "radius", 5);
    kblock_execute (average);
    out = kblock_get_output (average, "output");

    print_image (out, "out");

    check (out, correct);

    kimage_release (out);
    kblock_free_context (average);
}


static void test_2d_2c (KImage* img)
{
    KBlockContext* average;
    KImage* out;

    unsigned char correct[] = {
        1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 18, 19,
        2,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
        3,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 20,
        4,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 21,
        5,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 22,
        6,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 23,
        7,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 24,
        8,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25,
        8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 26,
        9, 10, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,

        2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 19, 20,
        3,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
        4,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 21,
        5,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 22,
        6,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 23,
        7,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 24,
        8,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 25,
        9,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 26,
        9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 27,
        10, 11, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28
    };

    average = kblock_new_context ("average");
    if (!average) {
        fprintf (stderr, "error -- unable to load module\n");
        exit (EXIT_FAILURE);
    }

    kblock_error_handler (average, error_handler, NULL);

    kblock_set_input (average, "input", img);
    kblock_set_option (average, "radius", 5);
    kblock_execute (average);
    out = kblock_get_output (average, "output");

    print_image (out, "out");

    check (out, correct);

    kimage_release (out);
    kblock_free_context (average);
}


int main (int argc, char** argv)
{
    KImage* img;

    kmodule_load_bank ();

    generate_2d_image (&img, 1);
    test_2d_1c (img);
    kimage_release(img);

    generate_2d_image (&img, 2);
    test_2d_2c (img);
    kimage_release(img);

    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
