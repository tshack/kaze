#include <stdlib.h>
#include <stdio.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"


static void make_test_images (
    KImage** test,
    KImage** correct,
    unsigned char width,
    unsigned char height,
    unsigned char threshold,
    unsigned char fill_value
)
{
    unsigned char i, j, val;
    unsigned int n;
    unsigned char* test_pix;
    unsigned char* correct_pix;
    int b[2];

    *test = kimage_new (char_px, 1, 2, kzip(b, width, height));
    *correct = kimage_new (char_px, 1, 2, kzip(b, width, height));

    test_pix = kimage_raw_pixels(*test);
    correct_pix = kimage_raw_pixels(*correct);

    for (j=0; j<height; j++) {
        for (i=0; i<width; i++) {
            n = j*width+i;
            val = i * (255/width);
            test_pix[n] = val;
            if (val < threshold)
                correct_pix[n] = fill_value;
            else
                correct_pix[n] = val;
        }
    }
}


static KErrorCode compare_images (
    KImage* output,
    KImage* correct,
    unsigned char width,
    unsigned char height
)
{
    int i, j, n;
    unsigned char* output_pix;
    unsigned char* correct_pix;

    output_pix = kimage_raw_pixels(output);
    correct_pix = kimage_raw_pixels(correct);

    for (j=0; j<height; j++) {
        for (i=0; i<width; i++) {
            n = j*width+i;
            if (correct_pix[n] != output_pix[n])
                return FAILURE;
        }
    }

    return SUCCESS;
}


static void error_handler (KErrorCode err, void* userdata)
{
    fprintf (stderr, "%s\n", kerror(err));
    exit(EXIT_FAILURE);
}


int main (int argc, char** argv)
{
    KImage* test;
    KImage* correct;
    KImage* module_output;
    KErrorCode err;
    KBlockContext* threshold;

    unsigned char width = 250;    /* test image width  */
    unsigned char height = 250;   /* test image height */
    unsigned char tval = 60;      /* threshold to use  */
    unsigned char fval = 60;      /* fill value to use */

    kmodule_load_bank ();

    make_test_images (&test, &correct, width, height, tval, fval);

    /* general module setup */
    threshold = kblock_new_context ("threshold");
    if (!threshold) {
        fprintf (stderr, "error -- unable to load module\n");
        return EXIT_FAILURE;
    }
    kblock_error_handler (threshold, error_handler, NULL);
    kblock_set_input (threshold, "input", test);
    kblock_set_option (threshold, "threshold", tval);
    kblock_set_option (threshold, "fill_value", fval);

    /* this module execution should give the *CORRECT* answer */
    kblock_set_option (threshold, "direction", "below");
    kblock_execute (threshold);
    module_output = kblock_get_output (threshold, "output");
    err = compare_images (module_output, correct, width, height);
    if (err == FAILURE)
        return EXIT_FAILURE;
    kimage_release (module_output);

    /* this module execution should give the *WRONG* answer */
    kblock_set_option (threshold, "direction", "above");
    kblock_execute (threshold);
    module_output = kblock_get_output (threshold, "output");
    err = compare_images (module_output, correct, width, height);
    if (err != FAILURE)
        return EXIT_FAILURE;
    kimage_release (module_output);

    /* cleanup */
    kblock_free_context (threshold);
    kimage_release (test);
    kimage_release (correct);
    kmodule_unload_bank ();

    return EXIT_SUCCESS;
}
