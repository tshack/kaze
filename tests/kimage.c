#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "kpixel.h"
#include "kimage.h"

#define DELTA 0.001

/*
 * TODO: We need to add pixel content tests.
 *        -- test clone explicitly (!!)
 *        -- basic write & read-back tests
 *        -- range clamping on kimage_convert() tests
 */

struct KTestImage {
    char* name;
    KPixelType type;
    unsigned int nchannels;
    unsigned int ndim;
    unsigned int dims[5];
};

static const unsigned int ntestpx = 9;
/* Test pixel values for [char_px] with expected conversion results */
static const unsigned char tp_char_px[]       = {   0,    1,    5,    10,    20,    40,    80,    160,    255};
static const unsigned int  tp_char_px_uint[]  = {   0,    1,    5,    10,    20,    40,    80,    160,    255};
static const int           tp_char_px_int[]   = {   0,    1,    5,    10,    20,    40,    80,    160,    255};
static const float         tp_char_px_float[] = {0.0f, 1.0f, 5.0f, 10.0f, 20.0f, 40.0f, 80.0f, 160.0f, 255.0f};

/* Test pixel values for [uint_px] with expected conversion results */
static const unsigned int  tp_uint_px[]       = {   0,    10,    20,    40,    80,    160,    320,    640,    1280};
static const unsigned char tp_uint_px_char[]  = {   0,    10,    20,    40,    80,    160,    255,    255,     255};
static const int           tp_uint_px_int[]   = {   0,    10,    20,    40,    80,    160,    320,    640,    1280};
static const float         tp_uint_px_float[] = {0.0f, 10.0f, 20.0f, 40.0f, 80.0f, 160.0f, 320.0f, 640.0f, 1280.0f};

/* Test pixel values for [int_px] with expected conversion results */
static const int           tp_int_px[]        = {   -500,    -100,    -1,     0,    10,    200,    500,    1000,     5000};
static const unsigned char tp_int_px_char[]   = {      0,       0,     0,     0,    10,    200,    255,     255,      255};
static const unsigned int  tp_int_px_uint[]   = {      0,       0,     0,     0,    10,    200,    500,    1000,     5000};
static const float         tp_int_px_float[]  = {-500.0f, -100.0f, -1.0f,  0.0f, 10.0f, 200.0f, 500.0f, 1000.0f,  5000.0f};

/* Test pixel values for [float_px] with expected conversion results */
static const float         tp_float_px[]      = {-543.2f, -456.7f, -0.5f,  0.0f,  0.5f, 234.9f, 567.2f, 1234.5f,  5432.0f};
static const unsigned char tp_float_px_char[] = {      0,       0,     0,     0,     0,    234,    255,     255,      255};
static const unsigned int  tp_float_px_uint[] = {      0,       0,     0,     0,     0,    234,    567,    1234,     5432};
static const int           tp_float_px_int[]  = {   -543,    -456,     0,     0,     0,    234,    567,    1234,     5432};



/*  Not a test.  Simply fills pixels with known values
 */
void fill_pixels (KImage* img)
{
    unsigned long n;
    void* pixels = kimage_raw_pixels (img);
    unsigned long total_pix = kimage_npix(img)*kimage_nchannels(img);

    switch (kimage_type (img)) {
    case char_px:
        for (n=0; n<total_pix; n++)
            ((unsigned char*)pixels)[n] = tp_char_px[n % ntestpx];

        break;
    case uint_px:
        for (n=0; n<total_pix; n++)
            ((unsigned int*)pixels)[n] = tp_uint_px[n % ntestpx];

        break;
    case int_px:
        for (n=0; n<total_pix; n++)
            ((int*)pixels)[n] = tp_int_px[n % ntestpx];

        break;
    case float_px:
        for (n=0; n<total_pix; n++)
            ((float*)pixels)[n] = tp_float_px[n % ntestpx];

        break;
    }
}


/*  Make sure KImages have the correct KPixel type
 */
static KErrorCode check_type (KImage* img, struct KTestImage* test)
{
    KErrorCode err;

    fprintf (stderr, "-- check_type:   ");

    if (kimage_type(img) == test->type) {
        fprintf (stderr, "PASS\n");
        err = SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        err = FAILURE;
    }

    return err;
}


/*  Make sure KImages report the correct # of dimensions
 */
static KErrorCode check_ndim (KImage* img, struct KTestImage* test)
{
    KErrorCode err;

    fprintf (stderr, "-- check_ndim:   ");

    if (kimage_ndim (img) == test->ndim) {
        fprintf (stderr, "PASS\n");
        err = SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        err = PIXEL_TYPE_MISMATCH;
    }

    return err;
}


/*  Make sure KImages report the correct dimension ranges
 */
static KErrorCode check_dims (KImage* img, struct KTestImage* test)
{
    unsigned int d;

    fprintf (stderr, "-- check_dims:   ");

    for (d=0; d<kimage_ndim(img); d++)
        if (kimage_dim (img, d) != test->dims[d]) {
            fprintf (stderr, "FAIL\n");
            return FAILURE;
        }

    fprintf (stderr, "PASS\n");
    return SUCCESS;
}


/*  Make sure KImages report the correct number of pixels
 */
static KErrorCode check_npix (KImage* img, struct KTestImage* test)
{
    KErrorCode err;
    unsigned int npix, d;

    npix = kimage_dim (img, 0);
    for (d=1; d<kimage_ndim(img); d++)
        npix *= kimage_dim (img, d);

    fprintf (stderr, "-- check_npix:   ");

    if (kimage_npix(img) == npix) {
        fprintf (stderr, "PASS\n");
        err = SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        err = FAILURE;
    }

    return err;
}


/*  Make sure KImages report the correct width
 */
static KErrorCode check_width (KImage* img, struct KTestImage* test)
{
    fprintf (stderr, "-- check_width:  ");

    if (kimage_width(img) == test->dims[0]) {
        fprintf (stderr, "PASS\n");
        return SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        return FAILURE;
    }
}


/*  Make sure KImages report the correct height
 */
static KErrorCode check_height (KImage* img, struct KTestImage* test)
{
    fprintf (stderr, "-- check_height: ");

    if (kimage_height(img) == test->dims[1]) {
        fprintf (stderr, "PASS\n");
        return SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        return FAILURE;
    }
}


/*  Make sure KImages report the correct depth
 */
static KErrorCode check_depth (KImage* img, struct KTestImage* test)
{
    fprintf (stderr, "-- check_depth:  ");

    /* TODO: reconsider if this should be proper behavior */
    if (kimage_ndim (img) < 3)
        if (kimage_depth (img) == 1) {
            fprintf (stderr, "PASS\n");
            return SUCCESS;
        }

    if (kimage_depth(img) == test->dims[2]) {
        fprintf (stderr, "PASS\n");
        return SUCCESS;
    } else {
        fprintf (stderr, "FAIL\n");
        return FAILURE;
    }
}


/*  Make sure KImage pixels convert as expected
 */
static KErrorCode check_pixels (KImage* img, KImage* clone)
{
    unsigned long n;
    unsigned char* char_pixels;
    unsigned int* uint_pixels;
    int* int_pixels;
    float* float_pixels;

    fprintf (stderr, "-- check_convert: ");

    switch (kimage_type (img)) {
    case char_px:
        switch (kimage_type (clone)) {
        case char_px:
            char_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (char_pixels[n] != tp_char_px[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case uint_px:
            uint_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (uint_pixels[n] != tp_char_px_uint[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case int_px:
            int_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (int_pixels[n] != tp_char_px_int[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case float_px:
            float_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (fabsf(float_pixels[n] - tp_char_px_float[n % ntestpx]) > DELTA) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        }
        break;

    case uint_px:
        switch (kimage_type (clone)) {
        case char_px:
            char_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (char_pixels[n] != tp_uint_px_char[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case uint_px:
            uint_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (uint_pixels[n] != tp_uint_px[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case int_px:
            int_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (int_pixels[n] != tp_uint_px_int[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case float_px:
            float_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (fabsf(float_pixels[n] - tp_uint_px_float[n % ntestpx]) > DELTA) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        }
        break;

    case int_px:
        switch (kimage_type (clone)) {
        case char_px:
            char_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (char_pixels[n] != tp_int_px_char[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case uint_px:
            uint_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (uint_pixels[n] != tp_int_px_uint[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case int_px:
            int_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (int_pixels[n] != tp_int_px[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case float_px:
            float_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (fabsf(float_pixels[n] - tp_int_px_float[n % ntestpx]) > DELTA) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        }
        break;

    case float_px:
        switch (kimage_type (clone)) {
        case char_px:
            char_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (char_pixels[n] != tp_float_px_char[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case uint_px:
            uint_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (uint_pixels[n] != tp_float_px_uint[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case int_px:
            int_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (int_pixels[n] != tp_float_px_int[n % ntestpx]) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        case float_px:
            float_pixels = kimage_raw_pixels (clone);
            for (n=0; n<kimage_npix(clone); n++)
                if (fabsf(float_pixels[n] - tp_float_px[n % ntestpx]) > DELTA) {
                    fprintf (stderr, "FAIL [test pixel: %lu]\n", n % ntestpx);
                    return FAILURE;
                }
            break;
        }
        break;
    }

    fprintf (stderr, "PASS\n");
    return SUCCESS;
}

/*  Return a string of the supplied KPixelType
 */
char* ktype (KPixelType type)
{
    switch (type) {
    case char_px:
        return "char_px";
    case float_px:
        return "float_px";
    case uint_px:
        return "uint_px";
    case int_px:
        return "int_px";
    default:
        return "(UNKNOWN TYPE)";
    }
}


int main (int argc, char** argv)
{
    KErrorCode err;
    KImage* img;
    KImage* clone;
    unsigned int n;
    int t;

    struct KTestImage test_images[] = {
        /* Name        PixType   Channels  Dims   Edge Lengths */
        { "Image 1",    char_px,  1,        2,     {128, 256} },
        { "Image 2",    char_px,  2,        2,     {128, 256} },
        { "Image 3",    char_px,  1,        3,     {128, 128, 128} },
        { "Image 4",    char_px,  3,        4,     {128, 128, 128, 10} },

        { "Image 5",   float_px,  1,        2,     {128, 256} },
        { "Image 6",   float_px,  2,        2,     {128, 256} },
        { "Image 7",   float_px,  1,        3,     {128, 128, 128} },
        { "Image 8",   float_px,  3,        4,     {128, 128, 128, 10} },

        { "Image 9",    uint_px,  1,        2,     {128, 256} },
        { "Image 10",   uint_px,  2,        2,     {128, 256} },
        { "Image 11",   uint_px,  1,        3,     {128, 128, 128} },
        { "Image 12",   uint_px,  3,        4,     {128, 128, 128, 10} },

        { "Image 13",    int_px,  1,        2,     {128, 256} },
        { "Image 14",    int_px,  2,        2,     {128, 256} },
        { "Image 15",    int_px,  1,        3,     {128, 128, 128} },
        { "Image 16",    int_px,  3,        4,     {128, 128, 128, 10} },
        { NULL }
    };

    KPixelType types [] = {
        char_px,
        uint_px,
        int_px,
        float_px,
    };

    for (n=0; test_images[n].name; n++) {
        fprintf (stderr, "=================================================\n");
        fprintf (stderr, " Running Tests for %s\n", test_images[n].name);
        fprintf (stderr, "=================================================\n");

        img = kimage_new (test_images[n].type, test_images[n].nchannels,
                           test_images[n].ndim, test_images[n].dims);

        if (!img) {
            fprintf (stderr, "Failed to create new KImage (%s)\n",
                             test_images[n].name);
            return FAILURE;
        }

        fill_pixels (img);

        for (t=-1; t<4; t++) {
            clone = kimage_clone (img);

            if (t != -1) {
                fprintf (stderr, "\nConverting Pixels: %s -> %s\n",
                                 ktype(kimage_type(clone)), ktype(types[t]));
                fprintf (stderr, "----------------------------------------\n");
                kimage_convert (clone, types[t]);
                test_images[n].type = types[t];
            }

            err = check_type (clone, &test_images[n]);
            if (err)
                return err;

            err = check_ndim (clone, &test_images[n]);
            if (err)
                return err;

            err = check_dims (clone, &test_images[n]);
            if (err)
                return err;

            err = check_npix (clone, &test_images[n]);
            if (err)
                return err;

            err = check_width (clone, &test_images[n]);
            if (err)
                return err;

            err = check_height (clone, &test_images[n]);
            if (err)
                return err;

            err = check_depth (clone, &test_images[n]);
            if (err)
                return err;

            err = check_pixels (img, clone);
            if (err)
                return err;

            kimage_release (clone);
        }

    kimage_release (img);

    }

    return 0;
}
