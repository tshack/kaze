#include <math.h>
#include <stdlib.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

KErrorCode test_3DDownsample (KBlockContext* dwnsmp, int* downsampled_int, float* downsampled_ave)
{
    int t, i, j, k, depth, height, width;
    KImage* img;
    KImage* img2;
    unsigned char* img_pixelsc;
    unsigned char* img2_pixelsc;
    unsigned int* img_pixelsu;
    unsigned int* img2_pixelsu;
    int* img_pixelsi;
    int* img2_pixelsi;
    float* img_pixelsf;
    float* img2_pixelsf;
    KErrorCode err;
    int b[3];

    KPixelType types [] = {
        char_px,
        uint_px,
        int_px,
        float_px,
    };

    for (t = 0; t < 4; t++) {
        depth  = 6;
        height = 6;
        width  = 6;
        switch (types[t]) {
            case char_px:
                img = kimage_new (types[t], 1, 3, kzip(b, width, height, depth));
                img_pixelsc = kimage_raw_pixels(img);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            img_pixelsc[k*height*width+j*width+i] = i+j*width+k*height*width;

                break;
            case uint_px:
                img = kimage_new (types[t], 1, 3, kzip(b, width, height, depth));
                img_pixelsu = kimage_raw_pixels(img);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            img_pixelsu[k*height*width+j*width+i] = i+j*width+k*height*width;

                break;
            case int_px:
                img = kimage_new (types[t], 1, 3, kzip(b, width, height, depth));
                img_pixelsi = kimage_raw_pixels(img);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            img_pixelsi[k*height*width+j*width+i] = i+j*width+k*height*width;

                break;
            case float_px:
                img = kimage_new (types[t], 1, 3, kzip(b, width, height, depth));
                img_pixelsf = kimage_raw_pixels(img);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            img_pixelsf[k*height*width+j*width+i] = i+j*width+k*height*width;

                break;
            default:
                return UNSUPPORTED_PIXEL_TYPE;
        }

        kblock_set_option (dwnsmp, "averaging", "disable");
        kblock_set_input (dwnsmp, "input", img);
        kblock_set_option (dwnsmp, "factor", 0.5);
        err = kblock_execute (dwnsmp);
        img2 = kblock_get_output (dwnsmp, "output");

        depth  = kimage_depth(img2);
        height = kimage_height(img2);
        width  = kimage_width(img2);
        switch (types[t]) {
            case char_px:
                img2_pixelsc = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if ( img2_pixelsc[k*height*width+j*width+i] != downsampled_int[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                depth  = kimage_depth(img2);
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsc = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if (img2_pixelsc[k*height*width+j*width+i] != (unsigned char) downsampled_ave[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);
                break;
            case uint_px:
                img2_pixelsu = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if ( img2_pixelsu[k*height*width+j*width+i] != downsampled_int[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                depth  = kimage_depth(img2);
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsu = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if (img2_pixelsu[k*height*width+j*width+i] != (unsigned int) downsampled_ave[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);
                break;
            case int_px:
                img2_pixelsi = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if ( img2_pixelsi[k*height*width+j*width+i] != downsampled_int[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                depth  = kimage_depth(img2);
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsi = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if (img2_pixelsi[k*height*width+j*width+i] != (int) downsampled_ave[k*height*width+j*width+i]) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);
                break;
            case float_px:
                img2_pixelsf = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if ( fabs(img2_pixelsf[k*height*width+j*width+i] - downsampled_int[k*height*width+j*width+i]) > 0.0001 ) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                depth  = kimage_depth(img2);
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsf = kimage_raw_pixels(img2);
                for (k = 0; k < depth; k++)
                    for (j = 0; j < height; j++)
                        for (i = 0; i < width; i++)
                            if ( fabs(img2_pixelsf[k*height*width+j*width+i] - downsampled_ave[k*height*width+j*width+i]) > 0.0001 ) {
                                err = 40+i;
                                goto finish_downsample;
                            }

                kimage_release(img2);
                break;
            default:
                err = UNSUPPORTED_PIXEL_TYPE;
                goto finish_downsample;
        }
        kimage_release (img);
    }

    return SUCCESS;

finish_downsample:
    kimage_release(img2);
img1:
    kimage_release(img);
    return err;
}

KErrorCode test_2DDownsample (KBlockContext* dwnsmp, int* downsampled_int, float* downsampled_ave)
{
    int t, i, j, width, height;
    KImage* img;
    KImage* img2;
    unsigned char* img_pixelsc;
    unsigned char* img2_pixelsc;
    unsigned int* img_pixelsu;
    unsigned int* img2_pixelsu;
    int* img_pixelsi;
    int* img2_pixelsi;
    float* img_pixelsf;
    float* img2_pixelsf;
    KErrorCode err;
    int b[2];

    KPixelType types [] = {
        char_px,
        uint_px,
        int_px,
        float_px,
    };


    for (t = 0; t < 4; t++) {
        width = 6;
        height = 6;
        switch (types[t]) {
            case char_px:
                img = kimage_new (types[t], 1, 2, kzip(b, width, height));
                img_pixelsc = kimage_raw_pixels(img);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        img_pixelsc[j*width+i] = i+j*width;

                break;
            case uint_px:
                img = kimage_new (types[t], 1, 2, kzip(b, width, height));
                img_pixelsu = kimage_raw_pixels(img);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        img_pixelsu[j*width+i] = i+j*width;

                break;
            case int_px:
                img = kimage_new (types[t], 1, 2, kzip(b, width, height));
                img_pixelsi = kimage_raw_pixels(img);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        img_pixelsi[j*width+i] = i+j*width;

                break;
            case float_px:
                img = kimage_new (types[t], 1, 2, kzip(b, width, height));
                img_pixelsf = kimage_raw_pixels(img);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        img_pixelsf[j*width+i] = i+j*width;

                break;
            default:
                return UNSUPPORTED_PIXEL_TYPE;
        }

        kblock_set_option (dwnsmp, "averaging", "disable");
        kblock_set_input (dwnsmp, "input", img);
        kblock_set_option (dwnsmp, "factor", 0.5);
        err = kblock_execute (dwnsmp);
        img2 = kblock_get_output (dwnsmp, "output");

        height = kimage_height(img2);
        width  = kimage_width(img2);
        switch (types[t]) {
            case char_px:
                img2_pixelsc = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if ( img2_pixelsc[j*width+i] != downsampled_int[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsc = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if (img2_pixelsc[j*width+i] != (unsigned char) downsampled_ave[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);
                break;
            case uint_px:
                img2_pixelsu = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if ( img2_pixelsu[j*width+i] != downsampled_int[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsu = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if (img2_pixelsu[j*width+i] != (unsigned int) downsampled_ave[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);
                break;
            case int_px:
                img2_pixelsi = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if ( img2_pixelsi[j*width+i] != downsampled_int[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsi = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if (img2_pixelsi[j*width+i] != (int) downsampled_ave[j*width+i]) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);
                break;
            case float_px:
                img2_pixelsf = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if ( fabs(img2_pixelsf[j*width+i] - downsampled_int[j*width+i]) > 0.0001 ) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                height = kimage_height(img2);
                width  = kimage_width(img2);

                img2_pixelsf = kimage_raw_pixels(img2);
                for (j = 0; j < height; j++)
                    for (i = 0; i < width; i++)
                        if ( fabs(img2_pixelsf[j*width+i] - downsampled_ave[j*width+i]) > 0.0001 ) {
                            err = 30+i;
                            goto finish_downsample;
                        }

                kimage_release(img2);
                break;
            default:
                err = UNSUPPORTED_PIXEL_TYPE;
                goto finish_downsample;
        }
        kimage_release (img);
    }

    return SUCCESS;

finish_downsample:
    kimage_release(img2);
img1:
    kimage_release(img);
    return err;
}

KErrorCode test_1DDownsample (KBlockContext* dwnsmp, int* downsampled_int, float* downsampled_ave)
{
    int t, i, width;
    KImage* img;
    KImage* img2;
    unsigned char* img_pixelsc;
    unsigned char* img2_pixelsc;
    unsigned int* img_pixelsu;
    unsigned int* img2_pixelsu;
    int* img_pixelsi;
    int* img2_pixelsi;
    float* img_pixelsf;
    float* img2_pixelsf;
    KErrorCode err;
    int b[1];

    KPixelType types [] = {
        char_px,
        uint_px,
        int_px,
        float_px,
    };

    for (t = 0; t < 4; t++) {
        width = 6;
        switch (types[t]) {
            case char_px:
                img = kimage_new (types[t], 1, 1, kzip(b, width));
                img_pixelsc = kimage_raw_pixels(img);
                for (i = 0; i < kimage_width(img); i++)
                    img_pixelsc[i] = (unsigned char) i;

                break;
            case uint_px:
                img = kimage_new (types[t], 1, 1, kzip(b, width));
                img_pixelsu = kimage_raw_pixels(img);
                for (i = 0; i < kimage_width(img); i++)
                    img_pixelsu[i] = (unsigned int) i;

                break;
            case int_px:
                img = kimage_new (types[t], 1, 1, kzip(b, width));
                img_pixelsi = kimage_raw_pixels(img);
                for (i = 0; i < kimage_width(img); i++)
                    img_pixelsi[i] = (int) i;

                break;
            case float_px:
                img = kimage_new (types[t], 1, 1, kzip(b, width));
                img_pixelsf = kimage_raw_pixels(img);
                for (i = 0; i < kimage_width(img); i++)
                    img_pixelsf[i] = (float) i*1.0;

                break;
            default:
                return UNSUPPORTED_PIXEL_TYPE;
        }

        /* test default averaging */
        kblock_set_option (dwnsmp, "averaging", "disable");
        kblock_set_input (dwnsmp, "input", img);
        kblock_set_option (dwnsmp, "factor", 0.5);
        err = kblock_execute (dwnsmp);
        img2 = kblock_get_output (dwnsmp, "output");

        width = kimage_width(img2);
        switch (types[t]) {
            case char_px:
                img2_pixelsc = kimage_raw_pixels(img2);
                for (i = 0; i < width; i++) {
                    if ( img2_pixelsc[i] != downsampled_int[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                width = kimage_width(img2);

                img2_pixelsc = kimage_raw_pixels(img2);
                for (i = 0; i < kimage_width(img2); i++) {
                    if ( img2_pixelsc[i] != (unsigned char) downsampled_ave[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);
                break;
            case uint_px:
                img2_pixelsu = kimage_raw_pixels(img2);
                for (i = 0; i < width; i++) {
                    if ( img2_pixelsu[i] != downsampled_int[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                width = kimage_width(img2);

                img2_pixelsu = kimage_raw_pixels(img2);
                for (i = 0; i < kimage_width(img2); i++) {
                    if ( img2_pixelsu[i] != (unsigned int) downsampled_ave[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);
                break;
            case int_px:
                img2_pixelsi = kimage_raw_pixels(img2);
                for (i = 0; i < width; i++) {
                    if ( img2_pixelsi[i] != downsampled_int[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                width = kimage_width(img2);

                img2_pixelsi = kimage_raw_pixels(img2);
                for (i = 0; i < kimage_width(img2); i++) {
                    if ( img2_pixelsi[i] != (int) downsampled_ave[i]) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);
                break;
            case float_px:
                img2_pixelsf = kimage_raw_pixels(img2);
                for (i = 0; i < width; i++) {
                    if ( fabs(img2_pixelsf[i] - (float)downsampled_int[i]) > 0.0001 ) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);

                /* test enable averaging */
                kblock_set_option (dwnsmp, "averaging", "enable");
                kblock_set_option (dwnsmp, "factor", 0.666666667);
                err = kblock_execute (dwnsmp);
                img2 = kblock_get_output (dwnsmp, "output");
                width = kimage_width(img2);

                img2_pixelsf = kimage_raw_pixels(img2);
                for (i = 0; i < kimage_width(img2); i++) {
                    if ( fabs(img2_pixelsf[i] - downsampled_ave[i]) > 0.0001 ) {
                        err = 20+i;
                        goto finish_downsample;
                    }
                }
                kimage_release(img2);
                break;
            default:
                err = UNSUPPORTED_PIXEL_TYPE;
                goto finish_downsample;
        }
        kimage_release (img);
    }

    return SUCCESS;

finish_downsample:
    kimage_release (img2);
img1:
    kimage_release (img);
    return err;
}

int main (int argc, char** argv)
{
    int i;
    KBlockContext* dwnsmp;
    KErrorCode err;
    KImage* kernel;

    kmodule_load_bank ();

    /* generate downsample context */
    dwnsmp = kblock_new_context ("downsample");
    if (!dwnsmp) {
        return NO_CONTEXT;
    }

 /*
    err = kblock_execute(dwnsmp);
    if (err != NULL_INPUT) {
        err = NULL_INPUT;
        goto clean_up;
    }
*/

    int downsampled_int []  = {
           0,   2,   4,
          12,  14,  16,
          24,  26,  28,

          72,  74,  76,
          84,  86,  88,
          96,  98, 100,

         144, 146, 148,
         156, 158, 160,
         168, 170, 172
    };

    float downsampled_ave [] = {
        0.0f,   1.0f,   2.5f,   4.0f,
        6.0f,   7.0f,   8.5f,  10.0f,
       15.0f,  16.0f,  17.5f,  19.0f,
       24.0f,  25.0f,  26.5f,  28.0f,

       36.0f,  37.0f,  38.5f,  40.0f,
       42.0f,  43.0f,  44.5f,  46.0f,
       51.0f,  52.0f,  53.5f,  55.0f,
       60.0f,  61.0f,  62.5f,  64.0f,

       90.0f,  91.0f,  92.5f,  94.0f,
       96.0f,  97.0f,  98.5f, 100.0f,
      105.0f, 106.0f, 107.5f, 109.0f,
      114.0f, 115.0f, 116.5f, 118.0f,

      144.0f, 145.0f, 146.5f, 148.0f,
      150.0f, 151.0f, 152.5f, 154.0f,
      159.0f, 160.0f, 161.5f, 163.0f,
      168.0f, 169.0f, 170.5f, 172.0f
    };

    err = test_1DDownsample (dwnsmp,downsampled_int, downsampled_ave);
    if (err != SUCCESS) {
        goto clean_up;
    }

    err = test_2DDownsample (dwnsmp, downsampled_int, downsampled_ave);
    if (err != SUCCESS) {
        goto clean_up;
    }

    err = test_3DDownsample (dwnsmp, downsampled_int, downsampled_ave);
    if (err != SUCCESS) {
        goto clean_up;
    }

    err = SUCCESS;

clean_up:
    kblock_free_context (dwnsmp);
    kmodule_unload_bank ();
    return err;
}
