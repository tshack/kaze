#include <stdio.h>
#include <string.h>
#include "kerror.h"

int main (int argc, char** argv)
{
    KErrorCode err;

    for (err=SUCCESS; err<_NUM_KERROR_CODES; err++)
        if (!strcmp (kerror(err), "(UNKNOWN) Encountered an unknown error!"))
            return 1;

    if (strcmp (kerror(_NUM_KERROR_CODES), "(UNKNOWN) Encountered an unknown error!"))
        return 1;
    
    return 0;
}
