#include <stdio.h>
#include <stdlib.h>
#include "kzipper.h"

int int_test_vector[] = {-7, 8, -9};
unsigned int uint_test_vector[] = {9, 0, 1};
double double_test_vector[] = {3.14, 8675.309, 13.37};


void test_uint (unsigned int* A)
{
    int i;

    int n = sizeof(uint_test_vector)/sizeof(*uint_test_vector);

    for (i=0; i<n; i++)
        if (A[i] != uint_test_vector[i]) {
            fprintf (stderr, "test_uint() failed\n");
            exit (EXIT_FAILURE);
        }
}

void test_int (int* A)
{
    int i;

    int n = sizeof(int_test_vector)/sizeof(*int_test_vector);

    for (i=0; i<n; i++)
        if (A[i] != int_test_vector[i]) {
            fprintf (stderr, "test_int() failed\n");
            exit (EXIT_FAILURE);
        }
}

void test_double (double* A)
{
    int i;

    int n = sizeof(double_test_vector)/sizeof(*double_test_vector);

    for (i=0; i<n; i++)
        if (A[i] != double_test_vector[i]) {
            fprintf (stderr, "test_double() failed\n");
            exit (EXIT_FAILURE);
        }
}



int main (int argc, char** argv)
{
    int A[sizeof(int_test_vector)/sizeof(*int_test_vector)];
    unsigned int B[sizeof(uint_test_vector)/sizeof(*uint_test_vector)];
    double C[sizeof(double_test_vector)/sizeof(*double_test_vector)];

    test_int (kzip(A, int_test_vector[0], int_test_vector[1],
                int_test_vector[2]));
    test_uint (kzip(B, uint_test_vector[0], uint_test_vector[1],
                uint_test_vector[2]));
    test_double (kzip(C, double_test_vector[0], double_test_vector[1],
                double_test_vector[2]));

    return 0;
}
