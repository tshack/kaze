macro(cache_var var msg)
    set (${var} ${${var}} CACHE STRING "${msg}")
    mark_as_advanced (${var})
endmacro()

macro(append var item)
    if (NOT ${var} STREQUAL "")
        set (${var} "${${var}}" "${item}")
    else ()
        set (${var} "${item}")
    endif ()
endmacro()

macro(get_dirlist dest parent_dir)
    file(GLOB children RELATIVE ${parent_dir} ${parent_dir}/*)
    set(dirlist "")
    foreach(child ${children})
        if(IS_DIRECTORY ${parent_dir}/${child})
            list(APPEND dirlist ${child})
        endif()
    endforeach()
    set(${dest} ${dirlist})
endmacro()

macro(add_module src_files mode)
    get_filename_component (module_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    # this set_property will yield dropdown combo boxes in cmake-gui & ccmake
    set (BUILD_MODULE_${module_name} "${mode}" CACHE STRING "(Internal/Module/Disabled)")
    set_property(CACHE BUILD_MODULE_${module_name} PROPERTY STRINGS Internal Module Disabled)

    # export module name to C/C++ as __MODULE__
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC -D__MODULE__='${module_name}'")
    set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS} -fPIC -D__MODULE__='${module_name}'")

    if (BUILD_MODULE_${module_name} MATCHES "^[Ii]")
        message (STATUS "(+) Module: " ${module_name})
        set (INTERNAL_MODULES_INIT_CODE "${INTERNAL_MODULES_INIT_CODE}\n\tINIT_MODULE (${module_name});")
        add_library (${module_name} STATIC ${src_files})
        add_internal_module (${module_name})
    elseif (BUILD_MODULE_${module_name} MATCHES "^[Mm]")
        message (STATUS "(M) Module: " ${module_name})
        add_library (${module_name} MODULE ${src_files})
        set_target_properties(${module_name} PROPERTIES PREFIX "")
        set_target_properties(${module_name} PROPERTIES SUFFIX ".km")
    else ()
        message (STATUS "(-) Module: " ${module_name})
    endif()
endmacro()

macro(module_link_libraries libs)
    get_filename_component (module_name ${CMAKE_CURRENT_SOURCE_DIR} NAME)
    if (NOT BUILD_MODULE_${module_name} MATCHES "^[Dd]")
        target_link_libraries (${module_name} ${libs})
    endif ()
endmacro()

macro (add_internal_module module)
    get_property (is_defined GLOBAL PROPERTY INTERNAL_MODULES DEFINED)
    if (NOT is_defined)
        define_property (GLOBAL PROPERTY INTERNAL_MODULES
            BRIEF_DOCS "List of internal modules"
            FULL_DOCS "List of modules to be compiled into Kaze")
    endif ()
    set_property(GLOBAL APPEND PROPERTY INTERNAL_MODULES "${module}")
endmacro()

macro(new_test src_name test_name)
    add_executable ("tests/${src_name}" ${CMAKE_SOURCE_DIR}/tests/${src_name}.c)
    target_link_libraries ("tests/${src_name}" ${LIBS_KAZE_ALL})
    add_test (${test_name} "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/tests/${src_name}")
endmacro()

macro(create_output_dir dir)
    file (MAKE_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${dir})
endmacro()

macro(new_example src_name)
    add_executable ("examples/${src_name}" "${CMAKE_SOURCE_DIR}/examples/${src_name}.c")
    target_link_libraries ("examples/${src_name}" ${LIBS_KAZE_ALL})
endmacro()

macro(copy_module_to_examples)
    set(COPY_SRC "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/kaze.so")
    set(COPY_DST "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/examples/kaze.so")
    add_custom_command(
        OUTPUT "${COPY_DST}"
        COMMAND ${CMAKE_COMMAND} -E copy "${COPY_SRC}" "${COPY_DST}"
        DEPENDS "${COPY_SRC}"
    )
    add_custom_target(copy-kaze-module ALL DEPENDS ${COPY_DST})
    add_dependencies( copy-kaze-module kaze )
endmacro()

macro(copy_example_items loc items)
    create_output_dir (${loc})
    foreach(item IN LISTS ${items})
        set(EXAMPLE_IMAGE_DST "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${loc}/${item}")
        set(EXAMPLE_IMAGE_SRC "${CMAKE_SOURCE_DIR}/examples/images/${item}")
        set(EXAMPLE_IMAGES_DEP ${EXAMPLE_IMAGES_DEP} ${EXAMPLE_IMAGE_DST})
        add_custom_command(
            OUTPUT "${EXAMPLE_IMAGE_DST}"
            COMMAND ${CMAKE_COMMAND} -E copy "${EXAMPLE_IMAGE_SRC}" "${EXAMPLE_IMAGE_DST}"
            DEPENDS "${EXAMPLE_IMAGE_SRC}"
        )
    endforeach()
    add_custom_target(example-images ALL DEPENDS ${EXAMPLE_IMAGES_DEP})
endmacro()
