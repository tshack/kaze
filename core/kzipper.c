#include <stddef.h>
#include <stdarg.h>
#include "kzipper.h"


unsigned int* kzip_uint (size_t n, unsigned int* array, ...)
{
    size_t d;
    va_list ap;

    va_start (ap, array);

    for (d=0; d<n; d++)
       array[d] = va_arg (ap, unsigned int);

    va_end (ap);

    return array;
}


int* kzip_int (size_t n, int* array, ...)
{
    size_t d;
    va_list ap;

    va_start (ap, array);

    for (d=0; d<n; d++)
       array[d] = va_arg (ap, int);

    va_end (ap);

    return array;
}


double* kzip_double (size_t n, double* array, ...)
{
    size_t d;
    va_list ap;

    va_start (ap, array);

    for (d=0; d<n; d++)
       array[d] = va_arg (ap, double);

    va_end (ap);

    return array;
}
