#include <stdlib.h>
#include <string.h>

#include "kimage.h"
#include "klist.h"
#include "internal/kfileops.h"
#include "internal/kimage.h"


struct fops_entry {
    KFileOps* fops;
    KList link;
};

static KLIST_HEAD(fops_list);


static const char* _get_extension (const char* filename)
{
    const char* basename;
    const char* extension;

    /* file extension starts just after with the first
     * encountered '.' in a file's name (i.e. file.tar.gz => tar.gz) */
    basename = strrchr (filename, '/');
    if (basename)
        basename++;
    else
        basename = filename;

    extension = strchr (basename, '.');
    if (extension)
        return ++extension;
    else
        return NULL;
}


static KFileOps* _find_fops_entry (const char* filename)
{
    struct fops_entry *fe;

    const char* extension = _get_extension (filename);

    klist_for_each_entry (fe, &fops_list, link) {
        if (!strcmp (fe->fops->extension, extension))
            return fe->fops;
    }

    return NULL;
}


KErrorCode kimage_register_fops (KFileOps* fops)
{
    struct fops_entry *fe;

    klist_for_each_entry (fe, &fops_list, link)
        if (!strcmp (fe->fops->extension, fops->extension))
            return MODULE_ERROR;

    fe = malloc (sizeof(*fe));
    fe->fops = fops;
    klist_add (&fe->link, &fops_list);

    return SUCCESS;
}


KErrorCode kimage_unregister_fops (KFileOps* fops)
{
    struct fops_entry *fe;
    struct fops_entry *tmp;

    klist_for_each_entry_safe (fe, tmp, &fops_list, link) {
        if (fe->fops == fops) {
            klist_del (&fe->link);
            free (fe);
            return SUCCESS;
        }
    }

    return MODULE_ERROR;
}


KImage* kimage_read (const char* filename)
{
    KFileOps* fops = _find_fops_entry (filename);

    if (!fops)
        return NULL;

    return fops->reader (filename);
}


KErrorCode kimage_write (const char* filename, KImage* img)
{
    KFileOps* fops = _find_fops_entry (filename);

    if (!fops)
        return UNSUPPORTED_FORMAT;

    return fops->writer (filename, img);
}
