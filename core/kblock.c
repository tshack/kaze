#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "kblock.h"
#include "kerror.h"
#include "klist.h"


struct error_handler {
    void* data;
    void (*callback)(KErrorCode err, void* data);
};

struct block_entry {
    KBlock* block;
    KList link;
};

static KLIST_HEAD(block_list);


static KBlock* _find_block_entry (const char* block_name)
{
    struct block_entry *be;

    klist_for_each_entry (be, &block_list, link) {
        if (strcmp (be->block->name, block_name) == 0)
            return be->block;
    }

    return NULL;
}


static inline void _invoke_error_callback (KBlockContext* ctx, KErrorCode err)
{
    struct error_handler* eh = ctx->error_handler;

    if (eh->callback && err != SUCCESS)
        eh->callback (err, eh->data);
}

static inline void* _get_option_pointer (KBlockContext* ctx, int n)
{
    const KOption* const opts = ctx->block->options;

    return (void*)((char*)ctx->data + opts[n].offset);
}


static inline void** _get_io_pointer (KBlockContext* ctx, int n)
{
    const KIOTerminal* const io = ctx->block->io;

    return (void**)((char*)ctx->data + io[n].offset);
}


static inline void _free_string_options (KBlockContext* ctx)
{
    int n;
    char** string_opt_ptr;

    const KOption* const opts = ctx->block->options;

    if (opts != NULL)
        for (n=0; opts[n].name; n++) {
            if (opts[n].type == OPT_STRING) {
                string_opt_ptr = _get_option_pointer (ctx, n);
                free (*string_opt_ptr);
            }
        }
}


static inline KErrorCode _set_option_int (KBlockContext* ctx, int n, int val)
{
    const KOption* const opts = ctx->block->options;
    int* const int_opt_ptr = _get_option_pointer (ctx, n);

    if ( (val >= opts[n].min) && (val <= opts[n].max) ) {
        *int_opt_ptr = val;
        return SUCCESS;
    } else
        return BAD_OPTION_VALUE;
}


static inline KErrorCode _set_option_float (KBlockContext* ctx, int n, float val)
{
    const KOption* const opts = ctx->block->options;
    float* const float_opt_ptr = _get_option_pointer (ctx, n);

    if ( (val >= opts[n].min) && (val <= opts[n].max) ) {
        *float_opt_ptr = val;
        return SUCCESS;
    } else
        return BAD_OPTION_VALUE;
}


static inline KErrorCode _set_option_string (KBlockContext* ctx, int n, const char* str)
{
    char* tmp;

    const KOption* const opts = ctx->block->options;
    char** const string_opt_ptr = _get_option_pointer (ctx, n);

    if (*string_opt_ptr == NULL)
        tmp = malloc(strlen(str)+1);
    else
        tmp = realloc(*string_opt_ptr, strlen(str)+1);

    if (tmp == NULL)
        return MEM_FAIL;
    else
        *string_opt_ptr = tmp;

    strcpy (*string_opt_ptr, str);
    return SUCCESS;
}


static KErrorCode _set_block_default (KBlockContext* ctx, int n)
{
    const KOption* const opts = ctx->block->options;

    switch (opts[n].type) {
    case OPT_INT:
        return _set_option_int (ctx, n, opts[n].default_value.i64);

    case OPT_FLOAT:
        return _set_option_float (ctx, n, opts[n].default_value.flt);

    case OPT_STRING:
        if (opts[n].default_value.str == NULL)
            return SUCCESS;

        return _set_option_string (ctx, n, opts[n].default_value.str);
        break;

    default:
        return MODULE_ERROR;
    }
}


static KErrorCode _set_block_option (KBlockContext* ctx, int n, va_list ap)
{
    va_list cpy;
    KErrorCode err;

    const KOption* const opts = ctx->block->options;

    va_copy (cpy, ap);

    switch (opts[n].type) {
    case OPT_INT:
        err = _set_option_int (ctx, n, va_arg (ap, uint64_t));
        goto cleanup;

    case OPT_FLOAT:
        err = _set_option_float (ctx, n, va_arg (ap, double));
        goto cleanup;

    case OPT_STRING:
        err = _set_option_string (ctx, n, va_arg (ap, char*));
        goto cleanup;

    default:
        err = MODULE_ERROR;
        break;
    }

cleanup:
    va_end(cpy);
    return err;
}


static KErrorCode _init_block_context (KBlockContext* ctx)
{
    int i;

    KErrorCode err = SUCCESS;
    const KOption* const opts = ctx->block->options;

    ctx->data = calloc (1, ctx->block->data_size);
    if (!ctx->data)
        return MEM_FAIL;

    if (opts != NULL)
        for (i=0; opts[i].name; i++) {
            err = _set_block_default (ctx, i);
            if (err)
                goto cleanup;
        }

    if (ctx->block->setup)
        err = ctx->block->setup (ctx);

    if (!err)
        goto success;

cleanup:
    free (ctx->data);
success:
    return err;
}


KBlockContext* kblock_new_context (const char* block_name)
{
    KBlock* block;

    KBlockContext* ctx = NULL;
    struct error_handler* eh = NULL;

    block = _find_block_entry (block_name);
    if (!block)
        goto out;

    /* initialize elements to NULL to allows for optional
     * attributes in KBlockContext objects */
    ctx = calloc (1, sizeof(*ctx));
    if (!ctx)
        goto out;

    eh = calloc (1, sizeof(*eh));
    if (!eh)
        goto out_error_handler;

    ctx->error_handler = eh;
    ctx->block = block;

    if (block->data_size)
        if (_init_block_context (ctx))
            goto out_init_context;

    goto out;

out_init_context:
    free (eh);
out_error_handler:
    free (ctx);
    ctx = NULL;
out:
    return ctx;
}


void kblock_free_context (KBlockContext* ctx)
{
    if (!ctx)
        return;

    if (ctx->block->teardown)
        ctx->block->teardown (ctx);

    _free_string_options (ctx);

    if (ctx->data)
        free (ctx->data);

    free (ctx->error_handler);
    free (ctx);
}


KErrorCode kblock_execute (KBlockContext* ctx)
{
    KErrorCode err;

    if (!ctx)
        return NO_CONTEXT;

    err = ctx->block->algorithm(ctx);

    if (err)
        _invoke_error_callback (ctx, err);

    return err;
}


KErrorCode kblock_set_option (KBlockContext* ctx, const char* key, ...)
{
    int i;
    va_list ap;
    KErrorCode err;

    if (!ctx)
        return NO_CONTEXT;

    const KOption* const opts = ctx->block->options;

    if (ctx->block->options == NULL) {
        err = INVALID_OPTION;
        goto cleanup;
    }

    va_start (ap, key);

    for (i=0; opts[i].name; i++)
        if (!strcmp (opts[i].name, key)) {
            err = _set_block_option (ctx, i, ap);
            goto cleanup;
        }

    err = INVALID_OPTION;

cleanup:
    va_end (ap);
    _invoke_error_callback (ctx, err);
    return err;
}


KErrorCode kblock_set_input (KBlockContext* ctx, const char* key, ...)
{
    int i;
    va_list ap;
    void* io_buffer;
    KErrorCode err;

    if (!ctx)
        return NO_CONTEXT;

    const KIOTerminal* const io = ctx->block->io;

    if (io == NULL) {
        err = MODULE_ERROR;
        goto cleanup;
    }

    va_start (ap, key);
    io_buffer = va_arg (ap, void*);
    va_end (ap);

    for (i=0; io[i].name; i++)
        if (!strcmp (io[i].name, key)) {
            if (io[i].direction == INPUT) {
                *_get_io_pointer (ctx, i) = io_buffer;
                return SUCCESS;
            } else {
                err = WRONG_IO_DIRECTION;
                goto cleanup;
            }
        }

    err = INVALID_INPUT;

cleanup:
    _invoke_error_callback (ctx, err);
    return err;
}


void* kblock_get_output (KBlockContext* ctx, const char* key)
{
    int i;

    if (!ctx)
        return NULL;

    const KIOTerminal* const io = ctx->block->io;
    KErrorCode err = INVALID_OUTPUT;

    for (i=0; io[i].name; i++)
        if (!strcmp (io[i].name, key)) {
            if (io[i].direction == OUTPUT)
                return *_get_io_pointer (ctx, i);
            else {
                err = WRONG_IO_DIRECTION;
                break;
            }
        }

    _invoke_error_callback (ctx, err);

    return NULL;
}


void kblock_error_handler (
    KBlockContext* ctx,
    void (*error_callback)(KErrorCode err, void* error_data),
    void* error_data
)
{
    struct error_handler* eh;

    if (ctx) {
        eh = ctx->error_handler;
        eh->callback = error_callback;
        eh->data = error_data;
    }
}


KErrorCode kblock_register (KBlock* block)
{
    struct block_entry *be;

    klist_for_each_entry (be, &block_list, link)
        if (!strcmp (be->block->name, block->name))
            return MODULE_ERROR;

    be = malloc (sizeof (*be));
    be->block = block;
    klist_add (&be->link, &block_list);

    return SUCCESS;
}


KErrorCode kblock_unregister (KBlock* block)
{
    struct block_entry *be;
    struct block_entry *tmp;

    klist_for_each_entry_safe (be, tmp, &block_list, link) {
        if (be->block == block) {
            klist_del (&be->link);
            free (be);
            return SUCCESS;
        }
    }

    return MODULE_ERROR;
}
