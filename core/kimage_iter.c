#include <stdlib.h>

#include "kerror.h"
#include "kimage.h"


KErrorCode kimage_iterator_init (KImageIterator* iter, KImage* img)
{
    unsigned int d;

    if (!iter || !img)
        return NULL_INPUT;

    iter->ndim = kimage_ndim(img);

    iter->coords = malloc (3 * iter->ndim * sizeof(*iter->coords));
    if (!iter->coords)
        return MEM_FAIL;

    iter->min = &iter->coords[iter->ndim];
    iter->max = &iter->min[iter->ndim];

    for (d=0; d<iter->ndim; d++) {
        iter->coords[d] = 0;
        iter->min[d] = 0;
        iter->max[d] = kimage_dim(img, d);
    }

    return SUCCESS;
}


void kimage_iterator_incr (KImageIterator* iter)
{
    iter->coords[0]++;
}


int kimage_iterator_check (KImageIterator* iter)
{
    unsigned int d;

    unsigned int ndim = iter->ndim;

    for (d=0; d<ndim-1; d++)
        if (iter->coords[d] == iter->max[d]) {
            iter->coords[d] = iter->min[d];
            iter->coords[d+1]++;
        }

    /* stop iterating when slowest axis hits max value */
    if (iter->coords[ndim-1] >= iter->max[ndim-1]) {
        free (iter->coords);
        iter->coords = NULL;
        return 0;
    }

    return 1;
}


void kimage_iterator_destroy (KImageIterator* iter)
{
    if (iter->coords)
        free (iter->coords);

    iter->coords = NULL;

}
