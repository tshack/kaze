#include "kerror.h"

char* kerror (KErrorCode err)
{
    switch (err) {
    case SUCCESS:
        return "(SUCCESS) Everything is peaches and roses";
    case FAILURE:
        return "(FAILURE) Unspecified failure";
    case MODULE_ERROR:
        return "(MODULE_ERROR) Module implementation error";
    case NO_CONTEXT:
        return "(NO_CONTEXT) Context creation failed";
    case WRONG_IO_DIRECTION:
        return "(WRONG_IO_DIRECTION) I/O directionality violated";
    case NULL_INPUT:
        return "(NULL_INPUT) Received unexpected NULL input";
    case INVALID_INPUT:
        return "(INVALID_INPUT) Received invalid input name/key";
    case INVALID_OUTPUT:
        return "(INVALID_OUTPUT) Received invalid output name/key";
    case INVALID_OPTION:
        return "(INVALID_OPTION) Received invalid option name/key";
    case BAD_OPTION_VALUE:
        return "(BAD_OPTION_VALUE) Supplied option value is invalid";
    case MEM_FAIL:
        return "(MEM_FAIL) Dynamic memory allocation failure";
    case BAD_FILE_HEADER:
        return "(BAD_FILE_HEADER) Corrupt/unknown file header encountered";
    case UNSUPPORTED_FORMAT:
        return "(UNSUPPORTED_FORMAT) Unsupported file format encountered";
    case INTERNAL_ERROR:
        return "(INTERNAL_ERROR) A 3rd party library has generated an unknown error";
    case FILE_ERROR:
        return "(FILE_ERROR) Failed to open/close/etc a file";
    case DIMENSION_MISMATCH:
        return "(DIMENSION_MISMATCH) Received data with incompatible dimensions";
    case PIXEL_TYPE_MISMATCH:
        return "(PIXEL_TYPE_MISMATCH) supplied KImage KPixelTypes don't match";
    case CHANNEL_MISMATCH:
        return "(CHANNEL_MISMATCH) Received data with incompatible # of channels";
    case UNSUPPORTED_PIXEL_TYPE:
        return "(UNSUPPORTED_PIXEL_TYPE) supplied KImage has unsupported pixel type";
    case UNSUPPORTED_DIMENSIONS:
        return "(UNSUPPORTED_DIMENSIONS) supplied KImage has unsupported dimensions";
    default:
        /* This should never happen! */
        return "(UNKNOWN) Encountered an unknown error!";
    }
}
