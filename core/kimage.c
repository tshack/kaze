#include <float.h>
#include <limits.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include "kerror.h"
#include "kimage.h"

#define CHAR_PX_SIZE (sizeof( ((KPixel)0).char_px ))
#define FLOAT_PX_SIZE (sizeof( ((KPixel)0).float_px ))
#define INT_PX_SIZE (sizeof( ((KPixel)0).int_px ))
#define UINT_PX_SIZE (sizeof( ((KPixel)0).uint_px ))


#define CLAMP(x, low, high) ({\
  __typeof__(x) __x = (x); \
  __typeof__(low) __low = (low);\
  __typeof__(high) __high = (high);\
  __x > __high ? __high : (__x < __low ? __low : __x);\
  })


#define CONVERT_PIXELS(img, from_type, to_type, min, max)                      \
do {                                                                           \
    unsigned long n;                                                           \
                                                                               \
    KImageRestricted* rest = img->restricted;                                  \
    from_type* old_data = rest->data;                                          \
    to_type* data;                                                             \
                                                                               \
    /* types are same size, simply cast in place */                            \
    if (sizeof(to_type) == sizeof(from_type)) {                                \
        data = (to_type*)old_data;                                             \
        for (n=0; n<(rest->npix)*(rest->nchan); n++)                           \
            data[n] = (to_type)CLAMP(old_data[n], min, max);                   \
    }                                                                          \
    /* destination type is smaller, cast from front to back then shrink */     \
    else if (sizeof(to_type) < sizeof(from_type)) {                            \
        data = (to_type*)old_data;                                             \
        for (n=0; n<(rest->npix)*(rest->nchan); n++)                           \
            data[n] = (to_type)CLAMP(old_data[n], min, max);                   \
        data = realloc (old_data, (rest->npix)*(rest->nchan)*sizeof(to_type)); \
    }                                                                          \
    /* destination type is larger, grow then cast from back to front */        \
    else {                                                                     \
        data = realloc (old_data, (rest->npix)*(rest->nchan)*sizeof(to_type)); \
        if (!data)                                                             \
            return MEM_FAIL;  /* from kimage_convert() !!! */                  \
                                                                               \
        old_data = (from_type*)data;                                           \
        for (n=((rest->npix)*(rest->nchan)); n-- > 0; )                        \
            data[n] = (to_type)CLAMP(old_data[n], min, max);                   \
    }                                                                          \
                                                                               \
    rest->data = data;                                                         \
} while (0)


typedef struct KImageRestricted KImageRestricted;
struct KImageRestricted {
    void* data;         /* pixel data */
    KPixelType ptype;   /* pixel type */
    unsigned long npix; /* number of pixels */
    unsigned int nchan; /* # of channels    */
    unsigned int ndim;  /* # of dimensions  */
    unsigned int* dims; /* # pixels in each dimension */
};



/* Note: Coordinates can fall outside of image boundaries,
 * (including being negative).  In such cases, we clamp to
 * image boundaries */
static inline unsigned long _get_pixel_idx (
    KImage* img,
    unsigned int channel,
    int* coord
)
{
    unsigned int c, d;
    unsigned long idx;

    unsigned int stride = 1;
    KImageRestricted* rest = img->restricted;


    if (coord[0] < 0)
        idx = 0;
    else if (coord[0] >= rest->dims[0])
        idx = rest->dims[0]-1;
    else
        idx = coord[0];

    for (d=0; d<rest->ndim-1; d++) {
        if (coord[d+1] < 0)
            c = 0;
        else if (coord[d+1] >= rest->dims[d+1])
            c = rest->dims[d+1]-1;
        else
            c = coord[d+1];

        stride *= rest->dims[d];
        idx += stride * c;
    }
    stride *= rest->dims[rest->ndim-1];
    idx += stride * channel;

    return idx;
}


KErrorCode kimage_get_neighborhood (
    KImage* img,
    KImage* buffer,
    int* center
)
{
    unsigned int c, d, ndim;
    unsigned long sidx, didx;
    int* min;
    int* max;
    int* scoords;
    int* dcoords;

    KImageRestricted* srest = img->restricted;
    KImageRestricted* drest = buffer->restricted;
    ndim = srest->ndim;

    /* buffer's KPixelType type must match image */
    if (drest->ptype != srest->ptype)
        return PIXEL_TYPE_MISMATCH;

    /* # of neighborhood buffer dimensions must match img */
    if (drest->ndim != ndim)
        return DIMENSION_MISMATCH;

    /* each neighborhood buffer dimension must be odd */
    for (d=0; d<ndim; d++)
        if (drest->dims[d] % 2 == 0)
            return DIMENSION_MISMATCH;

    /* # of neighborhood buffer channels must match img */
    if (drest->nchan != srest->nchan)
        return CHANNEL_MISMATCH;

    /* since this function will probably find its way into
     * loops quite frequently, let's optimize the malloc */
    min     = malloc (4 * ndim * sizeof(*min));
    max     = &min[ndim];
    scoords = &max[ndim];
    dcoords = &scoords[ndim];

    for (d=0; d<ndim; d++) {
       min[d] = center[d] - (drest->dims[d] / 2);
       max[d] = center[d] + (drest->dims[d] / 2) + 1;
       scoords[d] = min[d];
       dcoords[d] = 0;
    }

    while (scoords[ndim-1] < max[ndim-1]) {
        for (scoords[0]=min[0]; scoords[0]<max[0]; scoords[0]++, dcoords[0]++)
            for (c=0; c<srest->nchan; c++) {
                sidx = _get_pixel_idx (img, c, scoords);
                didx = _get_pixel_idx (buffer, c, dcoords);
                switch (srest->ptype) {
                case char_px:
                    ((unsigned char*)drest->data)[didx] = ((unsigned char*)srest->data)[sidx];
                    break;
                case float_px:
                    ((float*)drest->data)[didx] = ((float*)srest->data)[sidx];
                    break;
                case int_px:
                    ((int*)drest->data)[didx] = ((int*)srest->data)[sidx];
                    break;
                case uint_px:
                    ((unsigned int*)drest->data)[didx] = ((unsigned int*)srest->data)[sidx];
                    break;
                }
            }

        for (d=0; d<ndim-1; d++)
            if (scoords[d] == max[d]) {
                scoords[d] = min[d];
                scoords[d+1]++;

                dcoords[d] = 0;
                dcoords[d+1]++;
            }
    }

    free (min);

    return SUCCESS;
}


unsigned long kimage_npix (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return rest->npix;
}


unsigned int kimage_nchannels (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return rest->nchan;
}


unsigned int kimage_ndim (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return rest->ndim;
}


unsigned int kimage_dim (KImage* img, unsigned int dim)
{
    KImageRestricted* rest = img->restricted;

    if (dim < rest->ndim)
        return rest->dims[dim];
    else
        return 0;
}


unsigned int kimage_width (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    if (rest->ndim > 0)
        return rest->dims[0];
    else
        return 0;
}


unsigned int kimage_height (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    if (rest->ndim > 1)
        return rest->dims[1];
    else
        return 1;
}


unsigned int kimage_depth (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    if (rest->ndim > 2)
        return rest->dims[2];
    else
        return 1;
}


KPixelType kimage_type (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return rest->ptype;
}


/* Nb. Casts to a union type (i.e. KPixel) are not part of the C
 * standard -- this is facilitated by a gcc extension (and *MAY* also be
 * supported by other compilers, although not guaranteed). */
KPixel kimage_pixel (KImage* img, unsigned int channel, int* coords)
{
    unsigned long idx;

    KImageRestricted* rest = img->restricted;

    idx = _get_pixel_idx (img, channel, coords);

    switch (rest->ptype) {
    case char_px:
        return (KPixel)(((unsigned char*)rest->data)[idx]);
    case float_px:
        return (KPixel)(((float*)rest->data)[idx]);
    case int_px:
        return (KPixel)(((int*)rest->data)[idx]);
    case uint_px:
        return (KPixel)(((unsigned int*)rest->data)[idx]);
    default:
        /* this should never happen unless there is a bug in kimage.c
         * Since KImages are opaque, the user cannot manually set a
         * KImage's KPixelType to a value outside of the enum with an
         * explicit cast.  Regardless, we provide a default for good
         * practice (and to keep the compiler happy under -Wall) */
        return (KPixel)0;
    }
}


void kimage_set_pixel (
    KImage* img,
    KPixel val,
    unsigned int channel,
    int* coords
)
{
    unsigned long idx;

    KImageRestricted* rest = img->restricted;

    idx = _get_pixel_idx (img, channel, coords);

    switch (rest->ptype) {
    case char_px:
        ((unsigned char*)rest->data)[idx] = val.char_px;
        break;
    case float_px:
        ((float*)rest->data)[idx] = val.float_px;
        break;
    case int_px:
        ((int*)rest->data)[idx] = val.int_px;
        break;
    case uint_px:
        ((unsigned int*)rest->data)[idx] = val.uint_px;
        break;
    }
}


KErrorCode kimage_convert (KImage* img, KPixelType new_type)
{
    KImageRestricted* rest = img->restricted;
    KPixelType old_type = rest->ptype;

    /* char_px */
    if (old_type == char_px && new_type == char_px) {
        /* nothing to do */
    }
    else if (old_type == char_px && new_type == float_px) {
        CONVERT_PIXELS (img, unsigned char, float, 0, FLT_MAX);
        rest->ptype = float_px;
    }
    else if (old_type == char_px && new_type == int_px) {
        CONVERT_PIXELS (img, unsigned char, int, 0, INT_MAX);
        rest->ptype = int_px;
    }
    else if (old_type == char_px && new_type == uint_px) {
        CONVERT_PIXELS (img, unsigned char, unsigned int, 0, UINT_MAX);
        rest->ptype = uint_px;
    }

    /* float_px */
    else if (old_type == float_px && new_type == char_px) {
        CONVERT_PIXELS (img, float, unsigned char, 0, UCHAR_MAX);
        rest->ptype = char_px;
    }
    else if (old_type == float_px && new_type == float_px) {
        /* nothing to do */
    }
    else if (old_type == float_px && new_type == int_px) {
        CONVERT_PIXELS (img, float, int, INT_MIN, INT_MAX);
        rest->ptype = int_px;
    }
    else if (old_type == float_px && new_type == uint_px) {
        CONVERT_PIXELS (img, float, unsigned int, 0, UINT_MAX);
        rest->ptype = uint_px;
    }

    /* int_px */
    else if (old_type == int_px && new_type == char_px) {
        CONVERT_PIXELS (img, int, unsigned char, 0, UCHAR_MAX);
        rest->ptype = char_px;
    }
    else if (old_type == int_px && new_type == float_px) {
        CONVERT_PIXELS (img, int, float, -FLT_MAX, FLT_MAX);
        rest->ptype = float_px;
    }
    else if (old_type == int_px && new_type == int_px) {
        /* nothing to do */
    }
    else if (old_type == int_px && new_type == uint_px) {
        CONVERT_PIXELS (img, int, unsigned int, 0, UINT_MAX);
        rest->ptype = uint_px;
    }

    /* uint_px */
    else if (old_type == uint_px && new_type == char_px) {
        CONVERT_PIXELS (img, unsigned int, unsigned char, 0, UCHAR_MAX);
        rest->ptype = char_px;
    }
    else if (old_type == uint_px && new_type == float_px) {
        CONVERT_PIXELS (img, unsigned int, float, 0, FLT_MAX);
        rest->ptype = float_px;
    }
    else if (old_type == uint_px && new_type == int_px) {
        CONVERT_PIXELS (img, unsigned int, int, 0, INT_MAX);
        rest->ptype = int_px;
    }
    else if (old_type == uint_px && new_type == uint_px) {
        /* nothing to do */
    }

    return SUCCESS;
}


void* kimage_raw_pixels (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return rest->data;
}


KImage* kimage_empty_clone (KImage* img)
{
    KImageRestricted* rest = img->restricted;

    return kimage_new (rest->ptype, rest->nchan, rest->ndim, rest->dims);
}


KImage* kimage_clone (KImage* img)
{
    KImage* new;
    KImageRestricted* new_rest;

    KImageRestricted* rest = img->restricted;

    new = kimage_empty_clone (img);
    new_rest = new->restricted;

    switch (rest->ptype) {
    case char_px:
        memcpy(new_rest->data, rest->data, rest->nchan * rest->npix * CHAR_PX_SIZE);
        break;
    case float_px:
        memcpy(new_rest->data, rest->data, rest->nchan * rest->npix * FLOAT_PX_SIZE);
        break;
    case int_px:
        memcpy(new_rest->data, rest->data, rest->nchan * rest->npix * INT_PX_SIZE);
        break;
    case uint_px:
        memcpy(new_rest->data, rest->data, rest->nchan * rest->npix * UINT_PX_SIZE);
        break;
    }

    return new;
}


static void _kimage_destructor (KObject* kobj)
{
    KImage* img = container_of (kobj, KImage, kobj);

    if (!img)
        return;

    KImageRestricted* rest = img->restricted;

    free(rest->data);
    free(rest->dims);
    free(rest);
    free(img);
}


KImage* kimage_new (
    KPixelType ptype,
    unsigned int nchan,
    unsigned int ndim,
    unsigned int* dims
)
{
    int i;
    KImage* img;
    KImageRestricted* rest;

    img = malloc(sizeof(*img));
    if (!img)
        return NULL;

    rest = malloc(sizeof(*rest));
    if (!rest)
        goto fail_rest;

    img->restricted = rest;

    rest->ndim = ndim;
    rest->nchan = nchan;
    rest->dims = malloc(ndim * sizeof(*rest->dims));
    memcpy(rest->dims, dims, ndim * sizeof(*rest->dims));

    rest->npix = 1;
    for (i=0; i<ndim; i++)
        rest->npix *= rest->dims[i];

    switch (ptype) {
    case char_px:
        rest->data = malloc(rest->npix * nchan * CHAR_PX_SIZE);
        break;
    case float_px:
        rest->data = malloc(rest->npix * nchan * FLOAT_PX_SIZE);
        break;
    case int_px:
        rest->data = malloc(rest->npix * nchan * INT_PX_SIZE);
        break;
    case uint_px:
        rest->data = malloc(rest->npix * nchan * UINT_PX_SIZE);
        break;
    }
    rest->ptype = ptype;

    kobject_init (&img->kobj, _kimage_destructor);

    if (rest->data)
        goto success;

fail_data:
    free (rest);
fail_rest:
    free (img);
    img = NULL;
success:
    return img;
}
