#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

#include "kerror.h"
#include "kmodule.h"
#include "klist.h"

struct module_entry {
    KModule* module;
    KList link;
    void* dl_handle;
};

static KLIST_HEAD(module_bag);


void _init_kmodule (KModule* module, void* dl_handle)
{
    KErrorCode err;
    struct module_entry *me;

    err = module->load ();
    if (err) {
        if (dl_handle)
            dlclose (dl_handle);

        return;
    }

    me = malloc(sizeof(*me));
    me->module = module;
    me->dl_handle = dl_handle;
    klist_add (&me->link, &module_bag);
}


/* JAS: 2016-09-08
 * Valgrind reports a loss of 1 block (32 bytes) in dlopen() here.  It
 * seems like dlerror() facilities are neglecting to release the space
 * allocated for error codes from thread local storage. */
KErrorCode kmodule_load (const char* fn)
{
    void* handle;
    KModule* module;
    char* basename;
    char* name;
    char* symbol_name;
    char* error;

    handle = dlopen (fn, RTLD_LAZY);
    if (!handle)
        return FILE_ERROR;

    dlerror();    /* purge any existing errors */

    basename = strrchr (fn, '/');
    if (basename)
        basename++;
    else
        basename = (char*)fn;

    /* for a module named "foo.km" we want to load "module_foo" from
     * the symbol table, which is of type KModule */
    name = strndup (basename, strlen(basename) - strlen(".km"));
    symbol_name = malloc (strlen("module_") + strlen(name) + 1);
    sprintf (symbol_name, "module_%s", name);
    free (name);

    module = dlsym (handle, symbol_name);
    if ((error = dlerror()) != NULL)
        return FILE_ERROR;

    free (symbol_name);

    _init_kmodule (module, handle);

    return SUCCESS;
}


void kmodule_unload_bank (void)
{
    struct module_entry *me;
    struct module_entry *tmp;

    klist_for_each_entry_safe (me, tmp, &module_bag, link) {
        me->module->unload ();
        klist_del (&me->link);
        if (me->dl_handle)
            dlclose (me->dl_handle);

        free (me);
    }
}
