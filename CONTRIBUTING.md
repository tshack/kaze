# How to Contribute

The Kaze library is written in C and focuses on providing a simple,
stable, and minimalistic C API to its users.  The library consists of
several subsystems and datatypes with established user side APIs.
Changes to the user side API are taken seriously and are generally
discouraged without good reason.  The module system, however, provides a
means to extend the functionality of Kaze's subsystems and datatypes
without needing to modify the user API.  Consequently, most development
takes place in the module space.

Kaze uses [CMake](http://www.cmake.org) for build system generation and
[Git](http://www.git-scm.com) for version control.

This guide covers coding and style conventions adopted by the project,
the required format for commits/patches, and the workflow that should be
used contributors.


# Git Workflow and Commit Messages

Kaze uses Vincent Driessen's _gitflow_ development model, which is
described [here](http://nvie.com/posts/a-successful-git-branching-model/).
Consequently, contributors must branch off `develop` and submit
[merge requests](https://gitlab.com/tshack/kaze/merge_requests)
targeting Kaze's `develop` branch.  Do _NOT_ submit a request to merge your
branch into `master`.

If your feature branch falls too far behind `develop` or if your feature
branch needs a new feature introduced into `develop` since you
originally branched off, you can either merge `develop` into your
feature branch to pickup the new feature _or_ you can rebase onto
`develop`.  It is preferred that developers rebase since it keeps
feature branches more tidy.

Furthermore, it is preferred that developers interactively rebase their
branches (squashing, editing, and rewording as necessary) in order to
make each commit perform one logical action.  If a branch contains a
commit that introduces `foo` followed by a commit that introduces `bar`,
which is followed by a commit that applies a fix to `foo`, then the
branch should be rebased in order to squash the fix into the commit that
introduced `foo`.



## Commit Message Format

Commit messages should include a short summary (50 characters max) and
an optional detailed description, which should be hard wrapped to a text
width of 72 characters.

Short summaries must be written in the imperative (e.g. "fix bug"
**NOT** "fixes bug" or "fixed bug") and should be prefixed with the name
of the subsystem affected followed by a colon and a space.  The short
summary must be in all lower case unless mentioning a data type whose
name includes capital letters.

Below is a model commit message:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subsystem: imperative short summary (50 chars max)

A longer, more detailed description of what this commit does.  This
description should use hard line wrapping and should not exceed a line
width of 72 characters.  Note that the one line separation between the
short summary and the detailed description is used by Git to distinguish
the detailed description from the short summary and is mandatory if a
detailed description is included in the commit message.

The detailed description can consist of multiple paragraphs separated
by single blank lines.

- Bullet points can also be used

- Lists should also be limited to 72 characters wide and should use a
  hanging indent (as in this example) if spanning multiple lines

- Single dashes, double dashes, or single asterisks can be used to
  denote bullets.  Whatever the choice, it should be uniform and
  consistent within a single commit.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# Build System Basics

Kaze uses [CMake](http://www.cmake.org), which is a build system
generator.  For a given platform, CMake will generate a usable build
system.  Under Linux, for example, CMake will generate GNU Makefiles
targeting the GCC compiler.  This is done by simply changing your
working directory to where you would like to put the generate build
system and running `cmake` with the location of the source code as a
parameter.  For example, to generate a build system within a
sub-directory named `build/` under the main `kaze/` source tree:

```
$ cd kaze
$ mkdir build && cd build
$ cmake ..
```

The contents of `build/` now contains a GNU Makefile based build system.


## Building the Code

Once the build system has been generated, building all targets is as
simple as running `make` inside the `build/` folder:

```
$ cd kaze/build
$ make
```

## Running the Testing Suite

Once the code is built, you can run the testing suite to make sure
everything is operating as expected.  If you are working on a new
feature, it is recommended that you run the tests after every compile in
order to make sure you didn't accidentally break something.

```
$ cd kaze/build
$ make test
```

Branches pushed up to GitLab will automatically undergo a build test and
the testing suite will be ran.  If either fails, your branch will be
marked automatically as unmergeable.

## Running Code Coverage

Tests are great, but only if they actually test the code.  Building the
`kaze_coverage` target will not only tell you the percentage of code the
tests actually test (in terms of % lines and % functions), but an HTML
report allowing for line-by-line coverage analysis will also be
generated.  Performing coverage analysis requires special build flags,
which requires the build system to be regenerated in "Coverage Mode" and
for all the binaries to be rebuilt with coverage flags:

```
$ cd kaze/build
$ cmake -DCMAKE_BUILD_TYPE=Coverage ..
$ make
$ make kaze_coverage
$ google-chrome coverage/index.html
```

These reports are automatically generated and posted online for each
commit into `master`.  The most recent coverage analysis can be viewed
[here](http://tshack.gitlab.io/kaze/coverage).


## Generating Project Documentation

Project documentation is automatically generated using
[Doxygen](http://www.doxygen.org) and
[Graphviz](http://www.graphviz.org).  If you have both installed on your
system, then you can generate project documentation by building the
`doc` target:

```
$ cd kaze/build
$ make doc
$ google-chrome doxygen/doc/html/index.html
```

Documentation is automatically generated and posted online for each
commit into `master`.  The most recent project documentation can be
viewed [here](http://tshack.gitlab.io/kaze/).


# Coding Style

Kaze uses a style of C that focuses on readability and clear conveyance
of programmer intentions.  Kaze aims to be a sustainable piece of
software infrastructure, which makes code maintainability equal in
importance to functional capability.  In short, even if your genius
code "works," it will not be merged if it reads like an entry to the
International Obfuscated C Code Contest.


## Indentation

Kaze uses hard spaces instead of tabs.  This is your fault.  The
distinction between spaces and tabs is apparently too confusing to
warrant the use of tabs for indentation and spaces for alignment.
Consequently, tabs are forbidden and spaces shall be used for both.

Indentations are 4 space characters per level.


## Curly Braces

As in K&R style, opening braces should be placed at the end of the line
for a statement requiring them:

```C
if (x == y) {
    a = foo;
    b = bar;
    ...
}
```

The only exception is for function definitions, which must place the
opening brace alone on the following line:

```C
int main (int argc, char** argv)
{
    /* function body */
}
```

The closing brace must always be alone on its own line except for cases
where it is a continuation of the statement.  For example, an `if-else`
statement:

```C
if (x == y) {
    ...
} else if (x > y) {
    ...
} else {
    ...
}
```

or a `do-while` statement:

```C
do {
    /* loop body */
} while (condition);
```

Optional braces may be omitted if the intention of the code is clearly
maintained.  This keeps the code compact and clean of closing brace
barrages.  For example:

```C
for (k=0; k<depth; k++)
    for (j=0; j<height; j++)
        for (i=0; i<width; i++) {
            /* manipulate some volume data */
        }
```

## Functions

Functions should perform single, logical operations, and (in general)
should fit within one to two screens of text (i.e. ~50 lines).  If you
write a function that is a really long monster spanning hundreds of
lines, chances are that your function is performing multiple logical
operations and should be refactored into multiple smaller functions.
The exception, of course, are functions that consist predominately of a
single `switch` or `if-else` block that call a different function for
each case.

Local variables should be declared as close to the top of the function
as possible.  In general, function organization in Kaze look like this:

```C
int my_function (char* foo, int bar)
{
    /* uninitialized local variables */

    /* initialized local variables */

    /* some brief setup logic */

    /* variable length arrays */

    /* main function logic */
}
```

Naturally, not all elements are mandatory for every function.  Also,
dynamic stack allocations (such as variable length arrays) should be
used sparingly and only for very small arrays.  In such situations,
their use is encouraged over heap allocation due to their automatic
memory management.

Like functions spanning more than ~50 lines, functions with more than 10
local variables most probably need to be refactored into multiple
smaller functions.


## Naming

Kaze variables should be in all lower case.  Multi-word variables should
use underscores (i.e. `image_list`).  Camel case is reserved for custom
types (i.e. `KImageIterator`).

Variables names should be short yet descriptive.  Integer loop counters
should be called `i`, `j`, `k`, etc. __not__ `loop_counter`.  Floating-point
spatial coordinate variables should be called `x`, `y`, `z`, etc.
Temporary variables can be named `tmp`.  However, if you find yourself
with `tmp`, `tmp2`, `tmp3`, and `tmp4`; chances are your code is either
garbage **or** most of your variables aren't _really_ temporary at all
and need to be given real names.

Good names are descriptive and generally consist of full words.  For
example, an array of image coordinates could be named `image_coords`
__not__ `imgcrds`.  This lends to self documenting code.  Leaving out
random vowels and letters according to a random convention that you
change frequently and without regard does not make your code "leet",
"gosu", or "advanced".  It just confuses "future you" and the poor
person that has to maintain your code (i.e. "me").

Functions that are exposed through the official user API must be
prefixed by the subsystem or datatype they pertain to, followed by an
underscore character.  For example, all user API functions pertaining to
a `KImage` are prefixed with `kimage_`.


## Exporting Symbols

Kaze is a C library.  Since custom namespaces are not a thing, it is
exceedingly important to not pollute the common namespace.  Everything
you declare in the global scope should be `static` unless you are
extending the user API.  Modules automatically export a single symbol
used for hooking into Kaze when loading.  Module authors should _never_
need to declared non-`static` functions or variables in a module's
global scope.


## Comments

Properly written C employing good naming with short and deliberate
functions should be more expressive in terms of intended function than a
giant comment block.  In other words, good code is self-evident.
Comments should almost never find their way into the body of a function.

Occasionally, it may be useful to include a comment just above a
function definition to note some unexpected behavior or a clever
optimization.  Such comments should never need to explicitly state the
purpose of the function.  If the function and its parameters are well
named, then it should be obvious what the function does.  If it isn't,
then your time is better spent rewriting your function than it is
writing kludgey comments.

Declarations for user API functions are the exception. Exported user API
functions are declared in header files so they can be used in
applications linking against the Kaze library.  Consequently, these
headers should contain all the information necessary to use the Kaze API
-- this includes function declarations for the compiler and
documentation on the operation of these functions for the programmer
attempting to use the API.  Kaze uses [Doxygen](http://www.doxygen.org)
style comment blocks for this purpose, which provide standardized
markup for specifying parameters, return values, descriptions, and
example usage.  The Kaze Zenbook's HTML API reference is automatically
generated by parsing these Doxygen comment blocks from the API headers,
which contain the amount of detail normally found in a man page.
