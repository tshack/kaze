#ifndef _kpixel_h_
#define _kpixel_h_

typedef enum {
    char_px,
    float_px,
    int_px,
    uint_px
} KPixelType;

typedef union pixel {
    unsigned char  char_px;
    float float_px;
    int   int_px;
    unsigned int uint_px;
} KPixel;

#endif /* _kpixel_h_ */
