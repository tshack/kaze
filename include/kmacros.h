/**
 * @file   kmacros.h
 * @brief  Provides a few useful macros
 * @author James A. Shackleford
 * @date   August 23rd, 2016
 */

#ifndef _kmacros_h_
#define _kmacros_h_

#include <stddef.h>

/** @cond INTERNAL */
#define __stringify_1(s)  #s
#define __concat_1(a, b)   a##b
/** @endcond */


/**
 * @brief  Creates a string literal containing the the result of a
 *         macro expansion
 *
 * @param  s  Name of macro whose expansion will be stringified
 *
 * @return  A string literal containing the expansion of the
 *          supplied macro
 *
 * __Note:__ This differs from the C pre-processor's `#` operator in
 * that it stringifies the expansion of the supplied macro vs the macro
 * itself.  For example:
 *
 * @code
 * #define FOO 4
 * #define to_str(x) #x
 *
 * printf ("%s", to_str(FOO)); // output: FOO
 * printf ("%s", __stringify(FOO));  // output: 4
 * @endcode
 */
#define __stringify(s)     __stringify_1(s)

/**
 * @brief  Concatenates the supplied parameters.  If either parameter is
 *         a macro, it is expanded prior to concatenation.
 *
 * @param  a  Forms the left hand side of the concatenation result
 *
 * @param  b  Forms the right hand side of the concatenation result
 *
 * @return  The concatenation of `a` and `b`.  If `a` or `b` was a
 *          macro, then it was expanded prior to concatenation.
 *
 * For example:
 *
 * @code
 * #define WORD variable
 *
 * int my_variable = 12;
 *
 * printf ("%i", __concat(my_, WORD)); // output: 12
 * @endcode
 *
 */
#define __concat(a, b)     __concat_1(a, b)

/**
 * @brief  Given the address of a member, returns the address of the
 *         containing struct
 *
 * @param  ptr  Address of the member for which to get the containing
 *              struct
 *
 * @param  type  The type of the containing struct
 *
 * @param  member  The name of the member within the containing struct
 *
 * For example:
 *
 * @code
 * #include <stdio.h>
 * #include "kmacros.h"
 *
 * struct Employee {
 *     int id_number;
 *     char* name;
 * };
 *
 * void print_name_from_id (int* id)
 * {
 *     struct Employee *tmp = container_of (id, struct Employee, id_number);
 *
 *     printf ("Name: %s\n", tmp->name);
 * }
 *
 * int main (int argc, char** argv)
 * {
 *     struct Employee bob;
 *
 *     bob.id_number = 1234;
 *     bob.name = "Robert";
 *
 *     print_name_from_id (&bob.id_number);
 * }
 * @endcode
 */
#define container_of(ptr, type, member) ({ \
    const typeof( ((type *)0)->member ) *__mptr = (ptr); \
    (type *)( (char *)__mptr - offsetof(type,member) );})
/** @endcond */



#endif  /* _kmacros_h_ */
