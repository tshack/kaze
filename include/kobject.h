/**
 * @file   kobject.h
 * @brief  Provides KObject
 * @author James A. Shackleford
 * @date   Sept 4, 2016
 *
 * KObject provides the functionaliy of a "base class" for data
 * structures requiring thread-safe reference counting and automatic
 * destruction/recycling.
 *
 * Usage Example:
 *
 * @code
 * struct MyObject {
 *     int a;
 *     float* b;
 *     KObject kobj;
 * }
 *
 * static void myobject_destructor (KObject* kobject)
 * {
 *     MyObject* foo = container_of (kobject, KObject, kobj);
 *
 *     free (foo->b);
 *     free (foo);
 * }
 *
 * struct MyObject* myobject_new (int a)
 * {
 *     struct MyObject* foo = malloc (sizeof(*foo));
 *     foo->b = malloc (a*sizeof(*foo->b));
 *     foo->a = a;
 *
 *     kobject_init (&foo->kobj, myobject_destructor);
 *
 *     return foo;
 * }
 *
 * int main (int argc, char** argv)
 * {
 *     struct MyObject* foo = myobject_new (10);
 *
 *     ...
 *
 *     kobject_release (&foo->kobj);
 *
 *     ...
 * }
 * @endcode
 *
 * Note that it is probably desirable for your object's API to wrap
 * `kobject_release()` so that users are insulated from interfacing with
 * KObject directly.
 */
#ifndef _kobject_h_
#define _kobject_h_

#include "kmacros.h"

/**
 * @brief The base Kaze Object
 *
 * A KObject provides basic thread-safe reference counting and automatic
 * object destruction as prescribed by a user provided destructor
 * method.  A KObject (the whole object, not a pointer) should be
 * embedded in the struct inheriting these properties.  For example:
 *
 * @code
 * struct MyObject {
 *     int a;
 *     float* b;
 *     KObject kobj;
 * }
 * @endcode
 *
 * KObject initialization and reference count modification must be
 * performed using the API provided by kobject.h
 */
typedef struct KObject KObject;
struct KObject {
    unsigned int count;
    void (*destructor)(KObject* kobj);
};


/**
 * @brief  Increments a KObject's reference count by one
 *
 * @param  kobj  Pointer to the KObject
 *
 * **Note:**
 * It is extremely important that only code holding a reference be
 * allowed to increase the reference count on an object via
 * `kobject_obtain()`.  This should only happen just before the code
 * holding the reference gives another portion of code ownership over a
 * copy of the pointer to the object.  For example, the following would
 * be __incorrect__:
 *
 * @code
 * ...
 * some_object = get_object_reference ();
 * kobject_obtain (&some_object->kobj);
 * ...
 * kobject_release (&some_object->kobj);
 * @endcode
 *
 * In a multi-threaded situation, it is possible for a context switch
 * to occur between getting a copy of the pointer from
 * `get_object_reference()` and increasing its reference count via
 * `kobject_obtain()`.  If the other thread releases the last reference
 * to the object before control is returned to this thread, then the
 * object will have already been destroyed and the attempt to increment
 * the reference count will fail with an access to invalid memory.  The
 * correct behavior would have been for `get_object_reference()` to
 * increment the reference count internally via `kobject_obtain()` prior
 * to returning a copy of the pointer.
 */
static inline void kobject_obtain (KObject* kobj)
{
    __sync_add_and_fetch (&kobj->count, 1);
}

/**
 * @brief  Decrements a KObject's reference count by one
 *
 * @param  kobj  Pointer to the KObject
 *
 * This function must be called when a section of code holding a
 * reference to an object based on `KObject` is finished with the
 * reference.  This will decrement the reference count by 1.  If the
 * reference count of the object reaches 0, then the object's destructor
 * will be automatically invoked.
 */
static inline void kobject_release (KObject* kobj)
{
    if (__sync_sub_and_fetch (&kobj->count, 1) == 0)
        kobj->destructor (kobj);
}

/**
 * @brief  Initializes a KObject
 *
 * @param  kobj  Pointer to the KObject
 *
 * @param  destructor  Function pointer to object destructor
 *
 * This function must be called when an object based on `KObject` is
 * instantiated in order to set the object's destructor and initialize
 * its reference count to 1.
 */
static inline void kobject_init (KObject* kobj, void (*destructor)(KObject* kobj))
{
    kobj->count = 1;
    kobj->destructor = destructor;
}

#endif  /* _kobject_h_ */
