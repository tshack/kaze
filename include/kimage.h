/**
 * @file   kimage.h
 * @brief  Provides the KImage API
 * @author James A. Shackleford
 * @date   March 20, 2016
 *
 * KImages are mutable pixel buffer containers capable of handling an
 * arbitrary number of dimensions and channels.  Multiple pixel types
 * are supported, and KImages can convert between pixel types in-place
 * efficiently at runtime.
 */
#ifndef _kimage_h_
#define _kimage_h_

#include "kerror.h"
#include "kobject.h"
#include "kpixel.h"


/**
 * @brief The Kaze Image Object
 *
 * KImages are mutable pixel buffer containers capable of handling an
 * arbitrary number of dimensions and channels. Multiple pixel types are
 * supported, and KImages can convert between pixel types in-place
 * efficiently at runtime.
 *
 */
typedef struct KImage KImage;
struct KImage {
    /**
     * A KImage is a KObject
     */
    KObject kobj;

    /**
     * Opaque pointer to image data. A KImage can only be modified and
     * accessed through the KImage API provided by kimage.h */
    void* restricted;
};


/**
 * @brief  The Kaze Image Iterator
 *
 * A `KImageIterator` can be used with a single for-loop construct to
 * iterate through any given `KImage` with N-dimensions and M-channels.
 *
 * The members `min` and `max` point to arrays eaching containing
 * N-elements, which define the region of interest (ROI) through which
 * to iterate.  When initialized via `kimage_iterator_init()`, this ROI
 * is set to match the spatial extents of the `KImage`.
 */
typedef struct KImageIterator KImageIterator;
struct KImageIterator {
    /**
     * Set to the # of dimensions of the associated KImage undergoing
     * iteration */
    unsigned int ndim;

    /**
     * Pointer to an array defining the lower bound of iteration in each
     * spatial dimension */
    int* min;

    /**
     * Pointer to an array defining the upper bound of iteration in each
     * spatial dimension */
    int* max;

    /**
     * Pointer to an array containing the current pixel coordinate. For
     * use in the body of a for-loop construct.  Updated by
     * `kimage_iterator_incr()` and `kimage_iterator_check()` */
    int* coords;
};


/* CONSTRUCTORS & DESTRUCTORS *****************************************/
/**
 * @brief  Creates a new KImage
 *
 * @param  ptype  Datatype of pixels
 *
 * @param  nchan  Number of data/color channels
 *
 * @param  ndim  Number of spatial dimensions
 *
 * @param  dims  An ndim array of unsigned ints, where each element
 *               specifies the number of pixels in an image dimension
 *
 * @return  On success, returns a pointer to a new KImage. It is the
 *          responsibility of the caller to release the KImage using
 *          kimage_release().  Upon failure, returns NULL.
 *
 * Creates a new KImage with the specified pixel type, number of
 * channels, and dimensionality.
 *
 * For example, to create a 1024x768 2D image with 3 channels and 8 bits
 * per pixel (i.e. unsigned char pixels):
 * @code
 *   KImage* img;
 *   unsigned int dims[] = {1024, 768};
 *
 *   img = kimage_new(char_px, 3, 2, dims);
 * @endcode
 *
 * To createa a 512x512x128 3D volume with 1 channel and float pixels:
 * @code
 *   KImage* img;
 *   unsigned int dims[] = {512, 512, 128};
 *
 *   img = kimage_new(float_px, 1, 3, dims);
 * @endcode
 *
 * **Note:** `kimage_new()` usage is greatly simplified by using `kzip()`
 */
KImage* kimage_new (KPixelType ptype, unsigned int nchan, unsigned int ndim, unsigned int* dims);

/**
 * @brief  Creates a new KImage that is identical to the
 *         supplied KImage.
 *
 * @param  img  KImage to clone
 *
 * @return  On success, returns a pointer to a new KImage. It is the
 *          responsibility of the caller to release the KImage using
 *          kimage_release()
 */
KImage* kimage_clone (KImage* img);

/**
 * @brief  Creates a new KImage that is identical to the supplied KImage
 *         but with an uninitialized pixel buffer.
 *
 * @param  img  KImage to clone
 *
 * @return  On success, returns a pointer to a new KImage. It is the
 *          responsibility of the caller to release the KImage using
 *          kimage_release()
 *
 * Creates a duplicate of the supplied KImage, but does not copy over
 * pixel buffer values. Although the pixel buffer of the new KImage is
 * uninitialized, it is otherwise identical (pixel type, dimensionality,
 * etc) to the source KImage.  It is probably best to use
 * kimage_empty_clone() insead of kimage_clone() when a destination
 * buffer identical to another KImage is required.
 */
KImage* kimage_empty_clone (KImage* img);

/**
 * @brief  Releases a KImage reference
 *
 * @param  img  Pointer to KImage to release
 *
 * This function releases a reference to the KImage pointer `img`.  When
 * all references have been released, the dynamic resources allocated to
 * the KImage will be freed.
 *
 * **Note:** This is a convenience function.  A reference to a KImage
 * can always be released by operating directly on its KObject:
 *
 * @code
 * int b[2];
 *
 * KImage* img = kimage_new(char_px, 1, 2, kzip(b, 720, 480);
 * ...
 *
 * kobject_release(&img->kobj);
 * @endcode
 */
static inline void kimage_release (KImage* img)
{
    kobject_release (&img->kobj);
}
/**********************************************************************/

/* MUTATORS ***********************************************************/
/**
 * @brief  Converts a KImage's KPixelType from one type to another
 *
 * @param  img  Pointer to KImage
 *
 * @param  new_type  Convert the KImage's pixels to this KPixelType
 *
 * @return  If the new pixel type is larger than the current pixel type
 *          and there is insufficent memory to perform the conversion,
 *          the KImage will remain unchanged and kimage_convert() will
 *          return MEM_FAIL.  Otherwise, kimage_convert() returns
 *          SUCCESS.
 *
 * This function converts the pixel buffer of the specified KImage to
 * a new KPixelType. Conversion occurs in-place with the memory
 * allocated to the pixel buffer shrinking or growing as necessary.  If
 * the supplied destination KPixelType is the same as the KImage's
 * current KPixelType, no action is performed.
 *
 * Note that since pixel buffers are contiguous, the resizing of a pixel
 * buffer may require changing its location in memory. Consequently,
 * converting a KImage to a different KPixelType may invalidate a
 * previously obtained pixel buffer pointer retrieved via
 * kimage_raw_pixels().
 *
 */
KErrorCode kimage_convert (KImage* img, KPixelType new_type);

/**
 * @brief  Sets the value of a KImage pixel
 *
 * @param  img  Pointer to KImage
 *
 * @param  val  Set specified pixel to this value
 *
 * @param  channel  Channel of pixel to set.
 *                  (Note: channels are zero indexed)
 *
 * @param  coords  An ndim array of unsigned ints, where each element
 *                 specifies the destination coordinate in each image
 *                 dimension
 *
 * This function sets the value of a pixel within the specified channel
 * at the specified coordinate of a KImage.
 *
 * For example, to set pixel (15,78) in the 3rd channel of a 2D KImage
 * to the value 128:
 *
 * @code
 *   KImage* img;
 *   KPixel pixel;
 *   unsigned int dims[] = {800, 600};
 *   unsigned int coords[] = {15, 78};
 *
 *   img = kimage_new(char_px, 3, 2, dims);
 *
 *   pixel.char_px = 128;
 *   kimage_set_pixel(img, pixel, 2, coords);
 * @endcode
 *
 * Alternatively, if you are using a C compiler that supports casting to
 * Union types, such as GCC:
 * @code
 *   KImage* img;
 *   unsigned int dims[] = {800, 600};
 *   unsigned int coords[] = {15, 78};
 *
 *   img = kimage_new(char_px, 3, 2, dims);
 *
 *   kimage_set_pixel(img, (KPixel)128, 2, coords);
 * @endcode
 */
void kimage_set_pixel (KImage* img, KPixel val, unsigned int channel, int* coords);
/**********************************************************************/

/* ACCESSORS **********************************************************/
/**
 * @brief  Retrieves the number of pixels in a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of pixels within a single channel of the
 *          specified KImage.  (Note, all channels contain the
 *          same number of pixels)
 */
unsigned long kimage_npix (KImage* img);

/**
 * @brief  Retrieves the number of channels in a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of channels in the KImage
 */
unsigned int kimage_nchannels (KImage* img);

/**
 * @brief  Retrieves the number of dimensions in a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of spatial dimensions in the KImage
 */
unsigned int kimage_ndim (KImage* img);

/**
 * @brief  Retrieves the number of pixels in the specified spatial
 *         dimension of a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @param  dim  Spatial dimension for which to get the number of pixels
 *
 * @return  Number of pixels in the specified spatial dimension
 */
unsigned int kimage_dim (KImage* img, unsigned int dim);

/**
 * @brief  Retrieves the number of pixels in the first spatial
 *         dimension of a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of pixels in the first spatial dimension
 */
unsigned int kimage_width (KImage* img);

/**
 * @brief  Retrieves the number of pixels in the second spatial
 *         dimension of a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of pixels in the second spatial dimension
 */
unsigned int kimage_height (KImage* img);

/**
 * @brief  Retrieves the number of pixels in the third spatial
 *         dimension of a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  Number of pixels in the third spatial dimension
 */
unsigned int kimage_depth (KImage* img);

/**
 * @brief  Retrieves the KPixelType of a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  KPixelType of the specified KImage
 */
KPixelType kimage_type (KImage* img);

/**
 * @brief  Retrieves the value of a KImage pixel
 *
 * @param  img  Pointer to KImage
 *
 * @param  channel  Channel from which to retrieve pixel
 *                  (Note: channels are zero indexed)
 *
 * @param  coords  An ndim array of unsigned ints, which
 *                 specifies the desired pixel coordinate
 *
 * @return  A KPixel containing the value of the pixel within the
 *          specified channel at the specified KImage coordinate.
 *
 * For example, to get the value of pixel (15,78) in the 3rd channel
 * of a 2D KImage:
 *
 * @code
 *   KImage* img;
 *   KPixel pixel;
 *   unsigned int coords[] = {15, 78};
 *   ...
 *
 *   pixel = kimage_pixel(img, 2, coords);
 * @endcode
 */
KPixel kimage_pixel (KImage* img, unsigned int channel, int* coords);

/**
 * @brief  Retrieves the pointer to the pixel buffer encapsulated by
 *         a KImage
 *
 * @param  img  Pointer to KImage
 *
 * @return  A void* pointer to the specified KImage's pixel buffer.  It
 *          is the responsibility of the caller to either implicitly
 *          convert or cast to the appropriate type before dereferencing
 *          the returned pointer.
 *
 * This function exposes the raw pixel buffer encapsulated by a KImage,
 * which allows for direct pixel modification.  Although direct pixel
 * manipulation may be faster in some circumstances, extreme care should
 * be taken to not corrupt the pixel buffer data.
 *
 * Note that converting a KImage's pixel buffer to a different
 * KPixelType via kimage_convert() may invalidate previously obtained
 * pixel buffer pointers retrieved via kimage_raw_pixels().
 */
void* kimage_raw_pixels (KImage* img);

/**
 * @brief  Retrieves a neighborhood of pixel values from a KImage
 *
 * @param  img  KImage from which to retrieve the neighborhood
 *
 * @param  buffer  KImage which both stores the retrieved pixel
 *                 neighborhood and determines the extent of the
 *                 neighborhood to retrieve
 *
 * @param  center  An ndim array of ints, which specifies the center of
 *                 the neighborhood in the source KImage
 *
 * @return  If the number of dimensions possessed by the buffer do not
 *          match that of the KImage from which the neighborhood is to
 *          be retrieved, DIMENSION_MISMATCH is returned.  Likewise, if
 *          the KPixelTypes don't match, PIXEL_TYPE_MISMATCH; and if the
 *          number of channels don't match, DIMENSION_MISMATCH.  If the
 *          number of pixels in each spatial dimension of the buffer are
 *          not odd, DIMENSION_MISMATCH is returned. Otherwise, SUCCESS
 *          is returned.
 *
 * This function populates the supplied KImage buffer with a local
 * neighborhood of pixels copied from a KImage.  The dimensions of the
 * supplied KImage buffer determine the size of the neighborhood that
 * will be retrieved, with the retrieved region being centered at the
 * specified coordinate.  Consequently, each spatial dimension of the
 * buffer must be odd.
 *
 * Note that the type, number of spatial dimensions, and channels
 * possessed by the buffer must match that of the KImage from which the
 * neighborhood is to be retrieved.
 *
 * Additionally, note that if part of the neighborhood to be retrieved
 * falls outside of the KImage (i.e. overlaps an edge or corner) the
 * edge pixels will be "smeared" back to populate the regions of the
 * neighborhood falling outside.
 */
KErrorCode kimage_get_neighborhood (KImage* img, KImage* buffer, int* center);
/**********************************************************************/


/* kimage_iter.c */
/**
 * @brief  Initializes a `KImageIterator` for a given `KImage`
 *
 * @param  iter  Pointer to the `KImageIterator` to initialize
 *
 * @param  img  Pointer to the `KImage` to iterate over
 *
 * @return  If either the supplied `KImageIterator` or `KImage` pointers
 *          is `NULL`, the `KErrorCode` `NULL_INPUT` is returned.  If
 *          there is insufficient memory to allocate the iterator,
 *          `MEM_FAIL` is returned.  Otherwise, `SUCCESS` is returned.
 *
 */
KErrorCode kimage_iterator_init (KImageIterator* iter, KImage* img);

/**
 * @brief  Frees dynamic resources held by a `KImageIterator`
 *
 * @param  iter  Pointer to the `KImageIterator` to destroy
 */
void kimage_iterator_destroy (KImageIterator* iter);

/**
 * @brief  Increments the `KImageIterator` to the next sequential pixel
 *         coordinate.
 *
 * @param  iter  Pointer to the `KImageIterator` to increment
 *
 * `kimage_iterator_incr()` is intended to be used as the "afterthought"
 * element in a for-loop. */
void kimage_iterator_incr (KImageIterator* iter);

/**
 * @brief  Checks to see if the iterator has been exhausted
 *
 * @param  iter  Pointer to the `KImageIterator` to check
 *
 * @return  If the iterator has been exhausted (i.e. has iterated
 *          through all pixel coordinates) 0 is return; otherwise,
 *          non-zero is returned.
 *
 * `kimage_iterator_check()` is intended to be used as the "condition"
 * element in a for-loop.  Note that `kimage_iterator_check()` also
 * performs ROI boundry wrapping on `KImageIterator::coords` behind the
 * scenes. */
int kimage_iterator_check (KImageIterator* iter);

/**
 * @brief  Populates a KImage with a file loaded from disk
 *
 * @param  filename  String containing path to file to load
 *
 * @return  Upon success a pointer to a new `KImage` containing the
 *          loaded image is returned.  It is the responsibility of the
 *          caller to release the KImage via `kimage_release()`.  If the
 *          specified file cannot be loaded, `NULL` is returned.
 *
 * The image file formats that can be loaded by `kimage_read()` are
 * determined by the currently loaded Kaze modules.  If a module has
 * been loaded that provides support for the specified file extension,
 * then `kimage_read()` will be able to read the corresponding format.
 */
KImage* kimage_read (const char* filename);

/**
 * @brief  Writes a KImage to disk in a specified file format
 *
 * @param  filename  String containing the path to the file to write.
 *                   The provided file extension determines the output
 *                   image format.
 *
 * @param  img  Pointer to the KImage to write to disk
 *
 * @return  If the format specified by the given file extension is not
 *          supported, then `UNSUPPORTED_FORMAT` is returned.  If the
 *          Kaze module handling the requested format encounters an
 *          error while attempting to write to disk, the code for the
 *          encountered error will be returned.  Otherwise, `SUCCESS`
 *          is returned.
 *
 * The image file formats that can be written by `kimage_write()` are
 * determined by the currently loaded Kaze modules.  If a module has
 * been loaded that provides support for the specified file extension,
 * then `kimage_write()` will be able to write the corresponding format.
 */
KErrorCode kimage_write (const char* filename, KImage* img);


#endif /* _kimage_h_ */
