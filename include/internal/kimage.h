#ifndef _kimage_internal_h_
#define _kimage_internal_h_

#include "kerror.h"

typedef struct KFileOps KFileOps;

KErrorCode kimage_register_fops (KFileOps* fops);
KErrorCode kimage_unregister_fops (KFileOps* fops);

#endif  /* _kimage_internal_h_ */
