#ifndef _kfileops_internal_h_
#define _kfileops_internal_h_

#include "kerror.h"
#include "kimage.h"

typedef struct KFileOps KFileOps;
struct KFileOps {
    const char* extension;
    KImage* (*reader)(const char* fn);
    KErrorCode (*writer)(const char* fn, KImage* img);
};

#endif /* _kfileops_internal_h_ */
