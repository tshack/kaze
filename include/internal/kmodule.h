#ifndef _kmodule_internal_h_
#define _kmodule_internal_h_

#include "kmacros.h"
#include "kmodule.h"

#define THIS_MODULE   __concat(module_, __MODULE__)

#define MODULE_NAME   __stringify(__MODULE__)

static KErrorCode load ();
static KErrorCode unload ();

struct KModule THIS_MODULE = {
    .description = "",
    .load = load,
    .unload = unload
};

#define kmodule_description(str) \
    THIS_MODULE.description = str

#endif /* _kmodule_internal_h_ */
