#ifndef _kblock_internal_h_
#define _kblock_internal_h_

#include "kerror.h"

typedef struct KBlock KBlock;

KErrorCode kblock_register (KBlock* block);
KErrorCode kblock_unregister (KBlock* block);

#endif  /* _kblock_internal_h_ */
