/**
 * @file   kblock.h
 * @brief  Provides the KBlock API
 * @author James A. Shackleford
 * @date   March 26, 2016
 *
 * KBlocks are algorithm containers that can be easily strung together
 * to form more complex algorithmic pipelines.  KBlocks communicate
 * with the outside world through their KIOTerminal and KOption
 * interfaces.  The KIOTerminal can support an arbitrary number of named
 * input and output channels, which are used to pass the memory
 * addresses of input and output data.  The KOption interface can
 * support an arbitrary number of named input options and provides type
 * checking, default values, user defined valid value ranges, and
 * automatic input validation.
 *
 * A KBlock is used by creating a stateful context called a
 * KBlockContext at runtime.
 *
 * @code
 *
 *                            KOptions
 *                         |   |   |   |
 *                       __V___V___V___V__
 *  KIOTerminal  ------>|                 |------>  KIOTerminal
 *    Inputs     ------>|     KBlock      |------>    Outputs
 *                      |     Context     |------>
 *                       -----------------
 * @endcode
 */

#ifndef _kblock_h_
#define _kblock_h_

#include <stdint.h>
#include "kerror.h"

typedef struct KBlock KBlock;
typedef struct KBlockContext KBlockContext;
typedef struct KIOTerminal KIOTerminal;
typedef struct KOption KOption;

/**
 * @brief Flexible, pipelineable algorithm building block
 */
struct KBlock {
    /**
     * String used to identify the KBlock.  This is used when getting a
     * KBlockContext of a KBlock via kblock_new_context() */
    const char* name;

    /**
     * Used during context creation to allocate restricted storage
     * for a context's state, parameters, etc.  This allocated
     * storage is opaque to the context and is only readable by
     * the block itself.  */
    size_t data_size;

    /**
     * algorithm() is always invoked via a call to the user API
     * function kblock_execute().  algorithm() implements the image
     * processing operation of the block.  */
    KErrorCode (*algorithm)(KBlockContext* ctx);

    /**
     * Interface for passing pointers into and out of the block */
    const KIOTerminal* io;

    /**
     * Optionally points to an array of KOptions specifying the tunable
     * parameters of the block.  Each option has a name, type, default
     * value, and valid range among other attributes.  This is effectively
     * the only way to modify a context's restricted storage outside of the
     * block.  The user API function kblock_set_option() provides a method
     * of setting options (and therefore certain items in a context's
     * restricted storage) */
    const KOption* options;

    /**
     * The setup() hook function is called only once during context
     * creation, which is always invoked via a call to the user API
     * function kblock_new_context().  setup() is called immediately
     * after block options have been set to their default values for
     * the context and is the last action taken in the context
     * creation process.  This is a good opportunity for the block
     * context to allocate any dynamic memory it may need during future
     * invocations.
     *
     * N.b. setup() is called on the context only if the block has
     * registered local context data (i.e. restricted_size != NULL).  */
    KErrorCode (*setup)(KBlockContext* ctx);

    /**
     * teardown() is called only once during context destruction,
     * which is always invoked via a call to the user API function
     * kblock_free_context().  teardown() is the first action taken place
     * in the block context destruction process.  Any dynamic memory
     * allocated within the setup() hook should be freed inside of
     * teardown().  */
    KErrorCode (*teardown)(KBlockContext* ctx);
};


/**
 * @brief Stateful context of a KBlock
 */
struct KBlockContext {
    /* Pointer to KBlock.  Private to the block system. */
    KBlock* block;

    /* error handler callback stuff */
    void* error_handler;

    /* Context state. Only dereferencable inside the owning KBlock */
    void* data;
};


/**
 * @brief The KBlock I/O interface
 *
 * A KIOTerminal defines an individual KBlock input (or output).
 */
struct KIOTerminal {

    /**
     * String used to identify the KIOTerminal.  This is used when
     * setting a KBlockContext input using kblock_set_input() and when
     * getting a KBlockContext output using kblock_get_output() */
    const char* name;

    /**
     * A short description of the KIOTerminal */
    const char* description;

    /**
     * Offset into a KBlockContext's restricted state data, prescribing
     * where the KIOTerminal's state will be stored. */
    size_t offset;

    /**
     * Defines the type of data passed by the KIOTerminal */
    enum {
        KIMAGE_PTR,
        INT_PTR,
        FLOAT_PTR,
        DOUBLE_PTR
    } type;

    /**
     * Defines if the KIOTerminal is an input or an output */
    enum {
        INPUT,
        OUTPUT
    } direction;
};


/**
 * @brief The KBlock option interface
 *
 * A KOption defines a KBlock option that can be set. KOptions are used
 * to affect the execution behavior of a KBlockContext. */
struct KOption {

    /**
     * String used to identify the KOption.  This is used when setting
     * a KBlockContext's option using kblock_set_option() */
    const char* name;

    /**
     * A short description of the KOption */
    const char* description;

    /**
     * Offset into a KBlockContext's restricted state data, prescribing
     * where the KOption's state will be stored. */
    size_t offset;

    /**
     * Defines the type of the KOption */
    enum {
        OPT_INT,
        OPT_FLOAT,
        OPT_STRING
    } type;

    /**
     * Used to set the default value of a KOption */
    union {
        int64_t i64;
        float flt;
        const char* str;
    } default_value;

    /**
     * Defines the minimum value that can be assigned to a KOption
     * using kblock_set_option(). */
    double min;

    /**
     * Defines the maximum value that can be assigned to a KOption
     * using kblock_set_option(). */
    double max;
};


/**
 * @brief  Creates a new KBlockContext
 *
 * @param  block_name  String containing the name of the KBlock for
 *                      which to create a context. (Note: a KBlock's
 *                      name is defined by its .name member).
 *
 * @return  Upon success kblock_new_context() returns a pointer to a
 *          new KBlockContext.  It is the responsibility of the caller
 *          to free the returned context using kblock_free_context().
 *          A NULL pointer is returned on error.
 *
 * KBlocks are themselves not directly usable since they are unable to
 * store state information.  KBlocks are used by Kaze as a sort of
 * "blueprint" to construct KBlockContexts, which do have the ability
 * to store persistent state data.  Conceptually, the relationship
 * between KBlocks and KBlockContexts can be viewed as follows:
 *
 * @code
 *                            +-------------------+
 *                            |   KBlockContext   |
 *                            |                   |
 *    +-----------+           | +---------------+ |
 *    |  KBlock   |  =======> | |    KBlock     | |
 *    +-----------+           | +---------------+ |
 *                            | +---------------+ |
 *                            | | KBlock State  | |
 *                            | |     Data      | |
 *                            | +---------------+ |
 *                            +-------------------+
 * @endcode
 *
 * A KBlockContext behaves like a stateful algorithmic container where
 * the algorithm is provided by KBlock and the options, parameters,
 * intermediate results, etc are stored in the KBlockContext's data
 * storage area.  Unlike traditional C functions, KBlockContexts can
 * have multiple outputs, which are retrieved via kblock_get_output().
 * Additionally, multiple pointers to input data can be set via
 * kblock_set_input() and options affecting algorithmic behavior can be
 * set via kblock_set_option().  Similar to local static function
 * variables in C, KBlockContexts can maintain persistent state across
 * multiple invocations.
 */
KBlockContext* kblock_new_context (const char* block_name);


/**
 * @brief  Assigns an error handler callback to a KBlockContext
 *
 * @param  ctx  Pointer to the KBlockContext to which the error
 *              handler callback function will be assigned
 *
 * @param  callback  Function pointer to the error handler
 *                   callback function that is to be assigned
 *                   to the KBlockContext pointed to by ctx
 *
 * @param  userdata  Pointer to userdata that may be useful within
 *                   the error handler when invoked
 *
 * This useful function assigns an error handler function to to a
 * KBlockContext, which will be automatically invoked in the event that
 * a KBlockContext related API call generates an error.  Although not
 * required, registering an error handler alleviates the burden of
 * needing to manually check KErrorCore return values, which greatly
 * simplifies and beautifies code using Kaze's KBlocks.
 *
 * An error handler callback function has the following form:
 *
 * @code
 *   void error_handler (KErrorCode err, void* userdata) {
 *       ...
 *   }
 * @endcode
 *
 * and is registered to a KBlockContext by, for example:
 *
 * @code
 *   KBlockContext convolve;
 *
 *   convolve = kblock_new_context("convolve");
 *   if (!convolve)
 *       return EXIT_FAILURE;
 *
 *   kblock_error_handler(convolve, error_handler, NULL);
 *
 *   ...
 * @endcode
 *
 * Instead of passing NULL, a pointer to objects that may need to be
 * free()d or otherwise handled by the error handler may be passed as
 * the userdata argument.  Upon encountering an error, the error_handler
 * callback will be invoked automatically with the encountered error
 * being passed in as the first paramemter and the pointer to the
 * specified userdata as the second.
 */
void kblock_error_handler (KBlockContext* ctx, void (*callback)(KErrorCode err, void* userdata), void* userdata);

/**
 * @brief  Destroys a KBlockContext
 *
 * @param  ctx  Pointer to KBlockContext to destroy
 *
 * This function destroys the KBlockContext pointed to by ctx by
 * freeing the dynamic resources allocated to the context.
 */
void kblock_free_context (KBlockContext* ctx);

/**
 * @brief  Sets a KBlockContext input
 *
 * @param  ctx  Pointer to KBlockContext
 *
 * @param  key  String containing the name of the input to be set
 *
 * @param  ...  Pointer to data that will be set to the input
 *
 * @return  If an invalid input name is specified, INVALID_INPUT is
 *          returned.  Specifying an output returns WRONG_IO_DIRECTION.
 *          Otherwise, kblock_set_input() returns SUCCESS.
 *
 * KBlockContext inputs only accept pointers to data, by definition,
 * and are addressed by name, which is expressed in the form of a
 * string.  Inputs are, be definition, not mutated by the
 * KBlockContext.
 *
 * Example usage:
 * @code
 *   KImage* img;
 *   KBlockContext* threshold;
 *   KErrorCore err;
 *
 *   kmodule_load_bank();
 *
 *   img = kimage_read ("foo.png");
 *
 *   threshold = kblock_new_context("threshold");
 *   if (!threshold)
 *       return EXIT_FAILURE;
 *
 *   err = kblock_set_input (threshold, "input", img);
 *
 *   ...
 * @endcode
 *
 * In this example, the name of the KBlockContext input is simply
 * "input".  Naturally, this is not always the case.
 */
KErrorCode kblock_set_input (KBlockContext* ctx, const char* key, ...);

/**
 * @brief  Sets a KBlockContext option
 *
 * @param  ctx  Pointer to KBlockContext
 *
 * @param  key  String containing the name of the option to be set
 *
 * @param  ...  Value to be assigned to option
 *
 * @return  If an invalid option name is specified, kblock_set_option()
 *          returns INVALID_OPTION.  If an out-of-bounds (or otherwise
 *          invalid) value is specified, BAD_OPTION_VALUE is returned.
 *          Otherwise, SUCCESS is returned.
 *
 * This function provides the means to set a KBlockContext option.
 * Setting an option explicitly is not required since all KBlockContext
 * options have default values.  Furthermore, option values undergo
 * automatic  input validation, which is performed internally by the
 * KBlock system.
 *
 * Example usage:
 * @code
 *   KImage* img;
 *   KBlockContext* threshold;
 *   KErrorCore err;
 *
 *   kmodule_load_bank();
 *
 *   img = kimage_read ("foo.png");
 *
 *   threshold = kblock_new_context("threshold");
 *   if (!threshold)
 *       return EXIT_FAILURE;
 *
 *   err = kblock_set_option (threshold, "fill_value", 0);
 *   ...
 *
 *   err = kblock_set_option (threshold, "direction", "below");
 *   ...
 *
 * @endcode
 */
KErrorCode kblock_set_option (KBlockContext* ctx, const char* key, ...);

/**
 * @brief  Retrieves a KBlockContext output pointer
 *
 * @param  ctx  Pointer to KBlockContext
 *
 * @param  key  String containing the name of the output to retrieve
 *
 * @return  Upon success kblock_get_output() returns a void* pointer to
 *          the output data.  It is the responsibility of the caller to
 *          either implicitly convert or cast before dereferencing the
 *          returned pointer.  A NULL pointer is returned on error.  If
 *          an error handler has been assigned to the KBlockContext via
 *          kblock_error_handler(), it is possible receive a KErrorCode
 *          indicating the nature of the error.  Specifying an invalid
 *          output name with generate INVALID_OUTPUT.  If an input name
 *          is specified, WRONG_IO_DIRECTION will be generated.
 *
 *
 * All KBlockContext outputs are, by definition, pointers.  Output
 * pointers are always allocated by the KBlockContext and it is the
 * responsibility of the caller to free them as appropriate.
 * kblock_get_output() is used to retrieve an output pointer by
 * specifying the pointer to the desired KBlockContext and a string
 * containing the name of the desired output.
 */
void* kblock_get_output (KBlockContext* ctx, const char* key);

/**
 * @brief  Executes a KBlockContext
 *
 * @param  ctx  Pointer to KBlockContext
 *
 * @return  Possible error codes returned by kblock_execute()
 *          depend on the KBlock used to produce the KBlockContext
 *          being executed.  Any KErrorCode defined in kerror.h can
 *          potentially be returned.
 *
 * This function executes a KBlockContext.
 */
KErrorCode kblock_execute (KBlockContext* ctx);


#endif   /* _kblock_h_ */
