/**
 * @file   klist.h
 * @brief  Provides linked lists
 *
 * KLists provide a simple double linked list implementation, which can
 * be used to build basic data structures such as stacks, queues, bags,
 * etc.
 */
#ifndef _klist_h_
#define _klist_h_

#include "kmacros.h"


/**
 * @brief  A Kaze Linked List Object
 *
 * Structures can easily be made linkable by adding a `KList` member and
 * using the accompanying `klist_*` API.
 */
typedef struct KList KList;
struct KList {
    /** Pointer to next list entry */
    KList *next;
    /** Pointer to previous list entry */
    KList *prev;
};


/**
 * @brief  Initialize a global KList head
 *
 * @param  name  The KList head to initialize
 *
 * Functionally equivalent to KLIST_HEAD() but a little more clunky.
 *
 * Example usage:
 * @code
 * static KList foo_list = KLIST_HEAD_INIT(foo_list);
 * @endcode
 */
#define KLIST_HEAD_INIT(name) { &(name), &(name) }

/**
 * @brief  Initialize a global KList head more elegantly
 *
 * @param  name  The KList head to initialize
 *
 * Functionally equivalent to KLIST_HEAD_INIT() but provides a little
 * syntactical sugar.
 *
 * Example usage:
 * @code
 * static KLIST_HEAD(foo_list);
 * @endcode
 */
#define KLIST_HEAD(name) \
    KList name = KLIST_HEAD_INIT(name)


/**
 * @brief  Initialize a KList head
 *
 * @param  list  Pointer to KList to initialize
 */
static inline void INIT_KLIST_HEAD(KList* list)
{
    list->next = list;
    list->prev = list;
}


/**
 * @cond  INTERNEL
 *
 * Used internally to perform list additions
 */
static inline void __klist_add (KList* new, KList* prev, KList* next)
{
    next->prev = new;
    new->next = next;
    new->prev = prev;
    prev->next = new;
}
/** @endcond */


/**
 * @brief  Add a new entry
 *
 * @param  new  The entry to be added
 *
 * @param  entry  New entry will be added after this entry
 *
 * Insert a new entry after the specified entry.
 * Useful for implementing stacks.
 */
static inline void klist_add (KList* new, KList* entry)
{
    __klist_add (new, entry, entry->next);
}


/**
 * @brief  Add a new entry
 *
 * @param  new  The entry to be added
 *
 * @param  entry  New entry will be added before this entry
 *
 * Insert a new entry before the specified entry.
 * Useful for implementing queues.
 */
static inline void klist_add_tail (KList* new, KList* entry)
{
    __klist_add (new, entry->prev, entry);
}


/**
 * @brief  Remove an entry
 *
 * @param  entry  The entry to be removed
 *
 * Remove an entry from the list
 */
static inline void klist_del (KList* entry)
{
    entry->next->prev = entry->prev;
    entry->prev->next = entry->next;
    entry->next = NULL;
    entry->prev = NULL;
}


/**
 * @brief  Replace an old entry with a new one
 *
 * @param  old  Entry to replace
 *
 * @param  new  Entry to insert
 */
static inline void klist_replace (KList* old, KList* new)
{
    new->next = old->next;
    new->next->prev = new;
    new->prev = old->prev;
    new->prev->next = new;
    old->prev = NULL;
    old->next = NULL;
}


/**
 * @brief  Tests if a KList is empty
 *
 * @param  list  The KList to test
 */
static inline int klist_empty(const KList *list)
{
    return list->next == list;
}


/**
 * @brief  Get the struct for this entry
 *
 * @param  ptr  The &KList pointer
 *
 * @param  type  The type of the struct housing @ptr
 *
 * @param  member  The name of the KList within the housing struct
 */
#define klist_entry(ptr, type, member) \
    container_of(ptr, type, member)


/**
 * @brief Get first element of list
 *
 * @param  ptr  The &KList head pointer
 *
 * @param  type  The type of the struct housing @ptr
 *
 * @param  member  The name of the KList within the housing struct
 */
#define klist_first_entry(ptr, type, member) \
    container_of((ptr)->next, type, member)


/**
 * @brief Get the last element of list
 *
 * @param  ptr  The &KList head pointer
 *
 * @param  type	 The type of the struct housing @ptr
 *
 * @param  member  The name of the KList within the housing struct
 *
 * Note, the list is expected to be not empty.
 */
#define klist_last_entry(ptr, type, member) \
    klist_entry((ptr)->prev, type, member)


/**
 * @brief  Get the next entry in the list
 *
 * @param  pos  The type * to cursor
 *
 * @param  member  The name of the KList within the housing struct
 */
#define klist_next_entry(pos, member) \
    klist_entry((pos)->member.next, typeof(*(pos)), member)


/**
 * @brief  Iterate over list
 *
 * @param  pos  The &KList to use as a loop cursor
 *
 * @param  head  Head of the list to iterate over
 */

#define klist_for_each(pos, head) \
    for (pos = (head)->next; pos != (head); pos = pos->next)


/**
 * @brief  Iterate over items embedding KList
 *
 * @param  pos  Pointer of housing struct type to use as a loop cursor
 *
 * @param  head  The KList head
 *
 * @param  member  The name of the KList within the housing struct
 */
#define klist_for_each_entry(pos, head, member)               \
    for (pos = klist_first_entry(head, typeof(*pos), member); \
         &pos->member != (head);                              \
            pos = klist_next_entry(pos, member))


/**
 * @brief  klist_for_each_entry() but safe against entry removal
 *
 * @param  pos  Pointer of housing struct type to use as a loop cursor
 *
 * @param  n  Another pointer of housing struct type for temp storage
 *
 * @param  head  The KList head
 *
 * @param  member  The name of the KList within the housing struct
 */
#define klist_for_each_entry_safe(pos, n, head, member)         \
    for (pos = klist_first_entry(head, typeof(*pos), member),	\
        n = klist_next_entry(pos, member);			            \
        &pos->member != (head);                                 \
        pos = n, n = klist_next_entry(n, member))


#endif  /* _klist_h_ */
