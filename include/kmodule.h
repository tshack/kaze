/**
 * @file   kmodule.h
 * @brief  Provides the KModule Framework
 * @author James A. Shackleford
 * @date   March 26, 2016
 *
 * Most true functionality accessible through the Kaze API is provided
 * by `KModule`s.  Kaze module's are build around the KModule framework
 * and can be built directly into the Kaze library or loaded dynamically
 * at runtime by the application linking aginst the Kaze library.
 * For example, modules may register new file format support by
 * registering a new FileOps structure with the KImage subsystem or they
 * may provide new algorithmic building blocks by registering a new
 * KBlock with the algorithmic block subsystem.
 *
 */
#ifndef _kmodule_h_
#define _kmodule_h_

#include <stddef.h>

#include "kerror.h"


/**
 * @brief The Kaze Module Object
 */
typedef struct KModule KModule;
struct KModule {
    /**
     * A short description of the module */
    const char* description;

    /**
     * load() is called when the module is loaded into the Kaze module
     * sub-system.  This occurs either when kmodule_load_bank() is
     * called (i.e. when internal modules are loaded) or when
     * kmodule_load() is called (i.e. when external modules
     * are loaded).  */
    KErrorCode (*load)();

    /**
     * unload() is called when the module is unloaded from the Kaze
     * module sub-system.  This occurs when kmodule_unload_bank() is
     * called (i.e. when internal modules are unloaded) */
    KErrorCode (*unload)();
};


/**
 * @brief  Populates the module bank with all of the modules
 *         built into Kaze at compile time
 *
 * This function populates the module bank with all of the KModules that
 * were built into Kaze at compile time.  Since the module bank state is
 * held internal to the Kaze library, this function only needs to be
 * called once (independent of function scope) unless, of course, it is
 * manually unloaded via kmodule_unload_bank().
 */
void kmodule_load_bank (void);

/**
 * @brief  Unloads all modules from the Kaze module bank
 *
 * This function unloads all KModules that have been previously loaded
 * into the Kaze module bank.  This includes both internal KModules that
 * where compiled into the Kaze library and loaded via kmodule_load_bank()
 * as well as external KModules that were compiled as *.km files and
 * later loaded into the bank via kmodule_load().
 */
void kmodule_unload_bank (void);

/**
 * @brief  Loads a Kaze Module file (*.km) from disk into the Kaze
 *         module bank
 *
 * @param  filename  Path to the Kaze Module file to load into Kaze
 *                   module bank
 *
 * @return  If the specified Kaze Module file cannot be found or opened,
 *          FILE_ERROR will be returned; otherwise, SUCCESS is returned.
 *
 * This function accepts a string containing the path to a Kaze Module
 * file (i.e. a KModule that was built as an independent file by the
 * build system).  If the specified Kaze Module file is found on disk,
 * then it is loaded into the Kaze module bank.
 *
 * Note that if your application does not need any of the KModules that
 * come built into Kaze, you can choose to populate Kaze's module bank
 * using only kmodule_load().  In such a situation, there is absolutely
 * no need to call kmodule_load_bank().
 */
KErrorCode kmodule_load (const char* filename);

#endif /* _kmodule_h_ */
