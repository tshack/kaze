/**
 * @file   kzipper.h
 * @brief  Provides variadic array constructors
 * @author James A. Shackleford
 * @date   August 23rd, 2016
 *
 * This file provides the `kzip_*()` family of varaidic array
 * constructors, collectively referred to as `KZippers`.
 *
 * In Kaze, KZippers provide a variadic means of interfacing with user
 * API functions that require an array of numbers specifying either
 * coordinates or dimensions.  For example, consider writing a series of
 * pixels in a loop with `kimage_set_pixel()`, which accepts an array of
 * `unsigned int`s specifying the pixel coordinate to modify.  To set a
 * 10x10 patch of pixels in channel 0 to the value 25, one could write:
 *
 * ```C
 * int i, j;
 * int coords[2];
 * ...
 * for (j=0; j<10; j++)
 *     for (i=0; i<10; i++) {
 *         coords[0] = i;
 *         coords[1] = j;
 *         kimage_set_pixel (img, 25, 0, coords);
 *     }
 * ...
 * ```
 *
 * or, using KZippers:
 *
 * ```C
 * int i, j;
 * int b[2];
 * ...
 * for (j=0; j<10; j++)
 *     for (i=0; i<10; i++)
 *         kimage_set_pixel (img, 25, 0, kzip(b, i, j));
 * ...
 * ```
 *
 * In our example, we use the polymorphic `kzip()`, which is capable of
 * working with `int`, `unsigned int`, and `double`.  This is
 * accomplished using a C11 `_Generic` selection macro, which
 * effectively replaces `kzip()` with the KZipper implementation that
 * corresponds to the type of the provided buffer array.  For example,
 * we could have equivalently written our previous example as:
 *
 * ```C
 * int i, j;
 * int b[2];
 * ...
 * for (j=0; j<10; j++)
 *     for (i=0; i<10; i++)
 *         kimage_set_pixel (img, 25, 0, kzip_int(2, b, i, j));
 * ...
 * ```
 *
 * however, the C pre-processor will do this replacement for you when
 * using the `kzip()` interface.
 *
 * The value of KZippers becomes more obvious when realizing the general
 * nature of Kaze API functions such as `kimage_set_pixel()`, which is
 * capable of working on images with arbitrary dimensionality.  For
 * example, we can easily extend our previous example to 3-dimensions:
 *
 * ```C
 * int i, j, k;
 * int b[3];
 * ...
 * for (k=0; k<10; k++)
 *     for (j=0; j<10; j++)
 *         for (i=0; i<10; i++)
 *             kimage_set_pixel (img, 25, 0, kzip(b, i, j, k));
 * ...
 * ```
 */
#ifndef _kzipper_h_
#define _kzipper_h_

/**
 * @brief  Populates and returns an `unsigned int` array
 *
 * @param  n  The number of elements to populate in the array
 *
 * @param  array  The array to populate
 *
 * @param  ...  Comma separated list of values to use when
 *              populating `array`
 *
 * @return  The starting address of `array`.  This is a convience so
 *          that `kzip_uint()` can be used in place of a function
 *          parameter requiring an `unsigned int*`.
 */
unsigned int* kzip_uint (size_t n, unsigned int* array, ...);

/**
 * @brief  Populates and returns an `int` array
 *
 * @param  n  The number of elements to populate in the array
 *
 * @param  array  The array to populate
 *
 * @param  ...  Comma separated list of values to use when
 *              populating `array`
 *
 * @return  The starting address of `array`.  This is a convience so
 *          that `kzip_int()` can be used in place of a function
 *          parameter requiring an `int*`.
 */
int* kzip_int (size_t n, int* array, ...);

/**
 * @brief  Populates and returns an `double` array
 *
 * @param  n  The number of elements to populate in the array
 *
 * @param  array  The array to populate
 *
 * @param  ...  Comma separated list of values to use when
 *              populating `array`
 *
 * @return  The starting address of `array`.  This is a convience so
 *          that `kzip_double()` can be used in place of a function
 *          parameter requiring a `double*`.
 */
double* kzip_double (size_t n, double* array, ...);

/**
 * @cond INTERNAL
 * Performs function selection based on array type
 */
#define __kzip(n, ptr, ...) _Generic((ptr), \
        unsigned int* : kzip_uint, \
        int* : kzip_int, \
        double* : kzip_double)(n, ptr, __VA_ARGS__)
/** @endcond */

/**
 * @brief  Populates and returns an array of type `int`,
 *         `unsigned int`, or `double`.
 *
 * @param  array  The array to populate
 *
 * @param  ...  Comma separated list of values to use when
 *              populating `array`
 *
 * @return  The starting address of `array`.  This is a convience so
 *          that `kzip()` can be used in place of a function parameter.
 *
 * `kzip()` is a polymorphic KZipper, which can be used to populate
 * arrays of type `int`, `unsigned int`, or `double`.  Technically,
 * `kzip()` is a `_Generic` selection macro.  This means that the type
 * of the supplied array is used by the C preprocessor to automatically
 * determine and substitute the appropriate `kzip_*()` function prior to
 * compile.
 *
 * **Note:** The first parameter to `kzip()` _must_ be an array
 * (not a pointer).
 *
 * **Compiler Compatability:** `_Generic` selection macros were
 * introduced in C11, and therefore require a modern C compiler (e.g.
 * gcc >= 4.9)
 *
 */
#define kzip(array, ...) \
    __kzip(sizeof(array)/sizeof(*array), &(array)[0], __VA_ARGS__)

#endif /* _kzipper_h_ */
