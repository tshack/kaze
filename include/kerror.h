/**
 * @file   kerror.h
 * @brief  Provides KErrorCodes
 * @author James A. Shackleford
 * @date   July 6, 2016
 *
 * This file provides the enumeration KErrorCode, which is used by Kaze
 * to report runtime errors. Most Kaze functions not returning pointers
 * return KErrorCodes.  Additionally, the function kerror() is provided,
 * which accepts a KErrorCode and returns a human readable string
 * literal description.
 */
#ifndef _kerror_h_
#define _kerror_h_

/**
 * @brief  KErrorCodes are used by Kaze to describe errors encountered
 *         at runtime.
 */
typedef enum {
    SUCCESS,                /**< everything is peaches and roses            */
    FAILURE,                /**< unspecified failure (TRY NOT TO USE THIS!) */
    MODULE_ERROR,           /**< module implementation error                */
    NO_CONTEXT,             /**< context creation failed                    */
    WRONG_IO_DIRECTION,     /**< violated I/O directionality                */
    NULL_INPUT,             /**< received unexpected NULL input             */
    INVALID_INPUT,          /**< invalid input name supplied                */
    INVALID_OUTPUT,         /**< invalid output name supplied               */
    INVALID_OPTION,         /**< invalid option name supplied               */
    BAD_OPTION_VALUE,       /**< supplied option value invalid              */
    MEM_FAIL,               /**< failed malloc(), realloc() etc             */
    BAD_FILE_HEADER,        /**< read bad file header (any format)          */
    UNSUPPORTED_FORMAT,     /**< file format not supported                  */
    INTERNAL_ERROR,         /**< 3rd party library unspecified error        */
    FILE_ERROR,             /**< failed to open/close/etc a file            */
    DIMENSION_MISMATCH,     /**< supplied dimensions not compatible         */
    PIXEL_TYPE_MISMATCH,    /**< supplied KImage KPixelTypes don't match    */
    CHANNEL_MISMATCH,       /**< datatypes have unequal # of channels       */
    UNSUPPORTED_PIXEL_TYPE, /**< supplied KImage has unsupported pixel type */
    UNSUPPORTED_DIMENSIONS, /**< supplied KImage has unsupported dimensions */
    _NUM_KERROR_CODES       /* [UNUSED] internal use only, must be last   */
} KErrorCode;


/**
 * @brief  Converts a KErrorCode into a human readable string
 *
 * @param  err  KErrorCode to get description for
 *
 * @return  A string literal containing a description of the
 *          supplied KErrorCode
 */
char* kerror (KErrorCode err);

#endif /* _kerror_h_ */
