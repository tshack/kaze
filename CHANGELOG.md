# The Kaze Changelog

This project adheres to [Semantic Versioning](http://semver.org).

Here you will find a reverse chronological log of notable additions,
changes, deprecations, removals, and fixes.

## [0.2.0] - 2016-09-12
### Added
- New KImage file operations (`KFileOps`) system, which allows modules
  to extend file format read/write capabilites.  The generic interfaces
  `kimage_read()` and `kimage_write()` check the provided file extension
  and use the corresponding file format module automatically.

- New Module: "PNG", which provides PNG file read/write functionality
  via `kimage_read()` and `kimage_write()`

- New Module: "TIFF", which provides TIFF file read/write functionality
  via `kimage_read()` and `kimage_write()`

- New Module: "downsample", which provides the ability to shrink an image
  by a floating point factor between 0.0001 and 1.0 (eg. 0.01% and 100%)

- KBlock subsystem.  Algorithmic functionality from KModule has been
  moved to the new KBlock subsystem.

- New type `KObject`, which can be embedded in a `struct` to provide
  thread safe reference counting and automatic object destruction.
  (See `kobject_obtain()` and `kobject_release()`)

- Documentation: New guide `doc/WritingKBlocks.md`

- Documentation: New guide `doc/WritingKFileOps.md`

### Changed
- Bumped minimum required version of libpng to (>=1.2.9)

- KModule is now generic.  KModules now do not prescribe purpose.
  A KModule can now be used to register new file format I/O capabilities
  with the KImage subsystem or to register a new algorithmic block with
  the KBlock subsystem.

- Modules now have a manditory unload hook (called `unload()`)

- `kmodule_error_handler()` => `kblock_error_handler()`

- `kmodule_execute()` => `kblock_execute()`

- `kmodule_free_context()` => `kblock_free_context()`

- `kmodule_get_output()` => `kblock_get_output()`

- `kmodule_new_context()` => `kblock_new_context()`

- `kmodule_set_input()` => `kblock_set_input()`

- `kmodule_set_option()` => `kblock_set_option()`

- `kimage_free()` => `kimage_release()`

- The `convolve` Block was completely rewritten and has been
  greatly simplified

- Completely rewrote CONTRIBUTING.md

- Documentation: Rewrote `doc/ModuleGuide.md`

### Fixed
- Small memory leak when writing PNG files

- Adding library dependencies via `module_link_libraries()` in CMake for
  a disabled module used to throw an error regarding adding link
  libraries to a non-existing build target.  This has been fixed.

- Bug in `kmodule_load()` that prevented the loading of external modules

- Forced C++ linkage imposed by CMake when building `libkaze.so` with
  `Internal` C++ modules

### Removed
- `load_png()` -- superseded by `kimage_read()`

- `write_png()` -- superseded by `kimage_write()`

- `load_tiff()` -- superseded by `kimage_read()`

- `write_tiff()` -- superseded by `kimage_write()`


## [0.1.0] - 2016-08-30
### Added
- New type: `KImage`, which provides an N-dimensional, M-channel image
  representation supporting `unsigned char`, `int`, `unsigned int`, and
  `float` pixel types.  Pixel buffers are mutable and can be converted
  between supported types in-place.

- Support for reading/writing `KImage` objects using the PNG file format

- Support for reading/writing `KImage` objects using the TIFF file
  format

- A dimensionally agnostic iterator for KImages called `KImageIterator`.

- KZippers, which provide a limited polymorphic variadic interface for
  functions accepting arrays of numbers.  These provide users of the C
  API with increased usability while maintaining the "wrapability" of
  the core Kaze user API, which avoids variadic functions due to lack of
  interface support from other languages.

- A modular algorithm interface.  Algorithm modules provide a
  standardized data flow model and built-in input validation with
  default value support.

- Build system for modules.  Modules can either be built into the Kaze
  library directly, built as runtime loadable module files, or disabled
  completely.  The default build type can be easily changed in a
  module's CMakeLists.txt.  Custom builds for packaging are easily
  performed by toggling module build modes using `cmake`, `ccmake`, or
  `cmake-gui`.

- The ability to write modules in either C or C++

- New type: `KModule`, which is used to define a Kaze module.

- New type: `KModuleContext`, which provides a stateful context of a
  `KModule` implementing an image processing algorithm

- A reasonably sane error code system

- The ability to register an error handler callback function with a
  `KModuleContext`, which will automatically be called upon encountering
  an error.  Error handler callbacks are automatically passed the
  encountered error along with a registered pointer to user data.

- New type: `KList`, which provides a linked list implementation

- New Module: "average", which provides simple image averaging by
  creating a new image where pixel values are determined by the average
  value of their surrounding neighborhood.  Neighborhood size is an
  option.

- New Module: "convolve", which performs convolution on an image given a
  kernel

- New Module: "gausskernel", which generates a `KImage` populated with a
  Gaussian kernel of specified standard deviation

- New Module: "sobelkernel", which generates a `KImage` populated with
  Sobel kernels

- New Module: "subtract", which subtracts two images to produce a third

- New Module: "threshold", which performs simple pixel intensity
  thresholding

- New Module: "c_skeleton", which provides a template for module
  developers who want to write a new Kaze module in C.

- New Module: "cxx_skeleton", which provides a template for module
  developers who want to write a new Kaze module in C++.
