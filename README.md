# Welcome to Kaze

Kaze is a scientific image processing library written in C.

The official Git repository can be found here:
[gitlab.com/tshack/kaze](https://gitlab.com/tshack/kaze)

Currently, Kaze provides:

  * A robust dynamic module system.  The Kaze library itself is a
    lightweight runtime system. Functionality is provided and extended
    by Kaze modules, which can be built directly into Kaze or loaded
    into the library at runtime.  Applications linking against Kaze can
    add support for additional file types and introduce new algorithms
    after release by shipping out modules to users.  Modules can be
    written in either C or C++.

  * A modular algorithm system called _Kaze Blocks_.  Kaze Blocks are
    built on top of Kaze's dynamic module system and can be distributed
    as files, which are loadable at runtime.  Kaze Blocks provide
    standardized interfaces for setting algorithm options and marshaling
    data.  With little effort, algorithm modules can be strung together
    to build powerful pipelines.

  * A reference based memory management system, which facilitates
    complicated workflows where object ownership may change or be
    managed across multiple threads of execution.

  * An N-dimensional image type providing mutable pixel buffers,
    in-place pixel type conversion, unlimited channels, and smart
    pixel iteration.

  * Beautifully detailed API documentation and author guides.

  * An ever increasing inventory of Kaze Blocks and file I/O modules!


# Official Documentation

In addition to the source code itself, documentation is provided in the
form of Doxygen generated HTML called **The Kaze Zenbook**.  Every
commit to the Kaze master branch results in **The Kaze Zenbook** being
automatically regenerated and uploaded to the following URL:

[www.libkaze.org](http://www.libkaze.org)


# Build Dependencies

The base Kaze library has no build dependencies, although some C11
constructs (e.g. `_Generic`) and GCC extensions (e.g.  `__sync_*`
atomics) are used sparingly.  Consequently, Kaze should be compiled
using GCC.

Some of the modules expanding Kaze's functionality, however, do have
build dependencies, but building modules is optional.

Current module dependencies:

  * libpng (>= 1.2.9): PNG module
  * libtiff (>= 4.0): TIFF module


# Code Reporting

Concentrated effort is put into ensuring that Kaze is a stable library
that you can depend on as a building block for your work.  Consequently,
every push to the Kaze repository results in both build and unit tests
being automatically ran and reported.  Commits and merges into the
master branch result in publicly viewable coverage reports being
generated that show exactly which parts of the Kaze core are being
tested.  Coverage reports can be viewed here:

[www.libkaze.org/coverage](http://www.libkaze.org/coverage/)

Project activity and historcal information regarding files, commits, and
authors can be viewed here:

[www.libkaze.org/gitstats](http://www.libkaze.org/gitstats/)

General project statistics regarding languages used, code size, etc can
be found at:

[www.libkaze.org/cloc.txt](http://www.libkaze.org/cloc.txt)
