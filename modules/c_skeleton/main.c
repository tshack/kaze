#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"

#include "internal/kblock.h"
#include "internal/kmodule.h"

typedef struct ContextData {
/* Inputs_Outputs: */
    KImage* input;
    KImage* output;
/* Options: */
    unsigned int option1;
    unsigned int option2;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME      DESCRIPTION    VARIABLE         TYPE         DIRECTION */
    { "input",  "Input image",  OFFSET(input),   KIMAGE_PTR,  INPUT  },
    { "output", "Output image", OFFSET(output),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME       DESCRIPTION      VARIABLE          TYPE      DEFAULT   RANGE */
    { "option1", "Some option",    OFFSET(option1),  OPT_INT,  {.i64=3}, 0, UINT_MAX },
    { "option2", "Another option", OFFSET(option2),  OPT_INT,  {.i64=3}, 0, UINT_MAX },
    { NULL }
};


static KErrorCode algorithm (KBlockContext* ctx)
{
    ContextData* ctx_data = ctx->data;

    /* Algorithm goes here */

    return SUCCESS;
}


KBlock block_c_skeleton = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("C Skeleton Module");

    return kblock_register (&block_c_skeleton);
}

static KErrorCode unload ()
{
    return kblock_unregister (&block_c_skeleton);
}
