#include <stddef.h>
#include <stdlib.h>
#include <limits.h>
#include <float.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"

#include "internal/kblock.h"
#include "internal/kmodule.h"


typedef struct ContextData {
    /* Inputs_Outputs: */
    KImage* minuend;
    KImage* subtrahend;
    KImage* output;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION              VARIABLE            TYPE         DIRECTION */
    { "minuend",    "Image to subtract from", OFFSET(minuend),    KIMAGE_PTR,  INPUT  },
    { "subtrahend", "Image to subtract",      OFFSET(subtrahend), KIMAGE_PTR,  INPUT  },
    { "output",     "Image difference",       OFFSET(output),     KIMAGE_PTR,  OUTPUT },
    { NULL }
};


#define SUBTRACT_PIXELS(out, A, B, type, min) \
do { \
    unsigned long n; \
\
    type* a_pixels = kimage_raw_pixels(A); \
    type* b_pixels = kimage_raw_pixels(B); \
    type* o_pixels = kimage_raw_pixels(out); \
    type a, b; \
\
    for (n=0; n<kimage_npix(A)*kimage_nchannels(A); n++) { \
        a = a_pixels[n]; \
        b = b_pixels[n]; \
        o_pixels[n] = b + min < a ? a - b : min; \
    } \
} while (0)


/* maybe pull this into KImage proper? */
static KErrorCode _kimage_different_size (KImage* A, KImage* B)
{
    unsigned int d;

    /* check if images are the same dimension & size */
    if (kimage_ndim(A) != kimage_ndim(B))
        return DIMENSION_MISMATCH;

    for (d=0; d<kimage_ndim(A); d++)
        if (kimage_dim(A, d) != kimage_dim(B, d))
            return DIMENSION_MISMATCH;

    return SUCCESS;
}


static KErrorCode algorithm (KBlockContext* ctx)
{
    ContextData* data = ctx->data;
    KImage* subtrahend = data->subtrahend;
    KImage* minuend = data->minuend;
    KImage* output;


    if (!subtrahend || !minuend)
        return NULL_INPUT;

    if (_kimage_different_size (subtrahend, minuend))
        return DIMENSION_MISMATCH;

    if (kimage_type(subtrahend) != kimage_type(minuend))
        return PIXEL_TYPE_MISMATCH;

    if (kimage_nchannels(subtrahend) != kimage_nchannels(minuend))
        return CHANNEL_MISMATCH;


    data->output = kimage_empty_clone(data->subtrahend);

    switch (kimage_type(data->output)) {
    case char_px:
        SUBTRACT_PIXELS (data->output, minuend, subtrahend,
                unsigned char, 0);
        break;
    case float_px:
        SUBTRACT_PIXELS (data->output, minuend, subtrahend,
                float, -FLT_MAX);
        break;
    case int_px:
        SUBTRACT_PIXELS (data->output, minuend, subtrahend,
                int, INT_MIN);
        break;
    case uint_px:
        SUBTRACT_PIXELS (data->output, minuend, subtrahend,
                unsigned int, 0);
        break;
    }

    return SUCCESS;
}


KBlock block_subtract = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .data_size = sizeof(ContextData)
};

static KErrorCode load ()
{
    kmodule_description ("Subtracts one KImage from another to produce a third");

    return kblock_register (&block_subtract);
}


static KErrorCode unload ()
{
    return kblock_unregister (&block_subtract);
}
