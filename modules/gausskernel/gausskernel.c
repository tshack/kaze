#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#include "internal/kblock.h"
#include "internal/kmodule.h"

typedef struct ContextData {
    /* Inputs_Outputs: */
    KImage* output_kernel;
    /* Options: */
    float sigma;
    unsigned int ndim;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION           VARIABLE                TYPE         DIRECTION */
    { "output",     "Kernel as KImage",    OFFSET(output_kernel),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME         DESCRIPTION                                    VARIABLE            TYPE        DEFAULT     RANGE */
    { "sigma",     "Sigma value for generating gaussian kernels",  OFFSET(sigma),      OPT_FLOAT,  {.flt=1.0}, 0.0f, 20.0f},
    { "dimension", "Number of dimensions of the kernel ",          OFFSET(ndim),       OPT_INT,    {.i64=1},   1   , 10    },
    { NULL }
};


/* Return gaussian value for given sigma and position */
static float gaussian_value (float sigma, int pos)
{
    return (1.0/sqrt(2.0*M_PI*sigma*sigma)) * exp(-(pos*pos)/(2.0*sigma*sigma));
}

static KErrorCode algorithm (KBlockContext* ctx)
{
    unsigned int i;
    int *coords, *dim_sizes, s_kern;
    KImage* kernel;
    KPixel kernel_val;
    KImageIterator kernel_iter;
    ContextData* data = ctx->data;

    s_kern = 6*(int)ceil(data->sigma);
    if (!(s_kern % 2))
        s_kern++;

    coords = malloc (data->ndim*sizeof(int));
    dim_sizes = malloc (data->ndim*sizeof(int));

    for (i = 0; i < data->ndim; i++) {
         dim_sizes[i] = s_kern;
    }

    kernel = kimage_new (float_px, 1, data->ndim, dim_sizes);
    kimage_iterator_init (&kernel_iter, kernel);

    /* Gaussian kernel based on desired sigma and dimensionality */
    for (; kimage_iterator_check(&kernel_iter); kimage_iterator_incr(&kernel_iter)) {
        kernel_val.float_px  = 1.0;
        for (i = 0; i < data->ndim; i++) {
            kernel_val.float_px *= gaussian_value(data->sigma, s_kern/2 - kernel_iter.coords[i]);
        }
        kimage_set_pixel(kernel, kernel_val, 0, kernel_iter.coords);
    }

    data->output_kernel = kernel;
    kimage_iterator_destroy (&kernel_iter);
    free (dim_sizes);
    free (coords);
    return SUCCESS;
}


KBlock block_gausskernel = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Generate a 1D Guassian kernel");

    return kblock_register (&block_gausskernel);
}


static KErrorCode unload ()
{
    return kblock_unregister (&block_gausskernel);
}
