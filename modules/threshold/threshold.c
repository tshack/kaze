#include <stddef.h>
#include <string.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "internal/kblock.h"
#include "internal/kmodule.h"

/* private state data held by a context */
typedef struct ContextData {
    /* Inputs_Outputs: */
    KImage* input_image;
    KImage* output_image;
    /* Options: */
    int threshold;
    int fill_val;
    const char* direction;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION            VARIABLE               TYPE         DIRECTION */
    { "input",      "Image to threshold",  OFFSET(input_image),   KIMAGE_PTR,  INPUT  },
    { "output",     "Thresholded image",   OFFSET(output_image),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME          DESCRIPTION                                    VARIABLE            TYPE      DEFAULT    RANGE */
    { "threshold",  "The threshold boundary value",                OFFSET(threshold),  OPT_INT,  {.i64=128}, 0, 255 },
    { "fill_value", "Set thresholded pixels to this value",        OFFSET(fill_val),   OPT_INT,  {.i64=0},   0, 255 },
    { "direction",  "Set pixels \"above\" or \"below\" threshold", OFFSET(direction),  OPT_STRING, {.str="above"} },
    { NULL }
};


static KErrorCode algorithm (KBlockContext* ctx)
{
    unsigned int n;

    unsigned char* inpix;
    unsigned char* outpix;

    KImage* output;

    ContextData* ctx_data = ctx->data;
    KImage* input  = ctx_data->input_image;

    if (!input)
        return NULL_INPUT;

    if (kimage_type (input) != char_px)
        return UNSUPPORTED_PIXEL_TYPE;

    inpix = kimage_raw_pixels(input);

    ctx_data->output_image = kimage_clone(input);
    output = ctx_data->output_image;
    outpix = kimage_raw_pixels(output);

    /* Do the actual work */
    if (!strcmp ("above", ctx_data->direction)) {
        for (n=0; n<kimage_npix(input); n++) {
            if (inpix[n] > ctx_data->threshold) {
                outpix[n] = ctx_data->fill_val;
            } else {
                outpix[n] = inpix[n];
            }
        }
    } else {
        for (n=0; n<kimage_npix(input); n++) {
            if (inpix[n] < ctx_data->threshold) {
                outpix[n] = ctx_data->fill_val;
            } else {
                outpix[n] = inpix[n];
            }
        }

    }

    return SUCCESS;
}


KBlock block_threshold = {
    .name = MODULE_NAME,
    .data_size = sizeof(ContextData),
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block
};


static KErrorCode load ()
{
    kmodule_description ("Set values below threshold to zero");

    return kblock_register (&block_threshold);
}

static KErrorCode unload ()
{
    return kblock_unregister (&block_threshold);
}
