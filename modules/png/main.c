#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <png.h>

#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#include "internal/kimage.h"
#include "internal/kfileops.h"
#include "internal/kmodule.h"


static KErrorCode _test_magic (FILE* fp)
{
    png_byte header[8];

    if (!fread (header, 1, 8, fp))
        return FILE_ERROR;

    if (png_sig_cmp (header, 0, 8))
        return BAD_FILE_HEADER;

    return SUCCESS;
}


static KErrorCode _load_png_init (FILE* fp, png_structp* png_ptr, png_infop* info_ptr)
{
    *png_ptr = png_create_read_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!*png_ptr)
        return MEM_FAIL;

    *info_ptr = png_create_info_struct (*png_ptr);
    if (!*info_ptr) {
        png_destroy_read_struct(png_ptr, (png_infopp)NULL, (png_infopp)NULL);
        return MEM_FAIL;
    }

    if (setjmp (png_jmpbuf (*png_ptr))) {
        png_destroy_read_struct(png_ptr, info_ptr, (png_infopp)NULL);
        return INTERNAL_ERROR;
    }

    png_init_io (*png_ptr, fp);
    png_set_sig_bytes (*png_ptr, 8);
    png_read_info (*png_ptr, *info_ptr);

    png_set_interlace_handling (*png_ptr);
    png_read_update_info (*png_ptr, *info_ptr);

    return SUCCESS;
}


static KErrorCode _load_png_data (KImage** img, png_structp* png_ptr, png_infop* info_ptr)
{
    unsigned int i, j, c;
    unsigned int width;
    unsigned int height;
    unsigned int nchannels;
    png_byte bit_depth;
    png_byte color_type;
    png_bytep* row_ptrs;
    png_byte* row;
    png_byte* p;
    unsigned char* pixels;
    int b[2];

    if (setjmp (png_jmpbuf (*png_ptr))) {
        png_destroy_read_struct(png_ptr, info_ptr, (png_infopp)NULL);
        if (*img) {
            kimage_release (*img);
            *img = NULL;
        }
        return INTERNAL_ERROR;
    }

    width = png_get_image_width (*png_ptr, *info_ptr);
    height = png_get_image_height (*png_ptr, *info_ptr);
    bit_depth = png_get_bit_depth (*png_ptr, *info_ptr);
    color_type = png_get_color_type (*png_ptr, *info_ptr);

    row_ptrs = malloc (sizeof(*row_ptrs)*height);
    for (j=0; j<height; j++)
        row_ptrs[j] = malloc(png_get_rowbytes (*png_ptr, *info_ptr));

    png_read_image (*png_ptr, row_ptrs);

    /* strip out the alpha channel if it exists */
    if (color_type & PNG_COLOR_MASK_ALPHA)
        png_set_strip_alpha (*png_ptr);

    switch (color_type) {
    case PNG_COLOR_TYPE_GRAY_ALPHA:
    case PNG_COLOR_TYPE_GRAY:
        nchannels = 1;
        if (bit_depth < 8) {
            /* expand to 8-bpp */
            png_set_expand_gray_1_2_4_to_8 (*png_ptr);
        }
        break;
    case PNG_COLOR_TYPE_PALETTE:
        png_set_palette_to_rgb (*png_ptr);
    case PNG_COLOR_TYPE_RGB_ALPHA:
    case PNG_COLOR_TYPE_RGB:
        nchannels = 3;
        break;
    default:
        png_destroy_read_struct (png_ptr, info_ptr, (png_infopp)NULL);
        return UNSUPPORTED_FORMAT;
        break;
    }

    *img = kimage_new (char_px, nchannels, 2, kzip(b, width, height));
    pixels = kimage_raw_pixels(*img);

    /* read channels into KImage as color planes */
    for (j=0; j<height; j++) {
        row = row_ptrs[j];
        for (i=0; i<width; i++) {
            p = &(row[nchannels*i]);
            for (c=0; c<nchannels; c++)
                pixels[width*height*c + width*j + i] = (unsigned char)p[c];
        }
    }

    png_destroy_read_struct (png_ptr, info_ptr, (png_infopp)NULL);
    for (j=0; j<height; j++)
        free (row_ptrs[j]);

    free (row_ptrs);

    return SUCCESS;
}


static KErrorCode _write_png_init (FILE* fp, png_structp* png_ptr, png_infop* info_ptr)
{
    *png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!*png_ptr)
        return MEM_FAIL;

    *info_ptr = png_create_info_struct (*png_ptr);
    if (!*info_ptr)
        return MEM_FAIL;

    if (setjmp (png_jmpbuf (*png_ptr)))
        return INTERNAL_ERROR;

    png_init_io (*png_ptr, fp);

    return SUCCESS;
}


static KErrorCode _write_png_header (KImage* img, png_structp* png_ptr, png_infop* info_ptr)
{
    png_byte color_type;
    png_byte bit_depth = 8;

    if (setjmp (png_jmpbuf (*png_ptr)))
        return INTERNAL_ERROR;

    /* for now, set color_type based on # of channels */
    if (kimage_depth(img) == 3)
        color_type = PNG_COLOR_TYPE_RGB;
    else if (kimage_depth(img) == 1)
        color_type = PNG_COLOR_TYPE_GRAY;
    else
        return UNSUPPORTED_FORMAT;

    png_set_IHDR (*png_ptr, *info_ptr, kimage_width(img), kimage_height(img),
                  bit_depth, color_type, PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info (*png_ptr, *info_ptr);

    return SUCCESS;
}


static KErrorCode _write_png_data (KImage* img, png_structp* png_ptr, png_infop* info_ptr)
{
    unsigned int i, j, c;
    png_bytep* row_ptrs;
    png_byte* row;
    png_byte* p;

    unsigned char* pixels = kimage_raw_pixels(img);
    unsigned int width = kimage_width(img);
    unsigned int height = kimage_height(img);
    unsigned int channels = kimage_depth(img);

    row_ptrs = malloc (sizeof(*row_ptrs)*height);
    for (j=0; j<height; j++)
        row_ptrs[j] = malloc (png_get_rowbytes (*png_ptr, *info_ptr));

    for (j=0; j<height; j++) {
        row = row_ptrs[j];
        for (i=0; i<width; i++) {
            p = &(row[channels*i]);
            for (c=0; c<channels; c++)
                p[c] = pixels[width*height*c + width*j + i];
        }
    }

    if (setjmp (png_jmpbuf (*png_ptr)))
        return INTERNAL_ERROR;

    png_write_image (*png_ptr, row_ptrs);

    /* JAS 07-07-2016 -- this setjmp seems redundant */
    if (setjmp (png_jmpbuf (*png_ptr)))
        return INTERNAL_ERROR;

    png_write_end (*png_ptr, NULL);

    for (j=0; j<height; j++)
        free (row_ptrs[j]);

    free (row_ptrs);

    return SUCCESS;
}


static KImage* png_reader (const char* filename)
{
    FILE *fp;

    KImage* img = NULL;

    png_structp png_ptr;
    png_infop info_ptr;

    fp = fopen (filename, "rb");
    if (!fp)
        return img;

    if (_test_magic (fp))
        goto cleanup;

    if (_load_png_init (fp, &png_ptr, &info_ptr))
        goto cleanup;

    if (_load_png_data (&img, &png_ptr, &info_ptr))
        goto cleanup;

cleanup:
    fclose (fp);
    return img;
}


static KErrorCode png_writer (const char* filename, KImage* img)
{
    FILE* fp;
    png_structp png_ptr;
    png_infop info_ptr;

    KImage* tmp = img;
    KErrorCode err = SUCCESS;

    if (!tmp)
        return NULL_INPUT;

    fp = fopen(filename, "wb");
    if (!fp)
        return FILE_ERROR;

    if (kimage_type (tmp) != char_px) {
        tmp = kimage_clone (img);
        if (!tmp) {
            err = MEM_FAIL;
            goto cleanup;
        } else {
            err = kimage_convert (tmp, char_px);
            if (err)
                goto cleanup;
        }
    }

    err = _write_png_init (fp, &png_ptr, &info_ptr);
    if (err)
        goto cleanup;

    err = _write_png_header (tmp, &png_ptr, &info_ptr);
    if (err)
        goto cleanup;

    err = _write_png_data (tmp, &png_ptr, &info_ptr);
    if (err)
        goto cleanup;

cleanup:
    if (png_ptr)
        png_destroy_write_struct(&png_ptr, &info_ptr);
    if (tmp != img)
        kimage_release (tmp);
    fclose (fp);
    return err;
}


static KFileOps png_fops = {
    .extension = "png",
    .reader = png_reader,
    .writer = png_writer
};


static KErrorCode load ()
{
    kmodule_description ("Provides PNG read/write");

    return kimage_register_fops (&png_fops);
}


static KErrorCode unload ()
{
    return kimage_unregister_fops (&png_fops);
}
