#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "internal/kblock.h"
#include "kzipper.h"
#include "internal/kmodule.h"

/* module_convolve
 *
 * Accepted inputs:
 *     input       KImage      Any dimension
 *     kernel      KImage      Same dimension as "input"
 *
 * Outputs:
 *     output      KImage      Same dimension as "input"
 *
 * Option values:
 *     edge_handle string      "crop", "wrap", or "extend"
 */

typedef struct ContextData {
     /* Inputs_Outputs: */
    KImage* kernel;
    KImage* input_image;
    KImage* output_image;
    /* Options: */
    const char* edge_hand;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION                VARIABLE               TYPE         DIRECTION */
    { "kernel",     "Convolution Kernel",      OFFSET(kernel),        KIMAGE_PTR,  INPUT  },
    { "input",      "Image to convolve",       OFFSET(input_image),   KIMAGE_PTR,  INPUT  },
    { "output",     "Convolved image",         OFFSET(output_image),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME             DESCRIPTION                                       VARIABLE             TYPE        DEFAULT      RANGE */
    { "edge_handle",  "Handle edges: \"crop,\" \"wrap,\" or \"extend\"", OFFSET(edge_hand),   OPT_STRING, {.str="crop"} },
    { NULL }
};


static inline int clip (int pos, int pos_max)
{
    return (int) fmax(0, fmin(pos, pos_max));
}


static KErrorCode DX_convolve (KBlockContext* ctx)
{
    unsigned int i;
    int *coords, *input_dimsize;
    KImageIterator out_iter, kern_iter;
    KPixel input_val, kernel_val, output_val;
    ContextData* data = ctx->data;

    kimage_iterator_init (&out_iter, data->output_image);
    coords = malloc (kimage_ndim(data->input_image)*sizeof(int));
    input_dimsize = malloc (kimage_ndim(data->input_image)*sizeof(int));

    for (i = 0; i < kimage_ndim(data->input_image); i++) {
        input_dimsize[i] = kimage_dim(data->input_image, i);
    }

    if ( !strcmp ("crop", data->edge_hand)) {
        for (; kimage_iterator_check(&out_iter); kimage_iterator_incr(&out_iter)) {
            output_val.float_px = 0.0f;
            kimage_iterator_init (&kern_iter, data->kernel);

            for (; kimage_iterator_check(&kern_iter); kimage_iterator_incr(&kern_iter)) {
                kernel_val.float_px = kimage_pixel (data->kernel, 0, kern_iter.coords).float_px;
                for (i = 0; i < kimage_ndim(data->kernel); i++) {
                    coords[i] = out_iter.coords[i] + input_dimsize[i]/2 - 1 - kern_iter.coords[i];
                }
                input_val.float_px  = kimage_pixel (data->input_image, 0, coords).float_px;
                output_val.float_px += input_val.float_px * kernel_val.float_px;
            }
            kimage_set_pixel(data->output_image, output_val, 0, out_iter.coords);
        }
    } else if ( !strcmp ("wrap", data->edge_hand)) {
        for (; kimage_iterator_check(&out_iter); kimage_iterator_incr(&out_iter)) {
            output_val.float_px = 0.0f;
            kimage_iterator_init (&kern_iter, data->kernel);

            for (; kimage_iterator_check(&kern_iter); kimage_iterator_incr(&kern_iter)) {
                kernel_val.float_px = kimage_pixel (data->kernel, 0, kern_iter.coords).float_px;
                for (i = 0; i < kimage_ndim(data->kernel); i++) {
                    coords[i] = (out_iter.coords[i] + input_dimsize[i]/2 - 1 - kern_iter.coords[i] + input_dimsize[i])%input_dimsize[i];
                }
                input_val.float_px  = kimage_pixel (data->input_image, 0, coords).float_px;
                output_val.float_px += input_val.float_px * kernel_val.float_px;
            }
            kimage_set_pixel(data->output_image, output_val, 0, out_iter.coords);
        }
    } else if ( !strcmp ("extend", data->edge_hand)) {
        for (; kimage_iterator_check(&out_iter); kimage_iterator_incr(&out_iter)) {
            output_val.float_px = 0.0f;
            kimage_iterator_init (&kern_iter, data->kernel);

            for (; kimage_iterator_check(&kern_iter); kimage_iterator_incr(&kern_iter)) {
                kernel_val.float_px = kimage_pixel (data->kernel, 0, kern_iter.coords).float_px;
                for (i = 0; i < kimage_ndim(data->kernel); i++) {
                    coords[i] = clip(out_iter.coords[i] + input_dimsize[i]/2 - 1 - kern_iter.coords[i], input_dimsize[i]-1);
                }
                input_val.float_px  = kimage_pixel (data->input_image, 0, coords).float_px;
                output_val.float_px += input_val.float_px * kernel_val.float_px;
            }
            kimage_set_pixel(data->output_image, output_val, 0, out_iter.coords);
        }
    } else {
        kimage_release (data->output_image);
        kimage_iterator_destroy (&out_iter);
        free (input_dimsize);
        free (coords);
        return BAD_OPTION_VALUE;
    }

    kimage_iterator_destroy (&out_iter);
    kimage_iterator_destroy (&kern_iter);
    free (input_dimsize);
    free (coords);
    return SUCCESS;
}


static KErrorCode D2_convolve (KBlockContext* ctx)
{
    unsigned int i, j, in_w, in_h;
    unsigned int ii, jj;
    unsigned int kern_width, kern_height;
    float *kernel_px, *input, *output;
    float value;

    ContextData* data = ctx->data;

    kern_width  = kimage_width(data->kernel);
    kern_height = kimage_height(data->kernel);
    in_w = kimage_width(data->input_image);
    in_h = kimage_height(data->input_image);

    kernel_px = kimage_raw_pixels(data->kernel);
    input = kimage_raw_pixels(data->input_image);
    output = kimage_raw_pixels(data->output_image);

    if ( !strcmp ("crop", data->edge_hand)) {
        for (j=kern_height-1; j<in_h; j++) {
        for (i=kern_width-1 ; i<in_w; i++) {
            value = 0;
            for (jj=0; jj<kern_height; jj++) {
            for (ii=0; ii<kern_width; ii++) {
                value +=  input[(j-jj)*in_w+(i-ii)]
                         *kernel_px[jj*kern_width+ii];
            }
            }
            output[(j-kern_height/2)*in_w+(i-kern_width/2)] = value;
        }
        }
    } else if ( !strcmp ("wrap", data->edge_hand)) {
        for (j=kern_height/2; j<in_h+kern_height/2; j++) {
        for (i=kern_width/2 ; i<in_w+kern_width/2 ; i++) {
            value = 0;
            for (jj=0; jj<kern_height; jj++) {
            for (ii=0; ii<kern_width; ii++) {
                value +=  input[((in_h+j-jj)%in_h)*in_w+((in_w+i-ii)%in_w)]
                         *kernel_px[jj*kern_width+ii];
            }
            }
            output[(j-kern_height/2)*in_w+(i-kern_width/2)] = value;
        }
        }
    } else if ( !strcmp ("extend", data->edge_hand)) {
        for (j=kern_height/2; j<in_h+kern_height/2; j++) {
        for (i=kern_width/2 ; i<in_w+kern_width/2 ; i++) {
            value = 0;
            for (jj=0; jj<kern_height; jj++) {
            for (ii=0; ii<kern_width; ii++) {
                value +=  input[clip(j-jj,in_h-1)*in_w+clip(i-ii,in_w-1)]
                         *kernel_px[jj*kern_width+ii];
            }
            }
            output[(j-kern_height/2)*in_w+(i-kern_width/2)] = value;
        }
        }
    } else {
        kimage_release (data->output_image);
        return BAD_OPTION_VALUE;
    }

    return SUCCESS;
}


static KErrorCode check_inputs (KBlockContext* ctx)
{
    ContextData* data = ctx->data;
    KImage* kernel = data->kernel;
    KImage* input = data->input_image;

    if (!input)
        return NULL_INPUT;

    if (!kernel)
        return NULL_INPUT;

    if (kimage_ndim(kernel) != kimage_ndim(input))
        return INVALID_INPUT;

    return SUCCESS;
}


/* Use given kernel and convolve given image with it */
static KErrorCode algorithm (KBlockContext* ctx)
{
    unsigned int i;
    unsigned int kernel_size;
    KPixelType iptype;
    KErrorCode err;

    ContextData* data = ctx->data;

    err = check_inputs (ctx);
    if (err)
        return err;

    iptype = kimage_type(data->input_image);
    if (iptype != float_px)
        kimage_convert (data->input_image, float_px);

    /* Create output image */
    data->output_image = kimage_clone(data->input_image);

    /* Convolve image with kernel */
    switch (kimage_ndim(data->input_image)) {
        case 2:
            err = D2_convolve(ctx);
            break;
        default:
            err = DX_convolve(ctx);
            break;
    }

    if (iptype != float_px) {
        if (err == SUCCESS)
            kimage_convert(data->output_image, iptype);

        kimage_convert(data->input_image, iptype);
    }

    return err;
}


KBlock block_convolve = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Convolve an image with a provided 1D kernel");

    return kblock_register (&block_convolve);
}


static KErrorCode unload ()
{
    return kblock_unregister (&block_convolve);
}
