#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#include "internal/kblock.h"
#include "internal/kmodule.h"

#define DELTA 1e10

/* module_downsample.c
 *
 * Accepted inputs:
 *     KImage        1D, 2D, or 3D
 *
 * Outputs:
 *     KImage        1D, 2D, or 3D
 *
 * Option values:
 *     factor        float (0.0001 - 1.0)
 *     averaging     "disable" or "enable"
 *
 */
typedef struct ContextData {
 /* Inputs Outputs: */
    KImage* input;
    KImage* output;
 /* Options: */
    float factor;
    const char* averaging;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION                VARIABLE         TYPE         DIRECTION */
    { "input",      "Image to downsample",     OFFSET(input),   KIMAGE_PTR,  INPUT  },
    { "output",     "Downsampled image",       OFFSET(output),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME          DESCRIPTION                              VARIABLE               TYPE        DEFAULT       RANGE */
    { "factor",    "Downsample scaling factor",              OFFSET(factor),        OPT_FLOAT,  {.flt=0.5f},  0.0001f, 1.0f },
    { "averaging", "\"enable\" or \"disable\" averaging",    OFFSET(averaging),     OPT_STRING, {.str="disable"}        },
    { NULL }
};


static inline int clip (int pos, int pos_max)
{
    return (int) fmax(0, fmin(pos, pos_max));
}


static inline float D3_average_pixel (float* input, float factor,
        unsigned int in_width, unsigned int in_height, unsigned int in_depth,
        unsigned int i, unsigned int j, unsigned int k)
{
    float a, b, c;
    float px1, px2, px3;
    float px4, px5, px6;
    float px7, px8, px9;
    c = k/factor - (int)(k/factor);
    b = j/factor - (int)(j/factor);
    a = i/factor - (int)(i/factor);

    px1  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * (1.0 - b) * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * (1.0 - b)
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * (1.0 - b) * c;

    px2  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * (1.0 - b) * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * (1.0 - b)
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * (1.0 - b) * c;

    px3  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * (1.0 - b) * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * (1.0 - b)
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor-1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * (1.0 - b) * c;

    px4  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a)
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * c;

    px5  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ]
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * c;

    px6  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor),   in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * c;

    px7  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * b * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * b
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor-1), in_width )
                  ] * (1.0 - a) * b * c;

   px8  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * b * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * b
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor),   in_width )
                  ] * b * c;

    px9  =   input[  clip((int)(k/factor-1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * b * (1.0 - c)
           + input[  clip((int)(k/factor),   in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * b
           + input[  clip((int)(k/factor+1), in_depth ) *in_height*in_width
                   + clip((int)(j/factor+1), in_height) *in_width
                   + clip((int)(i/factor+1), in_width )
                  ] * a * b * c;

    return (  px1 + px2 + px3 + px4 + px5
                  + px6 + px7 + px8 + px9  ) / 8.0 ;
}

static KErrorCode D3_downsample (KBlockContext* ctx)
{
    unsigned int i, j, k;
    unsigned int in_width, in_height, in_depth;
    unsigned int out_width, out_height, out_depth;
    const char* averaging;
    float factor;
    float* input;
    float* output;
    KImage* output_im;
    int b[3];

    ContextData* ctx_data = ctx->data;
    factor = ctx_data->factor;
    averaging = ctx_data->averaging;

    in_width = kimage_width(ctx_data->input);
    in_height = kimage_height(ctx_data->input);
    in_depth = kimage_depth(ctx_data->input);
    input = kimage_raw_pixels(ctx_data->input);

    out_width = (int)round(in_width*factor);
    out_height = (int)round(in_height*factor);
    out_depth = (int)round(in_depth*factor);

    if (out_width < 1)
        out_width = 1;

    if (out_height < 1)
        out_height = 1;

    if (out_depth < 1)
        out_depth = 1;

    output_im = kimage_new (float_px, 1, 3,
                            kzip(b, out_width, out_height, out_depth));
    output = kimage_raw_pixels(output_im);

    if ( !strcmp(averaging, "enable") ) {
        if (in_width < 3 || in_height < 3 || in_depth < 3) {
            kimage_release (output_im);
            return INVALID_INPUT;
        }

        for (k = 0; k < out_depth; k++)
        for (j = 0; j < out_height; j++)
        for (i = 0; i < out_width; i++)
            output[k*out_height*out_width+j*out_width+i] =
                D3_average_pixel (input, factor, in_width, in_height, in_depth, i, j, k);
    } else {
        for (k = 0; k < out_depth; k++)
        for (j = 0; j < out_height; j++)
        for (i = 0; i < out_width; i++)
            output[k*out_height*out_width+j*out_width+i] = input[  clip((int)(k/factor), in_depth)*in_height*in_width
                                                 + clip((int)(j/factor), in_height)*in_width
                                                 + clip((int)(i/factor), in_width) ];
    }

    ctx_data->output = output_im;

    return SUCCESS;
}


static inline float D2_average_pixel (float* input, float factor,
        unsigned int in_width, unsigned int in_height, unsigned int i,
        unsigned int j)
{
    float a, b;
    float px1, px2, px3;
    float px4, px5, px6;
    float px7, px8, px9;
    b = j/factor - (int)(j/factor);
    a = i/factor - (int)(i/factor);

    px1  = input[  clip((int)(j/factor-1), in_height) *in_width
                 + clip((int)(i/factor-1), in_width )
                ] * (1.0 - a) * (1.0 - b);
    px2  = input[  clip((int)(j/factor-1), in_height) *in_width
                 + clip((int)(i/factor),   in_width )
                ] * (1.0 - b);
    px3  = input[  clip((int)(j/factor-1), in_height) *in_width
                 + clip((int)(i/factor+1), in_width )
                ] * a * (1.0 - b);

    px4  = input[  clip((int)(j/factor),   in_height) *in_width
                 + clip((int)(i/factor-1), in_width )
                ] * (1.0 - a);
    px5  = input[  clip((int)(j/factor),   in_height) *in_width
                 + clip((int)(i/factor),   in_width )
                ];
    px6  = input[  clip((int)(j/factor),   in_height) *in_width
                 + clip((int)(i/factor+1), in_width )
                ] * a;

    px7  = input[  clip((int)(j/factor+1), in_height) *in_width
                 + clip((int)(i/factor-1), in_width )
                ] * (1.0 - a) * b;
    px8  = input[  clip((int)(j/factor+1), in_height) *in_width
                 + clip((int)(i/factor),   in_width )
                ] * b;
    px9  = input[  clip((int)(j/factor+1), in_height) *in_width
                 + clip((int)(i/factor+1), in_width )
                ] * a * b;

    return (  px1 + px2 + px3 + px4 + px5
                  + px6 + px7 + px8 + px9  ) / 4.0 ;
}

static KErrorCode D2_downsample (KBlockContext* ctx)
{
    unsigned int i, j;
    unsigned int in_width, in_height;
    unsigned int out_width, out_height;
    const char* averaging;
    float factor;
    float* input;
    float* output;
    KImage* output_im;
    int b[2];

    ContextData* ctx_data = ctx->data;
    factor = ctx_data->factor;
    averaging = ctx_data->averaging;

    in_width = kimage_width(ctx_data->input);
    in_height = kimage_height(ctx_data->input);
    input = kimage_raw_pixels(ctx_data->input);

    out_width = (int)round(in_width*factor);
    out_height = (int)round(in_height*factor);

    if (out_width < 1)
        out_width = 1;

    if (out_height < 1)
        out_height = 1;

    output_im = kimage_new (float_px, 1, 2, kzip(b, out_width, out_height));
    output = kimage_raw_pixels(output_im);

    if ( !strcmp(averaging, "enable") ) {
        if (in_width < 3 || in_height < 3) {
            kimage_release (output_im);
            return INVALID_INPUT;
        }

        for (j = 0; j < out_height; j++)
        for (i = 0; i < out_width; i++)
            output[j*out_width+i] =  D2_average_pixel (input, factor, in_width,
                                                       in_height, i, j);
    } else {
        for (j = 0; j < out_height; j++)
        for (i = 0; i < out_width; i++)
            output[j*out_width+i] = input[
                                      clip((int)(j/factor), in_height)*in_width
                                    + clip((int)(i/factor), in_width )
                                     ];
    }

    ctx_data->output = output_im;

    return SUCCESS;
}


static inline float D1_average_pixel (float* input, float factor,
        unsigned int in_width, unsigned int i)
{
    float a;
    float px1, px2, px3;
    a = i/factor - (int)(i/factor);

    px1  = input[ clip((int)(i/factor-1), in_width) ] * (1.0 - a);
    px2  = input[ clip((int)(i/factor  ), in_width) ];
    px3  = input[ clip((int)(i/factor+1), in_width) ] * a;

    return ( px1 + px2 + px3 ) / 2.0;
}

static KErrorCode D1_downsample (KBlockContext* ctx)
{
    unsigned int i;
    unsigned int in_width;
    unsigned int out_width;
    const char* averaging;
    float factor;
    float* input;
    float* output;
    KImage* output_im;
    int b[1];

    ContextData* ctx_data = ctx->data;
    factor = ctx_data->factor;
    averaging = ctx_data->averaging;

    in_width = kimage_width(ctx_data->input);
    input = kimage_raw_pixels(ctx_data->input);

    out_width = (int)round(in_width*factor);

    if (out_width < 1) {
        out_width = 1;
    }

    output_im = kimage_new (float_px, 1, 1, kzip(b, out_width));
    output = kimage_raw_pixels(output_im);

    if ( !strcmp(averaging, "enable") ) {
        if (in_width < 3) {
            kimage_release (output_im);
            return INVALID_INPUT;
        }

        for (i = 0; i < out_width; i++)
            output[i] = D1_average_pixel (input, factor, in_width, i);
    } else {
        for (i = 0; i < out_width; i++)
            output[i] = input[ clip((int)(i/factor), in_width) ];
    }

    ctx_data->output = output_im;

    return SUCCESS;
}


static KErrorCode check_options (KBlockContext* ctx)
{
    ContextData* ctx_data = ctx->data;
    float factor = (float) 1.0/ctx_data->factor;
    const char* averaging = ctx_data->averaging;

    if ( !strcmp(averaging, "disable") ) {
        if (((unsigned int)roundf(factor) - factor) > DELTA)
            return INVALID_OPTION;
        return SUCCESS;
    } else if ( !strcmp(averaging, "enable") )
        return SUCCESS;
    else
        return INVALID_OPTION;
}


static KErrorCode algorithm (KBlockContext* ctx)
{
    KPixelType iptype;
    KErrorCode err;

    ContextData* ctx_data = ctx->data;

    if (!ctx_data->input)
        return NULL_INPUT;

    err = check_options(ctx);
    if (err)
        return err;

    iptype = kimage_type (ctx_data->input);
    if (iptype != float_px)
        kimage_convert (ctx_data->input, float_px);

    /* Do the actual work */
    switch (kimage_ndim (ctx_data->input)) {
        case 1:
            err = D1_downsample (ctx);
            break;
        case 2:
            err = D2_downsample (ctx);
            break;
        case 3:
            err = D3_downsample (ctx);
            break;
        default:
            return UNSUPPORTED_DIMENSIONS;
    }

    if (iptype != float_px) {
        if (err == SUCCESS)
            kimage_convert (ctx_data->output, iptype);
        kimage_convert (ctx_data->input, iptype);
    }

    return err;
}


KBlock block_downsample = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Downsample an image");

    return kblock_register (&block_downsample);
}

static KErrorCode unload ()
{
    return kblock_unregister (&block_downsample);
}
