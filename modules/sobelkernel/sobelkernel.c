#include <stddef.h>
#include <string.h>
#include <math.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#include "internal/kblock.h"
#include "internal/kmodule.h"


typedef struct ContextData {
    /* Inputs & Outputs: */
    KImage* output_kernelx;
    KImage* output_kernely;
    /* Options: */
    int size;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME           DESCRIPTION                      VARIABLE                 TYPE         DIRECTION */
    { "outputX",     "Kernel for x axis as KImage",    OFFSET(output_kernelx),  KIMAGE_PTR,  OUTPUT },
    { "outputY",     "Kernel for y axis as KImage",    OFFSET(output_kernely),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME        DESCRIPTION                                     VARIABLE            TYPE        DEFAULT     RANGE */
    { "size",     "Size of generated sobel kernel",                OFFSET(size),       OPT_INT,    {.i64=3},   0, 5},
    { NULL }
};


/* 3x3 sobel operators */
static const float sobel_x_3x3[] = {-1.f,  0.f,  1.f,
                                    -2.f,  0.f,  2.f,
                                    -1.f,  0.f,  1.f};

static const float sobel_y_3x3[] = {-1.f, -2.f, -1.f,
                                     0.f,  0.f,  0.f,
                                     1.f,  2.f,  1.f};

/* 5x5 sobel operators */
static const float sobel_x_5x5[] = { -1.f,  -2.f,   0.f,   2.f,   1.f,
                                     -4.f,  -8.f,   0.f,   8.f,   4.f,
                                     -6.f, -12.f,   0.f,  12.f,   6.f,
                                     -4.f,  -8.f,   0.f,   8.f,   4.f,
                                     -1.f,  -2.f,   0.f,   2.f,   1.f};

static const float sobel_y_5x5[] = { -1.f,  -4.f,  -6.f,  -4.f,  -1.f,
                                     -2.f,  -8.f, -12.f,  -8.f,  -2.f,
                                      0.f,   0.f,   0.f,   0.f,   0.f,
                                      2.f,   8.f,  12.f,   8.f,   2.f,
                                      1.f,   4.f,   6.f,   4.f,   1.f};


static KErrorCode algorithm (KBlockContext* ctx)
{
    int i, size;
    float* kernelx_px;
    float* kernely_px;
    KImage* kernelx;
    KImage* kernely;
    int b[2];

    ContextData* ctx_data = ctx->data;

    size = ctx_data->size;

    switch (size) {
        case 3:
            kernelx = kimage_new (float_px, 1, 2, kzip(b, size, size));
            kernely = kimage_new (float_px, 1, 2, kzip(b, size, size));

            kernelx_px = kimage_raw_pixels (kernelx);
            kernely_px = kimage_raw_pixels (kernely);

            memcpy (kernelx_px, sobel_x_3x3, 9*sizeof(*sobel_x_3x3));
            memcpy (kernely_px, sobel_y_3x3, 9*sizeof(*sobel_y_3x3));

            break;
        case 5:
            kernelx = kimage_new (float_px, 1, 2, kzip(b, size, size));
            kernely = kimage_new (float_px, 1, 2, kzip(b, size, size));

            kernelx_px = kimage_raw_pixels (kernelx);
            kernely_px = kimage_raw_pixels (kernely);

            memcpy (kernelx_px, sobel_x_5x5, 25*sizeof(*sobel_x_5x5));
            memcpy (kernely_px, sobel_y_5x5, 25*sizeof(*sobel_y_5x5));

            break;
        default:
            return BAD_OPTION_VALUE;
    }

    ctx_data->output_kernelx = kernelx;
    ctx_data->output_kernely = kernely;

    return SUCCESS;
}


KBlock block_sobelkernel = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Generate a Sobel kernel");

    return kblock_register (&block_sobelkernel);
}


static KErrorCode unload ()
{
    return kblock_unregister (&block_sobelkernel);
}
