#include <stddef.h>
#include <stdlib.h>
#include <limits.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "internal/kblock.h"
#include "internal/kmodule.h"


typedef struct ContextData {
    /* Inputs_Outputs: */
    KImage* input;
    KImage* output;
    /* Options: */
    unsigned int radius;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME          DESCRIPTION              VARIABLE         TYPE         DIRECTION */
    { "input",      "Image to filter", OFFSET(input),   KIMAGE_PTR,  INPUT  },
    { "output",     "Averaged image",  OFFSET(output),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME       DESCRIPTION                        VARIABL E        TYPE      DEFAULT   RANGE */
    { "radius",  "Window edge length (must be odd)", OFFSET(radius),  OPT_INT,  {.i64=3}, 0, UINT_MAX },
    { NULL }
};



static unsigned int* get_nhood_dims (unsigned int ndim, unsigned int radius)
{
    unsigned int d;
    unsigned int* nhood_dims;

    nhood_dims = malloc (ndim*sizeof(*nhood_dims));
    for (d=0; d<ndim; d++)
        nhood_dims[d] = radius;

    return nhood_dims;
}


static KImage* new_neighborhood (KImage* img, unsigned int radius)
{
    KImage* nhood;
    unsigned int* nhood_dims;

    nhood_dims = get_nhood_dims (kimage_ndim(img), radius);
    nhood = kimage_new (kimage_type(img), kimage_nchannels(img),
                         kimage_ndim(img), nhood_dims);
    free (nhood_dims);

    return nhood;
}


static KErrorCode algorithm (KBlockContext* ctx)
{
    unsigned int c;
    KImage* nhood;
    KImageIterator iter, niter;
    KPixel val;

    ContextData* data = ctx->data;

    if (!data->input)
        return NULL_INPUT;

    if (data->radius % 2 == 0)
        return BAD_OPTION_VALUE;

    data->output = kimage_empty_clone(data->input);

    nhood = new_neighborhood (data->input, data->radius);
    if (!nhood)
        return MEM_FAIL;

    kimage_iterator_init (&iter, data->input);
    for (; kimage_iterator_check(&iter); kimage_iterator_incr(&iter)) {
        kimage_get_neighborhood (data->input, nhood, iter.coords);

        for (c=0; c<kimage_nchannels(nhood); c++) {
            val.float_px = 0.0f;
            kimage_iterator_init (&niter, nhood);

            for (; kimage_iterator_check(&niter); kimage_iterator_incr(&niter))
                val.float_px += kimage_pixel(nhood, c, niter.coords).float_px;
            val.float_px /= kimage_npix(nhood);

            kimage_set_pixel (data->output, val, c, iter.coords);
        }
    }

    kimage_release (nhood);

    return SUCCESS;
}


KBlock block_average = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Sets pixels to the average value within a sliding window");

    return kblock_register (&block_average);
}

static KErrorCode unload ()
{
    return kblock_unregister (&block_average);
}
