#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <tiffio.h>

#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "kzipper.h"

#include "internal/kimage.h"
#include "internal/kfileops.h"
#include "internal/kmodule.h"


static KImage* tiff_reader (const char* filename)
{
    unsigned int i, j;
    size_t npixels;
    uint32 w, h;
    uint16 c;
    uint32* raster;
    int b[2];

    KImage* img = NULL;
    unsigned char* pix = NULL;
    TIFF* tif = TIFFOpen (filename, "r");
    if (!tif)
        goto cleanup;

    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &w);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &h);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &c);
    npixels = w * h ;
    img = kimage_new (char_px, c, 2, kzip(b, w, h));
    if (!img)
        goto cleanup;
    pix = kimage_raw_pixels (img);

    raster = (uint32*) _TIFFmalloc(npixels * sizeof (uint32));
    if (raster) {
        if (TIFFReadRGBAImage(tif, w, h, raster, 0))
            for (j=h; j-- > 0;)
                for (i=0; i<w; i++) {
                    pix[w*h*0 + w*(h-j-1) + i] = (unsigned char)TIFFGetR(raster[j*w+i]);
                    if (c>1) {
                       pix[w*h*1 + w*(h-j-1) + i] = (unsigned char)TIFFGetG(raster[j*w+i]);
                       pix[w*h*2 + w*(h-j-1) + i] = (unsigned char)TIFFGetB(raster[j*w+i]);
                    }
                    if (c>4) {
                       pix[w*h*3 + w*(h-j-1) + i] = (unsigned char)TIFFGetA(raster[j*w+i]);
                    }
                }
        _TIFFfree(raster);
    }

cleanup:
    TIFFClose(tif);
    return img;
}


static KErrorCode tiff_writer (const char* filename, KImage* img)
{
    unsigned int i,j,c;
    KErrorCode err = SUCCESS;
    TIFF *out = TIFFOpen (filename, "w");
    if (!out) {
        err = INVALID_INPUT;
        goto cleanup;
    }
    unsigned char* pix = kimage_raw_pixels (img);
    unsigned int width = kimage_width (img);
    unsigned int height = kimage_height (img);
    unsigned int channels = kimage_nchannels (img);
    unsigned char* buf = NULL;

    TIFFSetField (out, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField (out, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField (out, TIFFTAG_SAMPLESPERPIXEL, channels);
    TIFFSetField (out, TIFFTAG_BITSPERSAMPLE, 8);
    TIFFSetField (out, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField (out, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);

    if(channels==1)
        TIFFSetField (out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    else
        TIFFSetField (out, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);

    tsize_t linebytes = channels*width;

    if (TIFFScanlineSize(out))
        buf = (unsigned char*)_TIFFmalloc (linebytes);
    else
        buf = (unsigned char*)_TIFFmalloc (TIFFScanlineSize(out));

    if (!buf){
        err = MEM_FAIL;
        goto cleanup;
    }

    TIFFSetField (out, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(out, width*channels));

    for (j=0; j<height; j++) {
        for (i=0; i<width; i++)
            for (c=0; c<channels; c++)
                buf[i*channels+c] = pix[width*height*c + width*j + i];

        if (TIFFWriteScanline(out, buf, j, 0)<0)
            break;
    }

cleanup:
    if (out)
        TIFFClose(out);
    if (buf)
        _TIFFfree(buf);
    return err;
}


static KFileOps tiff_fops = {
    .extension = "tiff",
    .reader = tiff_reader,
    .writer = tiff_writer
};


static KErrorCode load ()
{
    kmodule_description ("Provides TIFF read/write");

    return kimage_register_fops (&tiff_fops);
}


static KErrorCode unload ()
{
    return kimage_unregister_fops (&tiff_fops);
}
