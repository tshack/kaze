Writing a KImage File I/O Module                     {#writing-kfileops}
========================================================================

[TOC]

Each file format supported by `kimage_read()` and `kimage_write()` is
implemented by a Kaze File I/O Module.  Support for additional file
formats is easily achieved by writing addional modules.

Internally, `kimage_read()` and `kimage_write()`:

- grab the file extension from the provided file name

- cycle through the registered File I/O Modules looking for one
  that has claimed responsibility for the specified file extension

- call the File I/O Module's read or write function, as appropriate


Writing a KImage File I/O Module is little more than writing a module
that simply registers a read function and a write function with the
KImage File Operations (KFileOps) system for a specific file extension.

If you haven't already, it is highly recommended that you first read the
[Module Author Guide](@ref moduleguide)


# Defining a KFileOps Structure                       {#kfileops-struct}

A `KFileOps` structure simply associates a particular file extension
with the functions necessary to read and write its corresponding format.
Here is an example of a `KFileOps` initialization taken from the `png`
KImage File I/O module:

```C
static KFileOps png_fops = {
    .extension = "png",
    .reader = png_reader,
    .writer = png_writer
};
```

This associates the "png" file extension with the functions
`png_reader()` and `png_writer()`.  The `KFileOps` attributes `.reader`
and `.writer` expect function pointers with specific formats.  For
example, `.reader` expects a function accepting a filename that returns
a pointer to a new `KImage` (or NULL on failure):

```C
static KImage* png_reader (const char* filename)
{
    /* implementation */
}
```

and `.writer` expects a function accepting a filename and the KImage
to write that returns a `KErrorCode`:

```C
static KErrorCode png_writer (const char* filename, KImage* img)
{
    /* implementation */
}
```


# Registering the KFileOps Structure                {#kfileops-register}

The `KFileOps` structure should be registered with KImage when your
module is loaded and unregistered when your module is unloaded.  This is
easily accomplished using `kimage_register_fops()` and
`kimage_unregister_fops()`, which are provided by
`"internal/kfileops.h"`.  These functions both return a `KErrorCode`
indicating their success or failure.  Borrowing, again, an excerpt from
the `png` File I/O Module as an example:

```C
static KErrorCode load ()
{
    kmodule_description ("Provides PNG read/write");

    return kimage_register_fops (&png_fops);
}


static KErrorCode unload ()
{
    return kimage_unregister_fops (&png_fops);
}
```
