Writing a Kaze Block Module                           {#writing-kblocks}
========================================================================

[TOC]

Kaze Block modules are easy to write and provide a standardized
interface to users of your algorithms.  If you have used Kaze Blocks
written by other people, then you are already familiar with the general
structure of a Kaze Block as seen from the user perspective;
namely, Kaze Blocks have:

  - **inputs**, which accept pointers to input buffers
  - **outputs**, which provide pointers to output buffers
  - **options**, which are integers, floats, and strings that modify the
    behavior of a Kaze Block module.

Of these interfaces, at a bare minimum, a Kaze Block (i.e. `KBlock`)
module must provide at least a single output.  Options, and even inputs,
are not required.  However, it is recommended that you provide options
when applicable.

If you haven't already, it is highly recommended that you first read the
[Module Author Guide](@ref moduleguide)


# The Skeleton of a Kaze Block Module                 {#kblock-skeleton}

The following is a bare bones skeleton for a `KBlock` module.  As shown,
the module has three inputs named `"option1"`, `"option2"`, and
`"option3"`; one input named `"input"`; and one output named `"output"`.

```C
#include <stddef.h>
#include <string.h>

#include "kblock.h"
#include "kerror.h"
#include "kimage.h"
#include "kmodule.h"
#include "internal/kblock.h"
#include "internal/kmodule.h"


typedef struct ContextData {
    /* Inputs & Outputs: */
    KImage* input_image;
    KImage* output_image;

    /* Options: */
    int option1;
    int option2;
    const char* option3;
} ContextData;

#define OFFSET(x) offsetof(ContextData, x)

static const KIOTerminal io_block[] = {
    /* NAME     DESCRIPTION     VARIABLE               TYPE         DIRECTION */
    { "input",  "Input Image",  OFFSET(input_image),   KIMAGE_PTR,  INPUT  },
    { "output", "Output Image", OFFSET(output_image),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};

static const KOption options_block[] = {
    /* NAME       DESCRIPTION           VARIABLE         TYPE      DEFAULT    RANGE */
    { "option1",  "Some option",        OFFSET(option1), OPT_INT,  {.i64=128}, 0, 255 },
    { "option2",  "Another option",     OFFSET(option2), OPT_INT,  {.i64=0},   0, 255 },
    { "option3",  "\"bar\" or \"baz\"", OFFSET(option3), OPT_STRING, {.str="bar"} },
    { NULL }
};


static KErrorCode algorithm (KBlockContext* ctx)
{
    ContextData* data = ctx->data;

    /* Your Algorithm Goes Here */
}


KBlock block_skeleton = {
    .name = MODULE_NAME,
    .algorithm = algorithm,
    .io = io_block,
    .options = options_block,
    .data_size = sizeof(ContextData)
};


static KErrorCode load ()
{
    kmodule_description ("Short algorithm description");

    return kblock_register (&block_skeleton);
}

static KErrorCode unload ()
{
    return kblock_unregister (&block_skeleton);
}
```

The inclusion of `"internal/kblock.h"` provides the functions necessary
to register a new `KBlock`, namely: `kblock_register()` and
`kblock_unregister()`.


## Module Initialization                                  {#kblock-init}

For modules implementing a new `KBlock`, the `load()` function is used
to register the new `KBlock` via `kblock_register()`.  Likewise, the
module's `unload()` function must unregister the `KBlock` via
`kblock_unregister()`.

The `KBlock` is simply a data structure consisting of several attributes
that define the behavior of the Kaze Block:

KBlock Attribute | Accepts                           | Description
-----------------|-----------------------------------|--------------------------------------------------
`.algorithm`     | `KErrorCode (*)(KBlockContext*)`  | Executed on `kblock_execute()` API call
`.data_size`     | `size_t`                          | Sets the size of the block's context storage
`.io`            | `NULL` Terminated `KIOTerminal[]` | Sets the Kaze Block's I/O block
`.options`       | `NULL` Terminated `KOption[]`     | Sets the Kaze Block's options block
`.setup`         | `KErrorCode (*)(KBlockContext*)`  | Executed on `kblock_new_context()` API call
`.teardown`      | `KErrorCode (*)(KBlockContext*)`  | Executed on `kblock_free_context()` API call


## Context Private Storage                     {#kblock-private-storage}

When writing a block it is important to keep in mind that you are
writing a blueprint, similar to a class, that will be used to construct
independent instances called `KBlockContext`s.  It is possible that a
user could create anywhere from 1 to 1000+ instances of your block,
which could all be in flight simultaneously.  Consequently, each of
these contexts must hold private ownership over a chunk of memory where
it can store its state.  The structure of this private storage is
defined at the top of the block skeleton:

```C
typedef struct ContextData {
    /* Inputs & Outputs: */
    KImage* input_image;
    KImage* output_image;

    /* Options: */
    int option1;
    int option2;
    const char* option3;
} ContextData;
```

As shown, pointers to input and output data are stored in context
private storage.  Options that affect the KBlock's behavior are also
stored here.  Anything else that should be persistent between calls to
the same `KBlockContext` may also be stored here.

Since the structure definition of this memory block is only known within
the block module, it is necessary for its size to be specified in the
KBlock's `data_size` attribute.  This is so that Kaze knows how much
memory to set aside while building a `KBlockContext`, which occurs when
a user invokes the `kblock_new_context()` API call.


## The KIOTerminal Block                           {#kblock-kioterminal}

Since the structure of a block's storage is private to a module, it is
impossible for code outside of the module file to modify pointers to
input and output buffers without the existence of some type of
aperture.  A `KIOTerminal` defines such an aperture into a
`KBlockContext`'s private storage, and provides a controlled access
mechanism for code external to the module to modify context private
storage.  We refer to a `NULL` terminated array of `KIOTerminal`s as a
_KIOTerminal Block_, where each `KIOTerminal` provides regulated access
to a single pointer variable in the context storage.

In our example module skeleton, we define one input and one output in
context private storage:

```C
typedef struct ContextData {
    ...
    /* Inputs & Outputs: */
    KImage* input_image;
    KImage* output_image;
    ...
} ContextData;
```

Consequently, we open up regulated access to these pointers through the
following KIOTerminal Block:

```C
static const KIOTerminal io_block[] = {
    /* NAME     DESCRIPTION     VARIABLE               TYPE         DIRECTION */
    { "input",  "Input Image",  OFFSET(input_image),   KIMAGE_PTR,  INPUT  },
    { "output", "Output Image", OFFSET(output_image),  KIMAGE_PTR,  OUTPUT },
    { NULL }
};
```

Notice that each `KIOTerminal` entry in the KIOTerminal Block specifies:

  1. **NAME:** The name of the input, which is used by the user API
     functions `kblock_set_input()` and `kblock_get_output()` when
     setting or retrieving buffer pointers

  2. **DESCRIPTION:** A short description, which can be retrieved by the
     user to populate UI widgets.  These are also used by the Kaze build
     system to generate Kaze Block module documentation.

  3. **VARIABLE:** The memory offset of the variable within the context
     private storage structure.  This is used to construct the memory
     aperture.

  4. **TYPE:** The type of the buffer pointer.  This is used to perform
     type checking and compatability when marshalling data from one Kaze
     Block to another in a pipeline.

  5. **DIRECTION:** The data flow direction of the buffer.  Data flows
     _into_ the Kaze Block from `INPUT`s and _out of_ the Kaze Block
     from `OUTPUT`s.  Furthermore, `OUTPUT` buffers must **always** be
     allocated within the Kaze Block module, and it is the caller's
     responsibility to free output buffers produced by Kaze Block
     modules.


## KBlock Options                                      {#kblock-options}

A `KBlock` option is stored in a context's private storage and is made
accessible to the block's users through a regulated aperture called a
`KOption`.  The definition of a _KOption Block_ is quite similar to a
KIOTerminal Block, however the function and capabilities are slightly
different.  Like a `KIOTerminal`, a `KOption` interface allows for
outside modification of a context private storage variable.  However,
`KOptions` don't manage pointers to buffers; they only manage integers,
floats, and strings.  Additionally, a `KOption` provides a default value
in the event a user does not manually set an option.  Furthermore, if a
user does choose to manually set an option, the `KOption` interface will
perform input range validation before modifying the corresponding
variable in context storage.  This dramatically cuts down on the amount
of error checking code needed to be written by KBlock authors.

In our example KBlock module skeleton, we define three variables in
context storage to be exposed through the `KOption` interface:

```C
typedef struct ContextData {
    ...
    /* Options: */
    int option1;
    int option2;
    const char* option3;
    ...
} ContextData;
```

and we open up regulated access to these variables through the following
KOption Block:

```C
static const KOption options_block[] = {
    /* NAME       DESCRIPTION           VARIABLE         TYPE      DEFAULT    RANGE */
    { "option1",  "Some option",        OFFSET(option1), OPT_INT,  {.i64=128}, 0, 255 },
    { "option2",  "Another option",     OFFSET(option2), OPT_INT,  {.i64=0},   0, 255 },
    { "option3",  "\"bar\" or \"baz\"", OFFSET(option3), OPT_STRING, {.str="bar"} },
    { NULL }
};
```

Notice that `KOption` entries in the KOption Block specify:

  1. **NAME:** The name of the option, which is used by the user API
     function `kblock_set_option()` when setting a Kaze Block option.

  2. **DESCRIPTION:** A short description, which can be retrieved by the
     user to populate UI widgets.  These are also used by the Kaze build
     system to generate Kaze Block module documentation.

  3. **VARIABLE:** The memory offset of the variable within the context
     private storage structure.  This is used to construct the memory
     aperture.

  4. **TYPE:** The type of the option.  This is used to perform type
     and range checking, and is also used by Kaze to provide the memory
     aperture.

  5. **DEFAULT:** The default value of the option.  This value is
     assigned to the underlying variable in the context's storage upon
     `KBlockContext` creation when a user calls `kblock_new_context()`.

  6. **RANGE:** (_optional_) When a user attempts to set a Kaze Block
     option value via `kblock_set_option()`, the new value must fall
     within the provided range.  If the value falls outside, the value
     of the underlying variable remains unchanged and
     `kblock_set_option()` returns a `KErrorCode`.


## Writing the Algorithm                             {#kblock-algorithm}

Your algorithm can be implemented using as many functions as you would
like.  When defining `KBlock`, you must tell Kaze which function is the
entry point to your algorithm.  This is effectively the `main()`
function of your algorithm, which is where execution will being when a
user calls `kblock_execute()`.

Just like the standard `main()` entry point function in C has a
predefined structure, so does a Kaze Block's `algorithm()` function.
Your entry point must be declared `static`, return a `KErrorCode`, and
accept  a `KBlockContext` pointer.  For example:

```C
static KErrorCode algorithm (KBlockContext* ctx)
{
    ContextData* data = ctx->data;

    /* Your Algorithm Goes Here */
}
```

Before discussing further, let's review the user workflow for using your
Kaze Block:

  - First, the user will get a context of your block via
    `kblock_new_context()`.

  - Next, the user will set input buffers using `kblock_set_input()`
    and options using `kblock_set_option()`.

  - The user will execute the algorithm one or more times by calling
    `kblock_execute()` followed by `kblock_get_output()`.

  - Finally, the user will free the context via `kblock_free_context()`

Upon calling `kblock_execute()`, your Kaze Block's algorithm entry point
will be invoked with the context passed in as the parameter
automatically. This allows you, the Kaze Block author, to access the
context's private storage in your algorithm via `ctx->data`, which
should be cast or implicitly converted to the type of your Kaze Block
module's context data before dereferencing.  Again, considering our
example skeleton:

```C
static KErrorCode algorithm (KBlockContext* ctx)
{
    ContextData* data = ctx->data;

    /* for example:
       access option1 using: data->option1
       access the input image buffer using: data->input_image

       simple, huh?
    */
}
```

Finally, recall from [The KIOTerminal Block](#module-kioterminal) section that
`KIOTerminal`s defined as OUTPUT buffers are always allocated within the
module, and it is the caller's responsibility to the free output buffers
produced by the module.
