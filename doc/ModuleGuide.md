Module Author Guide                                       {#moduleguide}
========================================================================

[TOC]

Modules provide a means of extending Kaze's functionality by registering
new capabilities with one or more of Kaze's subsystems.  Modules can
either be compiled directly into Kaze or loaded dynamically into Kaze at
runtime via the `kmodule_load()` API call.

For example, you could write a module that extends Kaze's image I/O
capabilities to read and write a new file format.  Such a module would
register this added functionality with the Kaze File Operations
(KFileOps) subsystem.  Alternatively, you could write a module that
adds a new algorithmic block to Kaze, which would register with the
KBlock subsystem.

Tutorials detailing how to write a module extending Kaze's various
subsystems can be found here:

- [Writing a KImage File I/O Module](@ref writing-kfileops) - Learn how
  to write modules that extend the file formats recognized by
  `kimage_read()` and `kimage_write()`

- [Writing a Kaze Block Module](@ref writing-kblocks) - Learn how to
  write modules that add to Kaze's algorithm library of `KBlock`s.


# The Skeleton of a Kaze Module                       {#module-skeleton}

The following is a bare bones skeleton for a Kaze module:

```C
#include "kerror.h"
#include "kmodule.h"
#include "internal/kmodule.h"


static KErrorCode load ()
{
    kmodule_description ("Does absolutely nothing");

    return SUCCESS;
}

static KErrorCode unload ()
{
    return SUCCESS;
}
```

Functionally, this module does absolutely nothing; however, it does
meet the minimum requirements to be successfully loaded and unloaded at
runtime by Kaze's module system.

Kaze modules must `#include "internal/kmodule.h"`.  This is a special
header file with tight build system integration that exports the symbol
table entries necessary for the module to be found by Kaze's module
loading system.  It is also necessary to `#include "kerror.h"`, since
modules use `KErrorCode`s to communicate success or failure when being
loaded/unloaded by Kaze's module system.


## Module Load Procedure                                  {#module-load}

Internal modules (i.e. modules compiled directly into Kaze) are loaded
when the `kmodule_load_bank()` API is called.  Likewise, external
modules are loaded from the filesystem via `kmodule_load()`.  In either
case, Kaze's module loading mechanics will execute the module's `load()`
function automatically.  Consequently, a module must use `load()` as an
opportunity to provide a description of itself (via
`kmodule_description()`) and to register itself with any Kaze subsystems
that it intends to provide extended functionality.

If all operations performed within `load()` where successful, then the
`KErrorCode` of `SUCCESS` must be returned; otherwise, a non-`SUCCESS`
`KErrorCode` must be returned.  Returning non-`SUCCESS` indicates to the
Kaze module loading system that associated dynamic memory resources
should be released.


## Module UnLoad Procedure                              {#module-unload}

All modules (both internal and external) are unloaded when the
`kmodule_unload_bank()` API is called.  When this occurs, the Kaze
module system cycles through all currently loaded modules and
automatically executes the `unload()` function provided by each one.
Consequently, modules must use `unload()` to unregister themselves with
any Kaze subsystems they registered themselves with in `load()`.
Furthermore, if any memory was dynamically allocated in `load()`, it
must be freed within `unload()`.  If successful, `unload()` must return
`SUCCESS`; otherwise, it must return a non-`SUCCESS` `KErrorCode`.


# Building Your Module                                   {#module-build}

Kaze's intelligent build system makes adding a new module easy.  All
modules reside within the `modules/` directory in the Kaze source tree.
New modules added to the `modules/` directory will be recognized by the
build system automatically.


## Directory Structure                     {#module-directory-structure}

Each module resides within its own folder and consists of the module's
source file and a build file named `CMakeLists.txt`.  For example, for a
module named `my_module`, the file structure looks like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~
kaze/
 |--modules/
     |--my_module/
         |--CMakeLists.txt
         +--main.c
~~~~~~~~~~~~~~~~~~~~~~~~~~~


## Buildsystem File                                 {#module-cmakelists}

Your module's directory should contain a `CMakeLists.txt`, which
specifies the default build mode for your module (i.e. `Internal`,
`Module`, or `Disabled`) and any external libraries your module should
be linked against.  Here is a minimal skeleton `CMakeLists.txt` for a
Kaze module:

```cmake
# Argument 1: module source file
# Argument 2: default build mode
#   - "Internal" will be built directly into into Kaze
#   - "Module" will be built as a kaze module file (*.km)
#   - "Disabled" will not be built by default
add_module (main.c "Internal")
module_link_libraries ()
```

Standard CMake functions such as `find_package()` can be used in this
file as necessary.  However, CMake functions that add build targets
should be avoided since the Kaze provided `add_module()` CMake macro
creates the necessary build targets automatically.

Furthermore, the Kaze provided `module_link_libraries()` macro should be
used to specify libraries that your module must be linked against.  The
usage of `module_link_libraries()` is similar to that of the standard
CMake `target_link_libraries()` function with the module implicitly
passed as the target.
